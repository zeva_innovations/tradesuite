import { Injectable } from '@angular/core';
import { CustomerFormValidatorService } from './customer-form-validator.service';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

@Injectable({
	providedIn: 'root'
})
export class CompanyDetailsFormService {
	mobnumPattern = '^((\\+[0-9]{1,3}-?)|0)?[0-9]{4,10}$';
	emailPattern = '^[a-z0-9](\\.?[a-z0-9_-]){0,}@[a-z0-9-]+\\.([a-z]{1,6}\\.)?[a-z]{2,6}$';
	websitePattern = '^(http:\\/\\/www\\.|https:\\/\\/www\\.|http:\\/\\/|https:\\/\\/)?[a-z0-9]+([\\-\\.]{1}[a-z0-9]+)*\\.[a-z]{2,5}(:[0-9]{1,5})?(\\/.*)?$';

	public formGroup: FormGroup;
	constructor(private customerFormValidator: CustomerFormValidatorService, private formBuilder: FormBuilder) {
		this.formGroup = this.formBuilder.group(
			{
				companyDetails: this.formBuilder.group({
					applicantCode: new FormControl({ value: null, disabled: true }, [Validators.required]),
					companyName: new FormControl(null, [Validators.required]),
					address1: new FormControl('', [Validators.required]),
					address2: new FormControl('', [Validators.required]),
					address3: new FormControl('', [Validators.pattern(this.websitePattern)]),
					ieRegCertificateNo: new FormControl('', [Validators.required]),
					phoneNo: new FormControl('', [Validators.required, Validators.pattern(this.mobnumPattern), Validators.minLength(7)]),
					emailID: new FormControl('', [Validators.pattern(this.emailPattern)]),
					city: new FormControl('', Validators.required),
					country: new FormControl('', Validators.required),
					state: new FormControl('', Validators.required),
					pin: new FormControl(''),
					remark: new FormControl(''),
					tin: new FormControl('', Validators.required),
					companyType: new FormControl('', Validators.required),
					companyAreaType: new FormControl('', Validators.required),
					industryType: new FormControl('', Validators.required),
					applicationinfo1: new FormControl('', Validators.required)
				})
			},
			{
				validator: this.customerFormValidator.formValidator()
			}
		);
	}

	checkDates(control: FormControl) {
		if (control.value === '' || control.value === null) {
			return { dobNull: true };
		}
		return null;
	}
}
