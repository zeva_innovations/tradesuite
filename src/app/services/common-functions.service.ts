import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { IApplicant } from '../interfaces/customer-details';
import { BehaviorSubject, Subject, Observable } from 'rxjs';
import { MatSnackBar } from '@angular/material';
@Injectable({
	providedIn: 'root'
})
export class CommonFunctionsService {
	applicantCodeForCompanyRegistration: any;
	companyDetailsForRenewal: any;
	isRenewalFlow = false;
	public isDgApproval = false;
	public goBackUrl = 'welcome';
	public isLoading = new BehaviorSubject<boolean>(false);
	public invokeEvent = new BehaviorSubject<string>('');
	public isTransactionCompleted = new BehaviorSubject<boolean>(false);
	constructor(public snackbar: MatSnackBar) { }

	convertDateFormat(date: moment.MomentInput) {
		const formattedDate = moment(date).format('DD-MM-YYYY');
		return formattedDate;
	}

	parseApplicantInfoFromlocalStorage() {
		const currentUser = JSON.parse(localStorage.getItem('currentUser'));
		const accessToken = currentUser.token;
		const refreshToken = currentUser.refreshToken;
		const orgId = currentUser.orG_ID;
		const unitOrgId = currentUser.uniT_ORG_ID;
		const branchId = currentUser.brancH_ORG_ID;
		const userName = currentUser.userName;
		const userId = currentUser.userID;
		return [accessToken, refreshToken, orgId, unitOrgId, branchId, userName, userId];
	}

	invokeMethods() {
		this.invokeEvent.next('openPopUp');
	}

	removeItemInArrayByvalue(inputArray: any[], itemTobeRemoved: any) {
		for (let i = 0; i < inputArray.length - 1; i++) {
			if (inputArray[i] === itemTobeRemoved) {
				inputArray.splice(i, 1);
			}
		}
	}

	getUTCNow() {
		const now = new Date();
		const time = now.getTime();
		let offset = now.getTimezoneOffset();
		offset = offset * 60000;
		return time - offset;
	}

	getRolesBasedOnMenu() {
		let roles = [];
		const userRoles = [];
		const userMenu = [];
		roles = JSON.parse(localStorage.getItem('roles'));
		if (roles && roles.length > 0) {
			roles.forEach((data) => {
				userRoles.push(data.mainMenu);
				userMenu.push(data.rights);
			});
		}

		return [userRoles, userMenu];
	}

	isUserAuthenticated() {
		if (localStorage.getItem('currentUser')) {
			return true;
		}
		return false;
	}

	getLicenseType(license: string) {
		if (license !== undefined && license !== null) {
			return license.split('-')[1];
		}
	}
}
