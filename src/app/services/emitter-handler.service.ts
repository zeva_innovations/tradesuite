import { Injectable, Output, EventEmitter } from '@angular/core';

@Injectable({
	providedIn: 'root'
})
export class EmitterHandlerService {
	constructor() {}
	editMode = false;
	applicantId = 0;
	popupFlag = false;
	@Output() change: EventEmitter<boolean> = new EventEmitter();
	@Output() passApplicantId: EventEmitter<any> = new EventEmitter();
	@Output() popUpChange: EventEmitter<boolean> = new EventEmitter();

	toggleEditMode() {
		this.editMode = true;
		this.change.emit(this.editMode);
	}
	emitApplicantId(applicantId: any) {
		this.passApplicantId.emit(applicantId);
	}
	emitOpenPopUpEvnet() {
		this.popupFlag = true;
		this.popUpChange.emit(this.popupFlag);
	}
}
