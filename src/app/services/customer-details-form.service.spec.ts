import { TestBed } from '@angular/core/testing';

import { CustomerDetailsFormService } from './customer-details-form.service';

describe('CustomerDetailsFormService', () => {
	beforeEach(() => TestBed.configureTestingModule({}));

	it('should be created', () => {
		const service: CustomerDetailsFormService = TestBed.get(CustomerDetailsFormService);
		expect(service).toBeTruthy();
	});
});
