import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpEventType } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { IApplicant } from '../interfaces/customer-details';

@Injectable({
	providedIn: 'root'
})
export class ApiHandlerService {
	constructor(private http: HttpClient) {}

	getApiRequest(url: string): Observable<any[]> {
		return this.http.get(url).pipe(
			map((applicants: any[]) => {
				return applicants;
			})
		);
	}

	postApiRequest(url: string, body: string): Observable<any[]> {
		return this.http.post(url, body).pipe(
			map((applicants: any[]) => {
				return applicants;
			})
		);
	}

	deleteApiRequest(url: string): Observable<any[]> {
		return this.http.delete(url).pipe(
			map((applicants: any[]) => {
				return applicants;
			})
		);
	}
	putApiRequest(url: string, body: string): Observable<any[]> {
		return this.http.put(url, body).pipe(
			map((applicants: any[]) => {
				return applicants;
			})
		);
	}

	postApiRequestForFileUpload(url: string, body: string) {
		return this.http
			.post(url, body, {
				reportProgress: true,
				observe: 'events'
			})
			.pipe(
				map((events) => {
					return events;
				})
			);
	}

	getApiRequestForFileDownload(url: string) {
		return this.http
			.get(url, {
				reportProgress: true,
				observe: 'events'
			})
			.pipe(
				map((events) => {
					return events;
				})
			);
	}
}
