import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { CommonFunctionsService } from './common-functions.service';
import * as _ from 'lodash';
import { AuthenticationService } from './authentication.service';

@Injectable({
	providedIn: 'root'
})
export class AuthGuard {
	constructor(private router: Router, public commonFunction: CommonFunctionsService, public authService: AuthenticationService) {}
	canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
		let rolesMenu = [];

		rolesMenu = this.commonFunction.getRolesBasedOnMenu()[0];
		if (rolesMenu) {
			if (rolesMenu && !rolesMenu.includes(route.data.roles[0])) {
				this.router.navigate(['/']);
				return false;
			}
			return true;
		}

		this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
		this.authService.logout();
		return false;
	}
}
