import { TestBed } from '@angular/core/testing';

import { CompanyDetailsFormService } from './company-details-form.service';

describe('CompanyDetailsFormService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CompanyDetailsFormService = TestBed.get(CompanyDetailsFormService);
    expect(service).toBeTruthy();
  });
});
