import { TestBed } from '@angular/core/testing';

import { TitlevalidatorService } from './titlevalidator.service';

describe('TitlevalidatorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TitlevalidatorService = TestBed.get(TitlevalidatorService);
    expect(service).toBeTruthy();
  });
});
