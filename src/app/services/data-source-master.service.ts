import { environment } from './../../environments/environment';
import { IApplicant } from './../interfaces/customer-details';
import { catchError, finalize } from 'rxjs/operators';
import { Injectable, ViewChild } from '@angular/core';
import { DataSource } from '@angular/cdk/table';
import { CollectionViewer } from '@angular/cdk/collections';
import { BehaviorSubject, of, Observable } from 'rxjs';
import { ApiHandlerService } from '../services/api-handler.service';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { CommonFunctionsService } from './common-functions.service';
import * as _ from 'lodash';
@Injectable({
	providedIn: 'root'
})
export class DataSourceMasterService extends MatTableDataSource<any> {
	loggedUserName = this.commonFunction.parseApplicantInfoFromlocalStorage()[5];
	loggedUserId = this.commonFunction.parseApplicantInfoFromlocalStorage()[6];

	public dataSubject = new BehaviorSubject<any[]>([]);
	public loadingSubject = new BehaviorSubject<boolean>(false);
	public loading$ = this.loadingSubject.asObservable();

	constructor(private apiHandler: ApiHandlerService, private commonFunction: CommonFunctionsService) {
		super();
	}

	loadApplicants(url: string) {
		this.loadingSubject.next(true);
		this.apiHandler.getApiRequest(url).pipe(catchError(() => of([])), finalize(() => this.loadingSubject.next(false))).subscribe((data) => {
			let masterDataObject: any;
			masterDataObject = data;
			let allMasterWithActive = _.map(masterDataObject, function(masterObj) {
				if (masterObj.status === 'ACTIVE') { return masterObj; }
			});
			// Remove undefines from the array
			allMasterWithActive = _.without(allMasterWithActive, undefined);
			this.dataSubject.next(allMasterWithActive);
		});
	}

	deleteApplicants(url: string, parameter: any) {
		const deleteParameter = url + '/' + parameter;
		this.loadingSubject.next(true);
		this.apiHandler.deleteApiRequest(deleteParameter).pipe(catchError(() => of([])), finalize(() => this.loadingSubject.next(false))).subscribe((data) => {
			this.loadApplicants(url);
		});
	}

	postApplicants(url: string, body: string) {
		this.loadingSubject.next(true);
		this.apiHandler.postApiRequest(url, body).pipe(catchError(() => of([])), finalize(() => this.loadingSubject.next(false))).subscribe((applicants) => {
			this.loadApplicants(url);
		});
	}

	putApplicants(url: string, parameter: any, body: string) {
		const putParameter = url + '/' + parameter;
		this.loadingSubject.next(true);
		this.apiHandler
			.putApiRequest(putParameter, body)
			.pipe(catchError(() => of([])), finalize(() => this.loadingSubject.next(false)))
			.subscribe((applicants) => {
				this.loadApplicants(url);
			});
	}

	connect(): BehaviorSubject<IApplicant[]> {
		return this.dataSubject;
	}
	disconnect(): void {
		return this.dataSubject.complete();
	}
}
