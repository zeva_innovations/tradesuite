import { TestBed } from '@angular/core/testing';

import { DataSourceMasterService } from './data-source-master.service';

describe('DataSourceMasterService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DataSourceMasterService = TestBed.get(DataSourceMasterService);
    expect(service).toBeTruthy();
  });
});
