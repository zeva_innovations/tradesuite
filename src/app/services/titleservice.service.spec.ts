import { TestBed } from '@angular/core/testing';

import { TitleserviceService } from './titleservice.service';

describe('TitleserviceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TitleserviceService = TestBed.get(TitleserviceService);
    expect(service).toBeTruthy();
  });
});
