import {
	HttpEvent,
	HttpHandler,
	HttpInterceptor,
	HttpRequest,
	HttpErrorResponse,
	HttpSentEvent,
	HttpHeaderResponse,
	HttpProgressEvent,
	HttpResponse,
	HttpUserEvent
} from '@angular/common/http';
import { Injectable, Injector } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError, BehaviorSubject } from 'rxjs';
import { catchError, switchMap, filter, take, finalize, map } from 'rxjs/operators';
import { AuthenticationService } from '../services/authentication.service';
import { ICurrentUser } from '../interfaces/user';
import { CommonFunctionsService } from '../services/common-functions.service';
import { HttpUploadProgressEvent, HttpDownloadProgressEvent } from '@angular/common/http/src/response';

@Injectable()
export class AuthInterceptorService implements HttpInterceptor {
	constructor(private authService: AuthenticationService, private commonFunction: CommonFunctionsService) {}

	isRefreshingToken = false;
	tokenSubject: BehaviorSubject<string> = new BehaviorSubject<string>(null);

	intercept(
		request: HttpRequest<any>,
		next: HttpHandler
	): Observable<
		| HttpSentEvent
		| HttpHeaderResponse
		| HttpProgressEvent
		| HttpUploadProgressEvent
		| HttpDownloadProgressEvent
		| HttpResponse<any>
		| HttpUserEvent<any>
		| any
	> {
		return next.handle(this.addTokenToRequest(request, this.authService.getAuthToken())).pipe(
			catchError((err) => {
				if (err instanceof HttpErrorResponse) {
					switch (err.status) {
						case 401:
							return this.handle401Error(request, next);
						case 400:
							this.authService.logout();
					}
				} else {
					return throwError(err);
				}
			}),
			finalize(() => {
				if (request.reportProgress !== true) {
					this.commonFunction.isLoading.next(false);
				}
				if (request.method === 'POST' || request.method === 'PUT') {
					this.commonFunction.isTransactionCompleted.next(true);
				}
			})
		);
	}

	private addTokenToRequest(request: HttpRequest<any>, token: string): HttpRequest<any> {
		this.commonFunction.isLoading.next(true);
		if (!(request.url.includes('refreshtoken') || request.url.includes('login'))) {
			return request.clone({
				setHeaders: {
					Authorization: `Bearer ${token}`,
					'Content-Type': 'application/json'
				}
			});
		} else {
			return request.clone({
				setHeaders: {
					'Content-Type': 'application/json'
				}
			});
		}
	}

	private handle401Error(request: HttpRequest<any>, next: HttpHandler) {
		if (!this.isRefreshingToken) {
			this.isRefreshingToken = true;
			// Reset here so that the following requests wait until the token
			// comes back from the refreshToken call.
			this.tokenSubject.next(null);

			return this.authService.refreshToken().pipe(
				switchMap((user: ICurrentUser) => {
					if (user) {
						this.tokenSubject.next(user.token);
						localStorage.setItem('currentUser', JSON.stringify(user));
						return next.handle(this.addTokenToRequest(request, user.token));
					}

					this.authService.logout();
				}),
				catchError((err) => {
					return <any>this.authService.logout();
				}),
				finalize(() => {
					this.isRefreshingToken = false;
				})
			);
		} else {
			this.isRefreshingToken = false;
			return this.tokenSubject.pipe(
				filter((token) => token != null),
				take(1),
				switchMap((token) => {
					return next.handle(this.addTokenToRequest(request, token));
				})
			);
		}
	}
}
