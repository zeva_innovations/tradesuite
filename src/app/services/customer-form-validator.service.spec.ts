import { TestBed } from '@angular/core/testing';

import { CustomerFormValidatorService } from './customer-form-validator.service';

describe('CustomerFormValidatorService', () => {
	beforeEach(() => TestBed.configureTestingModule({}));

	it('should be created', () => {
		const service: CustomerFormValidatorService = TestBed.get(CustomerFormValidatorService);
		expect(service).toBeTruthy();
	});
});
