import { Injectable } from '@angular/core';
import {FormGroup, ValidationErrors, ValidatorFn} from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class TitlevalidatorService {
  constructor() {}
  formValidator(): ValidatorFn {
    const errors: ValidationErrors = {};
    return (control: FormGroup): ValidationErrors => {
      return errors;
    };
  }
}
