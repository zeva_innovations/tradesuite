import { environment } from './../../environments/environment';
import { IApplicant } from './../interfaces/customer-details';
import { catchError, finalize } from 'rxjs/operators';
import { Injectable, ViewChild } from '@angular/core';
import { DataSource } from '@angular/cdk/table';
import { CollectionViewer } from '@angular/cdk/collections';
import { BehaviorSubject, of, Observable } from 'rxjs';
import { ApiHandlerService } from '../services/api-handler.service';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { CommonFunctionsService } from './common-functions.service';

@Injectable({
	providedIn: 'root'
})
export class DataSourceServiceService extends MatTableDataSource<IApplicant> {
	loggedUserName = this.commonFunction.parseApplicantInfoFromlocalStorage()[5];
	loggedUserId = this.commonFunction.parseApplicantInfoFromlocalStorage()[6];
	applicantsRoute = environment.tradeAppRoot + environment.getApplicants;
	applicantsByUser = environment.tradeAppRoot + environment.getApplicants + '/get/' + this.loggedUserName + this.loggedUserId;
	public applicantSubject = new BehaviorSubject<IApplicant[]>([]);
	public dialogSubject = new BehaviorSubject<IApplicant[]>([]);
	public loadingSubject = new BehaviorSubject<boolean>(false);
	public loading$ = this.loadingSubject.asObservable();

	constructor(private apiHandler: ApiHandlerService, private commonFunction: CommonFunctionsService) {
		super();
	}

	loadApplicants(url: string) {
		this.loadingSubject.next(true);
		this.apiHandler.getApiRequest(url).pipe(catchError(() => of([])), finalize(() => this.loadingSubject.next(false))).subscribe((applicants) => {
			this.applicantSubject.next(applicants);
		});
	}

	loadDataForDialog(url: string) {
		this.loadingSubject.next(true);
		this.apiHandler.getApiRequest(url).pipe(catchError(() => of([])), finalize(() => this.loadingSubject.next(false))).subscribe((applicants) => {
			this.dialogSubject.next(applicants);
		});
	}

	deleteApplicants(url: string, parameter: any) {
		const deleteParameter = url + '/' + parameter;
		this.loadingSubject.next(true);
		this.apiHandler
			.deleteApiRequest(deleteParameter)
			.pipe(catchError(() => of([])), finalize(() => this.loadingSubject.next(false)))
			.subscribe((applicants) => {
				this.loadApplicants(url + '/get/' + this.loggedUserName + this.loggedUserId);
			});
	}

	postApplicants(url: string, body: string) {
		this.loadingSubject.next(true);
		this.apiHandler.postApiRequest(url, body).pipe(catchError(() => of([])), finalize(() => this.loadingSubject.next(false))).subscribe((applicants) => {
			this.loadApplicants(url + '/get/' + this.loggedUserName + this.loggedUserId);
		});
	}

	putApplicants(url: string, parameter: any, body: string) {
		const putParameter = url + '/' + parameter;
		this.loadingSubject.next(true);
		this.apiHandler
			.putApiRequest(putParameter, body)
			.pipe(catchError(() => of([])), finalize(() => this.loadingSubject.next(false)))
			.subscribe((applicants) => {
				this.loadApplicants(url + '/get/' + this.loggedUserName + this.loggedUserId);
			});
	}

	putDialogGrid(url: string, parameter: any, body: string, dependency: any) {
		const putParameter = url + '/' + parameter;
		this.loadingSubject.next(true);
		this.apiHandler
			.putApiRequest(putParameter, body)
			.pipe(catchError(() => of([])), finalize(() => this.loadingSubject.next(false)))
			.subscribe((applicants) => {
				this.loadDataForDialog(url + '/getAccCompanyCode/' + dependency);
			});
	}
	putDialogGrid2(url: string, parameter: any, body: string, dependency: any) {
		const putParameter = url + '/' + parameter;
		this.loadingSubject.next(true);
		this.apiHandler
			.putApiRequest(putParameter, body)
			.pipe(catchError(() => of([])), finalize(() => this.loadingSubject.next(false)))
			.subscribe((applicants) => {
				this.loadDataForDialog(url + '/getAccCompanyCode2/' + dependency);
			});
	}
	connect(): BehaviorSubject<IApplicant[]> {
		return this.applicantSubject;
	}
	disconnect(): void {
		return this.applicantSubject.complete();
	}
}
