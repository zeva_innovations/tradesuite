import { Injectable } from '@angular/core';
import { FormArray, FormControl, FormGroup, ValidationErrors, ValidatorFn, AbstractControl } from '@angular/forms';

@Injectable()
export class CustomerFormValidatorService {
	constructor() {}
	formValidator(): ValidatorFn {
		const errors: ValidationErrors = {};
		return (control: FormGroup): ValidationErrors => {
			return errors;
		};
	}
}
