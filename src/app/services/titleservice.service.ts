import { Injectable } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, FormGroupDirective, NgForm, Validators} from '@angular/forms';

import {ErrorStateMatcher} from '@angular/material';
import {TitlevalidatorService} from './titlevalidator.service';

@Injectable({
  providedIn: 'root'
})
export class TitleserviceService { public formGroup: FormGroup;
  constructor(
    private titleValidator: TitlevalidatorService,
    private formBuilder: FormBuilder
  ) {
    this.formGroup = this.formBuilder.group(
      {
        titleDetails: this.formBuilder.group({
          titleName: new FormControl(null, [Validators.required]),
          titleCode: new FormControl('', [Validators.required]),
        })
      },
      {
        validator: this.titleValidator.formValidator()
      }
    );
  }

  checkDates(control: FormControl) {
    if (control.value === '' || control.value === null) {
      return { dobNull: true };
    }
    return null;
  }
}

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(
    control: FormControl | null,
    form: FormGroupDirective | NgForm | null
  ): boolean {
    const invalidCtrl = !!(control && control.invalid);
    const invalidParent = !!(
      control &&
      control.parent &&
      control.parent.invalid
    );

    return invalidCtrl || invalidParent;
  }
}
