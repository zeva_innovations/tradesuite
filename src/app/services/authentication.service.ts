import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { share, map } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';
import { ICurrentUser } from '../interfaces/user';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';

@Injectable({
	providedIn: 'root'
})
export class AuthenticationService {
	rootApi = environment.tradeAppRoot;
	loginApi = this.rootApi + environment.loginRoute;
	refreshTokenApi = this.rootApi + environment.refreshTokenRoute;

	constructor(private http: HttpClient, private router: Router) {}

	login(username: string, password: string): Observable<ICurrentUser> {
		return this.http
			.post<ICurrentUser>(this.loginApi, {
				Username: username,
				PasswordHash: password
			})
			.pipe(
				map((user) => {
					if (user && user.token) {
						localStorage.setItem('currentUser', JSON.stringify(user));
					}
					return user;
				})
			);
	}

	logout() {
		localStorage.removeItem('currentUser');
		localStorage.removeItem('roles');
		this.router.navigate(['/login']);
	}

	// We will wait for the backend to Implement the Refresh Token then we can use this to make sure we are able to do Refresh Token
	// http://ericsmasal.com/2018/07/02/angular-6-with-jwt-and-refresh-tokens-and-a-little-rxjs-6/
	refreshToken(): Observable<ICurrentUser> {
		const currentUser = JSON.parse(localStorage.getItem('currentUser'));
		const accessToken = currentUser.token;
		const refreshToken = currentUser.refreshToken;
		return this.http
			.post<ICurrentUser>(this.refreshTokenApi, {
				accessToken: accessToken,
				refreshToken: refreshToken
			})
			.pipe(
				map((user) => {
					if (user && user.token) {
						localStorage.setItem('currentUser', JSON.stringify(user));
					}
					return user;
				})
			);
	}

	getAuthToken(): string {
		const currentUser = JSON.parse(localStorage.getItem('currentUser'));
		if (currentUser != null) {
			return currentUser.token;
		}

		return '';
	}
}
