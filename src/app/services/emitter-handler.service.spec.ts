import { TestBed } from '@angular/core/testing';

import { EmitterHandlerService } from './emitter-handler.service';

describe('EmitterHandlerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EmitterHandlerService = TestBed.get(EmitterHandlerService);
    expect(service).toBeTruthy();
  });
});
