import { TestBed } from '@angular/core/testing';

import { DataSourceServiceService } from './data-source-service.service';

describe('DataSourceServiceService', () => {
	beforeEach(() => TestBed.configureTestingModule({}));

	it('should be created', () => {
		const service: DataSourceServiceService = TestBed.get(DataSourceServiceService);
		expect(service).toBeTruthy();
	});
});
