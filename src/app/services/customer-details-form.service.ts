import { CustomerFormValidatorService } from './customer-form-validator.service';
import { Injectable } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators, FormControl, FormGroupDirective, NgForm } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { validate } from 'codelyzer/walkerFactory/walkerFn';

@Injectable({
	providedIn: 'root'
})
export class CustomerDetailsFormService {
	mobnumPattern = '^((\\+[0-9]{1,3}-?)|0)?[0-9]{4,10}$';
	emailPattern = '^[a-z0-9](\\.?[a-z0-9_-]){0,}@[a-z0-9-]+\\.([a-z]{1,6}\\.)?[a-z]{2,6}$';

	public formGroup: FormGroup;
	constructor(private customerFormValidator: CustomerFormValidatorService, private formBuilder: FormBuilder) {
		this.formGroup = this.formBuilder.group(
			{
				customerDetails: this.formBuilder.group({
					firstName: new FormControl('', [Validators.required]),
					surName: new FormControl('', [Validators.required]),
					emailID: new FormControl('', [Validators.pattern(this.emailPattern)]),
					phoneNo: new FormControl('', [Validators.required, Validators.pattern(this.mobnumPattern), Validators.minLength(7)]),
					dob: new FormControl({ value: '', disabled: false }, [this.checkDates]),
					city: new FormControl('', Validators.required),
					country: new FormControl('', Validators.required),
					state: new FormControl('', Validators.required),
					gender: new FormControl('', Validators.required),
					passportNo: new FormControl(''),
					socialSecurityNo: new FormControl(''),
					remark1: new FormControl('')
				})
			},
			{
				validator: this.customerFormValidator.formValidator()
			}
		);
	}

	checkDates(control: FormControl) {
		if (control.value === '' || control.value === null) {
			return { dobNull: true };
		}
		return null;
	}
}

export class MyErrorStateMatcher implements ErrorStateMatcher {
	isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
		const invalidCtrl = !!(control && control.invalid);
		const invalidParent = !!(control && control.parent && control.parent.invalid);

		return invalidCtrl || invalidParent;
	}
}
