import { Injectable } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {CustomerFormValidatorService} from '../customer-form-validator.service';

@Injectable({
  providedIn: 'root'
})
export class AssignDocumentService {


  public formGroup: FormGroup;
  constructor(private customerFormValidator: CustomerFormValidatorService, private formBuilder: FormBuilder) {
    this.formGroup = this.formBuilder.group(
      {
        assignDocumentDetails: this.formBuilder.group({

          applyType: new FormControl(null, [Validators.required]),
          industryNameCode: new FormControl(null, [Validators.required]),
          documentType: new FormControl(null, [Validators.required]),
        })
      },
      {
        validator: this.customerFormValidator.formValidator()
      }
    );
  }
  checkDates(control: FormControl) {
    if (control.value === '' || control.value === null) {
      return { dobNull: true };
    }
    return null;
  }
}
