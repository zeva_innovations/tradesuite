import { TestBed } from '@angular/core/testing';

import { AssignDocumentService } from './assign-document.service';

describe('AssignDocumentService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AssignDocumentService = TestBed.get(AssignDocumentService);
    expect(service).toBeTruthy();
  });
});
