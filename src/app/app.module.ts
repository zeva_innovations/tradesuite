import { AuthInterceptorService } from './services/auth-interceptor.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { CustomerDetailsComponent } from './components/customer-details/customer-details.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from './shared/shared.module';
import { CustomerFormValidatorService } from './services/customer-form-validator.service';
import { CustomerDetailsFormService } from './services/customer-details-form.service';
import { SnackbarComponent } from './components/snackbar/snackbar.component';
import { ApplicantGridComponent } from './components/applicant-grid/applicant-grid.component';
import { FileUploadComponent, DialogContentExampleDialog } from './components/file-upload/file-upload.component';
import { FileUploadModule } from 'ng2-file-upload';
import { EmitterHandlerService } from './services/emitter-handler.service';
import { ApplicantReceiptComponent } from './components/applicant-receipt/applicant-receipt.component';
import { ApplicantionIssueComponent, ApplicationIssueDialog } from './components/applicantion-issue/applicantion-issue.component';
import { CompanyDetailsComponent } from './components/company-details/company-details.component';
import { SidenavBarComponent } from './components/sidenav-bar/sidenav-bar.component';
import { FormCollectionComponent, FormCollectionDialog } from './components/form-collection/form-collection.component';
import { ScanDepartmentComponent } from './components/scan-department/scan-department.component';
import { ApplicationIssueCompanyGridComponent } from './components/application-issue-company-grid/application-issue-company-grid.component';
import { CompanyRegistrationComponent } from './components/company-registration/company-registration.component';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { DataentryComponent } from './components/dataentry/dataentry.component';
import { VerifyassignComponent } from './components/verifyassign/verifyassign.component';
import { ApprovalComponent, ApprovalDialogComponent } from './components/approval/approval.component';
import { CompanyApprovalListComponent } from './components/company-approval-list/company-approval-list.component';
import { CompanyListAccountOneComponent } from './components/company-list-account-one/company-list-account-one.component';
import { AccountOneDialogComponent, AccountOneComponent } from './components/account-one/account-one.component';
import { AccountTwoDialogComponent, AccountTwoComponent } from './components/account-two/account-two.component';
import { CompanyListAccountTwoComponent } from './components/company-list-account-two/company-list-account-two.component';
import { FormCollectionCompanyGridComponent } from './components/form-collection-company-grid/form-collection-company-grid.component';
import { CompanyApprovalDGListComponent } from './components/company-approval-dglist/company-approval-dglist.component';
import { CardPrintListComponent } from './components/card-print-list/card-print-list.component';
import { CardWriteListComponent } from './components/card-write-list/card-write-list.component';
import { CardIssueListComponent } from './components/card-issue-list/card-issue-list.component';
import { CompanyRenewalGridComponent } from './components/company-renewal-grid/company-renewal-grid.component';
import { CompanyListsComponent } from './components/company-lists/company-lists.component';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { LicensePipe } from './pipes/license.pipe';

export const MY_FORMATS = {
	parse: {
		dateInput: 'DD-MM-YYYY'
	},
	display: {
		dateInput: 'DD-MM-YYYY',
		monthYearLabel: 'MMM YYYY',
		dateA11yLabel: 'LL',
		monthYearA11yLabel: 'MMMM YYYY'
	}
};
@NgModule({
	declarations: [
		AppComponent,
		LoginComponent,
		RegisterComponent,
		CustomerDetailsComponent,
		SnackbarComponent,
		ApplicantGridComponent,
		FileUploadComponent,
		DialogContentExampleDialog,
		ApplicantReceiptComponent,
		ApplicantionIssueComponent,
		ApplicationIssueDialog,
		CompanyDetailsComponent,
		SidenavBarComponent,
		FormCollectionComponent,
		ScanDepartmentComponent,
		ApplicationIssueCompanyGridComponent,
		CompanyRegistrationComponent,
		WelcomeComponent,
		DataentryComponent,
		VerifyassignComponent,
		ApprovalComponent,
		CompanyApprovalListComponent,
		ApprovalDialogComponent,
		CompanyListAccountOneComponent,
		AccountOneComponent,
		AccountOneDialogComponent,
		AccountTwoComponent,
		AccountTwoDialogComponent,
		CompanyListAccountTwoComponent,
		FormCollectionCompanyGridComponent,
		CompanyApprovalDGListComponent,
		CardPrintListComponent,
		CardWriteListComponent,
		CardIssueListComponent,
		FormCollectionDialog,
		CompanyRenewalGridComponent,
		CompanyListsComponent,
		LicensePipe
	],
	imports: [
		RouterModule,
		BrowserModule,
		SharedModule,
		AppRoutingModule,
		FormsModule,
		ReactiveFormsModule,
		BrowserAnimationsModule,
		HttpClientModule,
		MatMomentDateModule,
		FileUploadModule,

		Ng4LoadingSpinnerModule.forRoot()
	],
	providers: [
		CustomerFormValidatorService,
		CustomerDetailsFormService,
		FormsModule,
		ReactiveFormsModule,
		HttpClientModule,
		EmitterHandlerService,
		{
			provide: HTTP_INTERCEPTORS,
			useClass: AuthInterceptorService,
			multi: true
		},
		{
			provide: DateAdapter,
			useClass: MomentDateAdapter,
			deps: [MAT_DATE_LOCALE]
		},

		{ provide: MAT_DATE_FORMATS, useValue: MY_FORMATS }
	],
	entryComponents: [
		DialogContentExampleDialog,
		ApplicationIssueDialog,
		ApprovalDialogComponent,
		AccountOneDialogComponent,
		AccountTwoDialogComponent,
		FormCollectionDialog
	],
	bootstrap: [AppComponent]
})
export class AppModule { }
