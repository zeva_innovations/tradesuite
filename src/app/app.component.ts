import { CommonFunctionsService } from 'src/app/services/common-functions.service';
import { Component } from '@angular/core';
import { Location } from '@angular/common';
import { AuthenticationService } from './services/authentication.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Idle, DEFAULT_INTERRUPTSOURCES } from '@ng-idle/core';
import { Keepalive } from '@ng-idle/keepalive';
import { MatDialog } from '@angular/material';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
})
export class AppComponent {
	title = 'Trade Suite';
	returnUrl: string;
	idleState = 'Not started.';
	timedOut = false;
	lastPing?: Date = null;
	currentPath: string;
	constructor(
		private location: Location,
		public authService: AuthenticationService,
		private router: Router,
		private route: ActivatedRoute,
		private idle: Idle,
		private keepalive: Keepalive,
		private dialogRef: MatDialog,
		public commonFunction: CommonFunctionsService,
		private spinnerService: Ng4LoadingSpinnerService
	) {
		this.commonFunction.isLoading.subscribe((data) => {
			if (data === true) {
				this.spinnerService.show();
			} else {
				this.spinnerService.hide();
			}
		});
		idle.setIdle(300);

		// sets a timeout period of 5 seconds. after 10 seconds of inactivity, the user will be considered timed out.
		idle.setTimeout(10);

		// sets the default interrupts, in this case, things like clicks, scrolls, touches to the document
		idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);

		idle.onIdleEnd.subscribe(() => (this.idleState = 'No longer idle.'));

		idle.onTimeout.subscribe(() => {
			this.idleState = 'Timed out!';
			this.timedOut = true;
		});

		idle.onIdleStart.subscribe(() => (this.idleState = 'You\'ve gone idle!'));
		idle.onTimeoutWarning.subscribe((countdown) => {
			if (countdown === 1) {
				this.dialogRef.closeAll();
				this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/login';
				this.router.navigate([this.returnUrl]);
			}
		});

		// Sets the ping interval to 15 seconds
		keepalive.interval(15);

		keepalive.onPing.subscribe(() => (this.lastPing = new Date()));

		// Lets check the path everytime the route changes, stop or start the idle check as appropriate.
		router.events.subscribe((val) => {
			this.currentPath = location.path();
			if (this.currentPath.search(/authentication\/login/gi) == -1) {
				idle.watch();
			} else {
				idle.stop();
			}
		});
	}
	ngOnInit() {
		this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/login';

		this.location.subscribe((x) => {
			if (x.type === 'popstate') {
				setTimeout(() => {
					this.dialogRef.closeAll();
					this.router.navigate([this.returnUrl]);
				}, 100);
			}
		});
	}
}
