import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerifyassignComponent } from './verifyassign.component';

describe('VerifyassignComponent', () => {
  let component: VerifyassignComponent;
  let fixture: ComponentFixture<VerifyassignComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerifyassignComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerifyassignComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
