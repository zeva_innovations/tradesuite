import { ApiHandlerService } from './../../services/api-handler.service';
import { CommonFunctionsService } from './../../services/common-functions.service';
import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { DomSanitizer } from '@angular/platform-browser';
import { FormGroup, FormBuilder, FormControl, FormArray } from '@angular/forms';
import { Router, RouterEvent, NavigationEnd } from '@angular/router';

@Component({
	selector: 'app-verifyassign',
	templateUrl: './verifyassign.component.html',
	styleUrls: ['./verifyassign.component.scss']
})
export class VerifyassignComponent implements OnInit {
	companyObject: any = this.commonFunction.applicantCodeForCompanyRegistration;
	documentBasedOnUser = environment.tradeAppRoot + environment.documentBasedOnUser;
	documentMasterType = environment.tradeAppRoot + environment.documentTypeMaster;
	verifyDocument = environment.tradeAppRoot + environment.documentUploadRoute;

	uploadedImages = [];
	documentTypeList = [];
	documentIdList = [];
	documentMasterTypeArray: string[] = [];
	com: [];
	componentName: string = 'Data Entry';

	form: FormGroup;
	isVerifiedDocuments: boolean;
	constructor(
		public commonFunction: CommonFunctionsService,
		private apiHandler: ApiHandlerService,
		private sanitizer: DomSanitizer,
		private formBuilder: FormBuilder,
		public router: Router
	) { }

	get companyDetails(): string {
		this.companyObject = this.commonFunction.applicantCodeForCompanyRegistration;
		return this.commonFunction.applicantCodeForCompanyRegistration;
	}

	set companyDetails(value: string) {
		this.commonFunction.applicantCodeForCompanyRegistration = value;
	}

	ngOnInit() {
		this.buildDrodDownForm();
		this.getDocumentTypeMaster();

		if (this.commonFunction.goBackUrl === 'dataentry') {
			this.componentName = 'Data Entry Image Verfication';
		} else if (this.commonFunction.goBackUrl === 'companyapproval') {
			this.componentName = 'Director Approval Image Verfication';
		} else if (this.commonFunction.goBackUrl === 'companydgapproval') {
			this.componentName = 'Final Approval Image Verfication';
		} else {
			this.componentName = this.commonFunction.goBackUrl;
		}
	}

	getUploadedImages() {
		this.commonFunction.isLoading.next(true);
		let urlBasedOnUser = this.documentBasedOnUser;
		let companyCode;
		if (this.companyObject.companyCode !== undefined && this.companyObject.companyCode !== null) {
			companyCode = this.companyObject.companyCode;
		} else {
			companyCode = this.companyObject.applicantID;
		}

		if (companyCode !== '') {
			urlBasedOnUser = urlBasedOnUser.concat('/', companyCode);
			this.apiHandler.getApiRequest(urlBasedOnUser).subscribe((data) => {
				data.forEach((data) => {
					this.documentTypeList.push(data.documentType);
					this.documentIdList.push(data.documentUpID);
					this.uploadedImages.push('data:image/png;base64, ' + data.dataSmall);
				});
				this.buildDrodDownForm();
			});
		}
	}

	buildDrodDownForm() {
		const formControls = this.documentTypeList.map((control) => new FormControl(control));
		this.form = this.formBuilder.group({
			documentCheckListArray: new FormArray(formControls)
		});
	}

	getDocumentTypeMaster() {
		this.apiHandler.getApiRequest(this.documentMasterType).subscribe((data) => {
			this.documentMasterTypeArray = data;
		});
	}

	updateVerifyDocuments(formObject: any) {
		const listArray = formObject['documentCheckListArray'];
		this.documentIdList.forEach((documentId, index) => {
			const body = { documentUpID: documentId, documentType: listArray[index] };

			this.apiHandler.putApiRequest(this.verifyDocument + '/' + documentId, JSON.stringify(body)).subscribe((data) => {
				this.isVerifiedDocuments = true;
			});
		});
	}

	ngAfterViewInit(): void {
		this.buildDrodDownForm();
		setTimeout(() => this.getUploadedImages(), 1000);
	}

	goBack() {
		this.router.navigate(['/' + this.commonFunction.goBackUrl]);
	}
}
