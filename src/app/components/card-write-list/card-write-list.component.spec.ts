import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardWriteListComponent } from './card-write-list.component';

describe('CardWriteListComponent', () => {
  let component: CardWriteListComponent;
  let fixture: ComponentFixture<CardWriteListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardWriteListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardWriteListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
