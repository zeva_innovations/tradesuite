import { take, switchMap } from 'rxjs/operators';
import { FormGroup, FormBuilder, FormControl, FormArray } from '@angular/forms';
import { environment } from './../../../environments/environment';
import { CommonFunctionsService } from './../../services/common-functions.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatSnackBar } from '@angular/material';
import { FileUploader, FileItem } from 'ng2-file-upload';
import { EmitterHandlerService } from 'src/app/services/emitter-handler.service';
import { ApiHandlerService } from '../../services/api-handler.service';
import { DomSanitizer } from '@angular/platform-browser';
import { ApplicantGridComponent } from '../applicant-grid/applicant-grid.component';
import { config } from 'rxjs';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { Response } from 'selenium-webdriver/http';
import * as _ from 'lodash';

@Component({
	selector: 'app-file-upload',
	templateUrl: './file-upload.component.html',
	styleUrls: ['./file-upload.component.scss'],
	providers: [CommonFunctionsService]
})
export class FileUploadComponent implements OnInit {
	constructor(public dialog: MatDialog, public commonFunction: CommonFunctionsService) { }

	ngOnInit() { }
	openDialog() {
		const dialogRef = this.dialog.open(DialogContentExampleDialog, {
			height: '88%',
			width: '80%'
		});
		dialogRef.afterClosed().subscribe((result) => { });
	}
}

@Component({
	selector: 'dialog-content-example-dialog',
	templateUrl: 'dialog-content-example-dialog.html',
	styleUrls: ['./file-upload.component.scss']
})
export class DialogContentExampleDialog {
	@ViewChild(ApplicantGridComponent) applicantGrid: ApplicantGridComponent;
	applicantObject: any;
	comapnyObject: any;
	applicantCode = '';
	companyCode = '';
	objectModel = '';
	isApplicantObject = false;
	isComapnyObject = false;
	public hasBaseDropZoneOver = false;
	public hasAnotherDropZoneOver = false;
	documentUploadRoute = environment.tradeAppRoot + environment.documentUploadRoute;
	documentMasterType = environment.tradeAppRoot + environment.documentTypeMaster;
	documentBasedOnUser = environment.tradeAppRoot + environment.documentBasedOnUser;
	selectedDocumentType = '';
	documentMasterTypeArray: string[] = [];
	imagePath = [];
	alreadyUploadedRaw = [];
	uploadedImages = [];
	loggedUserName = this.commonFunction.parseApplicantInfoFromlocalStorage()[5];
	loggedUserId = this.commonFunction.parseApplicantInfoFromlocalStorage()[6];
	isUploadedImagesShown = false;
	form: FormGroup;
	uploadProgress: number;
	uploadProgressArray: number[] = [0];
	progressForNotification: number;
	isNotificationShown = false;

	singleFileUploadCount = 0;
	pendingUploadCount = 0;

	constructor(
		private apiHandler: ApiHandlerService,
		public commonFunction: CommonFunctionsService,
		private sanitizer: DomSanitizer,
		private emitHandler: EmitterHandlerService,
		private formBuilder: FormBuilder,
		private matSnackBar: MatSnackBar
	) {
		this.form = this.formBuilder.group({
			documentTypeArray: this.formBuilder.array([])
		});
	}
	public uploader: FileUploader = new FileUploader({});
	onAddDocumentsType() {
		const control = new FormGroup({
			documentType: new FormControl('')
		});
		(<FormArray>this.form.get('documentTypeArray')).push(control);
	}
	ngOnInit() {
		this.uploader.onAfterAddingFile = (fileItem) => {
			this.uploadProgressArray = [];
			const url = window.URL ? window.URL.createObjectURL(fileItem._file) : (window as any).webkitURL.createObjectURL(fileItem._file);
			this.pendingUploadCount = this.uploader.queue.length;
			this.imagePath.push(url);
			this.onAddDocumentsType();
			document.body.scrollTop = 0;
		};
		this.emitHandler.passApplicantId.pipe(take(1)).subscribe((inputObject) => {
			this.findObjectModel(inputObject);

			if (this.isApplicantObject) {
				this.applicantObject = inputObject;
				this.applicantCode = inputObject.applicantCode;
			}
			if (this.isComapnyObject) {
				this.comapnyObject = inputObject;
				this.companyCode = inputObject.companyCode;
			}
		});

		this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
			alert('File uploaded successfully');
		};
		this.uploader.onBeforeUploadItem = (item) => {
			this.saveImages();
			// Here S3 image upload code.
		};
		this.getDocumentTypeMaster();
	}

	ngAfterViewInit(): void {
		// setTimeout(() => this.getUploadedImages(), 1000);
	}

	removePreview(index: number) {
		this.imagePath.splice(index, 1);
		this.uploadProgressArray.splice(index, 1);
		this.pendingUploadCount--;
		(<FormArray>this.form.get('documentTypeArray')).removeAt(index);
	}

	getDocumentTypeMaster() {
		this.apiHandler.getApiRequest(this.documentMasterType).subscribe((data) => {
			this.documentMasterTypeArray = data;
		});
	}
	removeAllPreview() {
		this.pendingUploadCount = 0;
		this.imagePath = [];
		this.uploadProgressArray = [];
		while ((<FormArray>this.form.get('documentTypeArray')).length !== 0) {
			(<FormArray>this.form.get('documentTypeArray')).removeAt(0);
		}
	}
	saveImages(index?: number) {
		this.isNotificationShown = false;
		this.uploadProgressArray = [];
		const fileCount: number = this.uploader.queue.length;

		if (fileCount > 0) {
			this.uploader.queue.forEach((val, i, array) => {
				const fileReader = new FileReader();

				fileReader.onloadend = (e) => {
					const imageData = <string>fileReader.result;
					const rawData = imageData.split('base64,');
					let imageBase64Data: string;
					if (rawData.length > 1) {
						imageBase64Data = rawData[1];
						const updateIdForFormData = {};
						updateIdForFormData['Org_ID'] = this.commonFunction.parseApplicantInfoFromlocalStorage()[2];
						updateIdForFormData['Unit_Org_ID'] = this.commonFunction.parseApplicantInfoFromlocalStorage()[3];
						updateIdForFormData['Branch_ID'] = this.commonFunction.parseApplicantInfoFromlocalStorage()[4];
						updateIdForFormData['ContentType'] = 'PNG';
						updateIdForFormData['UploadCode'] = '564';
						updateIdForFormData['created_By'] = this.loggedUserName + this.loggedUserId;
						if (this.applicantCode !== '') {
							updateIdForFormData['applicationCode'] = this.applicantCode;
						}
						if (this.companyCode !== null) {
							updateIdForFormData['companyCode'] = this.companyCode;
						}
						updateIdForFormData['data'] = imageBase64Data;
						if (index !== undefined && index !== null) {
							const documentFormArray = (<FormArray>this.form.get('documentTypeArray')).at(index).value.documentType;
							updateIdForFormData['DocumentType'] = documentFormArray;
							if (i === index) {
								this.pendingUploadCount--;
								this.singleFileUploadCount++;
								this.apiHandler.postApiRequestForFileUpload(this.documentUploadRoute, JSON.stringify(updateIdForFormData)).subscribe((data) => {
									if (data.type === HttpEventType.UploadProgress) {
										this.commonFunction.isLoading.next(true);
										this.uploadProgress = data.loaded / data.total * 100;
										this.uploadProgressArray = [];
										this.uploadProgressArray[0] = this.uploadProgress;
									}
									this.openSnackBar();
								});

								this.alreadyUploadedRaw.push(imageBase64Data);
							}
						} else {
							const documentFormArray = (<FormArray>this.form.get('documentTypeArray')).at(i).value.documentType;
							updateIdForFormData['DocumentType'] = documentFormArray;
							if (!this.alreadyUploadedRaw.includes(imageBase64Data)) {
								this.pendingUploadCount--;
								this.alreadyUploadedRaw.push(imageBase64Data);
								this.apiHandler.postApiRequestForFileUpload(this.documentUploadRoute, JSON.stringify(updateIdForFormData)).subscribe((data) => {
									if (data.type === HttpEventType.UploadProgress) {
										this.commonFunction.isLoading.next(true);
										this.uploadProgress = data.loaded / data.total * 100;
										this.uploadProgressArray[i] = this.uploadProgress;
									}
									this.openSnackBar();
								});
							}
						}
					}
				};
				fileReader.readAsDataURL(val._file);
			});
		}
	}

	getUploadedImages() {
		let urlBasedOnUser = this.documentBasedOnUser;
		let imageArray: any;
		this.uploadedImages = [];
		if (this.companyCode !== '') {
			urlBasedOnUser = urlBasedOnUser.concat('/', this.companyCode);
			this.apiHandler.getApiRequestForFileDownload(urlBasedOnUser).subscribe((data) => {
				this.commonFunction.isLoading.next(true);
				if (data.type === HttpEventType.DownloadProgress) {
					this.commonFunction.isLoading.next(true);
				} else if (data.type === HttpEventType.Response) {
					if (data.statusText === 'OK') {
						imageArray = data.body;
						imageArray.forEach((element) => {
							this.uploadedImages.push({ image: 'data:image/png;base64, ' + element.dataSmall, id: element.documentUpID });
						});
						if (this.uploadedImages.length === 0) {
							this.matSnackBar.open('Notice : No images have been uploaded !!', 'Close', {
								duration: 3000
							});
						}
						this.commonFunction.isLoading.next(false);
					}
				}
			});
		} else if (this.applicantCode !== '') {
			urlBasedOnUser = urlBasedOnUser.concat('/', this.applicantCode);
			this.apiHandler.getApiRequest(urlBasedOnUser).subscribe((data) => {
				this.commonFunction.isLoading.next(true);
				data.forEach((data) => {
					this.uploadedImages.push({ image: 'data:image/png;base64, ' + data.dataSmall, id: data.documentUpID });
					this.isUploadedImagesShown = true;
					this.commonFunction.isLoading.next(false);
				});
				if (this.uploadedImages.length === 0) {
					this.matSnackBar.open('Notice : No images have been uploaded !!', 'Close', {
						duration: 3000
					});
				}
			});
		}
	}
	public fileOverBase(e: any): void {
		this.hasBaseDropZoneOver = e;
	}

	public fileOverAnother(e: any): void {
		this.hasAnotherDropZoneOver = e;
	}
	findObjectModel(objType: any) {
		if (objType.applicantID !== undefined) {
			this.isApplicantObject = true;
		}
		if (objType.companyID !== undefined) {
			this.isComapnyObject = true;
		}
	}
	findAverageOfArray(uploadProgress: number[]) {
		let sum = 0;
		let average = 0;

		uploadProgress.forEach((element) => {
			sum += element;
		});
		if (uploadProgress.length === 1) {
			average = sum;
		} else {
			average = sum / (this.uploader.queue.length - this.pendingUploadCount - this.singleFileUploadCount);
		}

		if (Number.isNaN(Math.round(average)) || Math.round(average) === Infinity) {
			return 0;
		}
		this.progressForNotification = Math.round(average);
		return Math.round(average);
	}

	openSnackBar() {
		if (this.progressForNotification === 100) {
			if (this.isNotificationShown === false) {
				this.matSnackBar.open('File Uploaded Successfully !!', 'Close', {
					duration: 3000
				});
				this.isNotificationShown = true;
			}
			this.commonFunction.isLoading.next(false);
		}
	}

	deleteImage(imageId: number) {
		this.apiHandler.deleteApiRequest(this.documentUploadRoute + '/' + imageId).subscribe((data: any) => {
			this.matSnackBar.open('Image ' + data.documentType + ' deleted successfully !!', 'Close', {
				duration: 3000
			});
			/**
			 * This function removes the image from the preview 
			 */
			this.uploadedImages = this.uploadedImages.filter(function (item) {
				return item.id !== imageId;
			});
		});
	}
}
