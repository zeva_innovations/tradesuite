import { CommonFunctionsService } from './../../services/common-functions.service';
import { FileUploadComponent } from './../file-upload/file-upload.component';
import { Observable, of } from 'rxjs';
import { map, first, catchError, finalize } from 'rxjs/operators';
import { Component, OnInit, AfterViewInit, HostListener, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatTable, MatSort } from '@angular/material';
import { ApiHandlerService } from '../../services/api-handler.service';
import { HttpClient } from '@angular/common/http';
import { DataSourceServiceService } from '../../services/data-source-service.service';
import { environment } from '../../../environments/environment';
import { CustomerDetailsFormService } from '../../services/customer-details-form.service';
import { EmitterHandlerService } from '../../services/emitter-handler.service';
import * as moment from 'moment';

@Component({
	selector: 'app-applicant-grid',
	templateUrl: './applicant-grid.component.html',
	styleUrls: ['./applicant-grid.component.scss'],
	providers: [CommonFunctionsService]
})
@HostListener('click')
export class ApplicantGridComponent implements AfterViewInit {
	@ViewChild(ApplicantGridComponent) applicantGrid: ApplicantGridComponent;
	loggedUserName = this.commonFunction.parseApplicantInfoFromlocalStorage()[5];
	loggedUserId = this.commonFunction.parseApplicantInfoFromlocalStorage()[6];

	applicantsRoute = environment.tradeAppRoot + environment.getApplicants;
	applicantsByUser = environment.tradeAppRoot + environment.getApplicants + '/get/' + this.loggedUserName + this.loggedUserId;

	rootAppUrl = environment.tradeAppRoot;
	applicantUrl = this.rootAppUrl + environment.getApplicants;
	dataSource = new MatTableDataSource([]);

	displayedColumns = ['applicantID', 'firstName', 'emailID', 'phoneNo', 'country', 'actions'];
	@ViewChild(MatSort) sort: MatSort;
	@ViewChild(MatPaginator) paginator: MatPaginator;

	constructor(
		private apiHandler: ApiHandlerService,
		private commonFunction: CommonFunctionsService,
		private customerForm: CustomerDetailsFormService,
		private eventHandler: EmitterHandlerService,
		private dataService: DataSourceServiceService
	) {}

	ngOnInit() {
		this.dataService.loadApplicants(this.applicantsByUser);
		this.dataService.applicantSubject.subscribe((val) => {
			this.dataSource = new MatTableDataSource(val);
			this.dataSource.sort = this.sort;
			this.dataSource.paginator = this.paginator;
			return val;
		});
	}
	ngAfterViewInit() {
		setTimeout(() => {
			this.dataSource.sort = this.sort;
			this.dataSource.paginator = this.paginator;
		}, 2000);
	}
	add(body: string) {
		this.dataService.postApplicants(this.applicantsRoute, body);
		this.dataSource.sort = this.sort;
		this.dataSource.paginator = this.paginator;
	}

	edit(applicantID: number) {
		const getApplicantInfo = this.applicantsRoute + '/GetId/' + applicantID;
		this.apiHandler.getApiRequest(getApplicantInfo).pipe(catchError(() => of([]))).subscribe((applicants: any) => {
			let applicantInfo: any;
			applicantInfo = { customerDetails: {} };
			applicantInfo.customerDetails['firstName'] = applicants.firstName;
			applicantInfo.customerDetails['surName'] = applicants.surName;
			applicantInfo.customerDetails['emailID'] = applicants.emailID;
			applicantInfo.customerDetails['phoneNo'] = applicants.phoneNo;
			applicantInfo.customerDetails['dob'] = moment(applicants.dob);
			applicantInfo.customerDetails['city'] = applicants.city;
			applicantInfo.customerDetails['country'] = applicants.country;
			applicantInfo.customerDetails['state'] = applicants.state;
			applicantInfo.customerDetails['gender'] = applicants.gender;
			applicantInfo.customerDetails['passportNo'] = applicants.passportNo;
			applicantInfo.customerDetails['socialSecurityNo'] = applicants.socialSecurityNo;
			applicantInfo.customerDetails['remark1'] = applicants.remark1;
			this.customerForm.formGroup.setValue(applicantInfo);
			this.eventHandler.toggleEditMode();
			this.eventHandler.emitApplicantId(applicantID);
		});
	}
	update(parameter: any, body: string) {
		this.dataService.putApplicants(this.applicantsRoute, parameter, body);
	}
	delete(applicantID: number) {
		this.dataService.deleteApplicants(this.applicantsRoute, applicantID);
	}
	attach(applicantID: any) {
		this.eventHandler.emitApplicantId(applicantID);
	}
	applyFilter(filterValue: string) {
		this.dataSource.filter = filterValue.trim().toLowerCase();
	}
	companyDetails(value: any) {
		this.commonFunction.applicantCodeForCompanyRegistration = value;
	}
}
