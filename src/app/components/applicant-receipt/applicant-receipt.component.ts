import { ApiHandlerService } from './../../services/api-handler.service';
import { Component, OnInit } from '@angular/core';
import * as jspdf from 'jspdf';
import html2canvas from 'html2canvas';
import { environment } from 'src/environments/environment';
import * as _ from 'lodash';
import { analyzeAndValidateNgModules } from '@angular/compiler';

@Component({
	selector: 'app-applicant-receipt',
	templateUrl: './applicant-receipt.component.html',
	styleUrls: ['./applicant-receipt.component.scss']
})
export class ApplicantReceiptComponent implements OnInit {
	Assign_DocumentForIndustryAPI = environment.tradeAppRoot + environment.assignDocumentForIndustry;
	constructor(private apiHandler: ApiHandlerService) {}

	ngOnInit() {
		this.getDocumentsForIndustry();
	}

	public captureScreen() {
		const data = document.getElementById('contentToConvert');
		html2canvas(data).then((canvas) => {
			// Few necessary setting options
			const imgWidth = 208;
			const pageHeight = 295;
			const imgHeight = canvas.height * imgWidth / canvas.width;
			const heightLeft = imgHeight;

			const contentDataURL = canvas.toDataURL('image/png');
			const pdf = new jspdf('p', 'mm', 'a4'); // A4 size page of PDF
			const position = 0;
			pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight);
			pdf.save('Receipt.pdf'); // Generated PDF
		});
	}

	getDocumentsForIndustry() {
		let dataArray = [];
		this.apiHandler.getApiRequest(this.Assign_DocumentForIndustryAPI).subscribe((data) => {
			dataArray = _.filter(data, function(user) {
				return user.receiptNameCode === '42-FRESHER ' || user.industryNameCode === '8-Wholesalers';
			});
		});
	}
}
