import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicantReceiptComponent } from './applicant-receipt.component';

describe('ApplicantReceiptComponent', () => {
  let component: ApplicantReceiptComponent;
  let fixture: ComponentFixture<ApplicantReceiptComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicantReceiptComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicantReceiptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
