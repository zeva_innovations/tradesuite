import { CommonFunctionsService } from 'src/app/services/common-functions.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, FormArray } from '@angular/forms';
import { ApiHandlerService } from 'src/app/services/api-handler.service';
import { environment } from 'src/environments/environment';
import { MatDialog } from '@angular/material';
import { EmitterHandlerService } from 'src/app/services/emitter-handler.service';

@Component({
	selector: 'form-collection-dialog',
	templateUrl: './form-collection-dialog.html'
})
export class FormCollectionDialog implements OnInit {
	constructor(public dialog: MatDialog, public commonFunction: CommonFunctionsService) {}

	ngOnInit() {}
	openDialog() {
		const dialogRef = this.dialog.open(FormCollectionComponent, {
			height: '80%',
			width: '80%'
		});
		dialogRef.afterClosed().subscribe((result) => {});
	}
}

@Component({
	selector: 'app-form-collection',
	templateUrl: './form-collection.component.html',
	styleUrls: ['./form-collection.component.scss']
})
export class FormCollectionComponent implements OnInit {
	// https://stackblitz.com/edit/angular-ghgppz?file=src%2Fapp%2Fapp.component.ts
	form: FormGroup;
	documentCheckListArray = [];
	existingDocumentsListArray = [];
	compmayDetailArray: any = [];
	companyCode = '';
	documentMasterType = environment.tradeAppRoot + environment.documentTypeMaster;
	getCompanyFromCompanyCode = environment.tradeAppRoot + environment.getCompanyFromCompanyCode;
	getDocCheckList = environment.tradeAppRoot + environment.documentCheckList;
	postCheckBox = environment.tradeAppRoot + environment.postFormCheckBox;
	putCheckBox = environment.tradeAppRoot + environment.putFormCheckBox;
	existingFormForCompany = environment.tradeAppRoot + environment.existingFormForCompany;
	companyRoute = environment.tradeAppRoot + environment.getCompanyDetails;
	isAllCheckBoxSelected = false;
	enableSubmit = false;

	constructor(
		private formBuilder: FormBuilder,
		private apiHandler: ApiHandlerService,
		public commonFunction: CommonFunctionsService,
		private emitHandler: EmitterHandlerService,
		private dialogRef: MatDialog
	) {}

	ngOnInit() {
		const formControls = this.documentCheckListArray.map((control) => new FormControl(false));
		const selectAllControl = new FormControl(false);
		this.form = this.formBuilder.group({
			documentCheckListArray: new FormArray(formControls),
			selectAll: selectAllControl,
			inputCompanyCode: new FormControl({ value: this.companyCode, disabled: true })
		});

		this.onChanges();

		this.emitHandler.passApplicantId.subscribe((inputObject) => {
			this.companyCode = inputObject;
			this.form.patchValue({ inputCompanyCode: this.companyCode });
		});
	}

	onChanges(): void {
		this.form.get('selectAll').valueChanges.subscribe((bool) => {
			this.form.get('documentCheckListArray').patchValue(Array(this.documentCheckListArray.length).fill(bool), { emitEvent: false });
		});

		this.form.get('documentCheckListArray').valueChanges.subscribe((val) => {
			const allSelected = val.every((bool) => bool);
			if (this.form.get('selectAll').value !== allSelected) {
				this.form.get('selectAll').patchValue(allSelected, { emitEvent: false });
			}
		});
	}

	submit() {
		const selectedPreferences = this.form.value;
		alert(JSON.stringify(selectedPreferences));
	}
	ngAfterViewInit(): void {
		const formControls = this.documentCheckListArray.map((control) => new FormControl(false));
		const selectAllControl = new FormControl(false);
		this.form = this.formBuilder.group({
			documentCheckListArray: new FormArray(formControls),
			selectAll: selectAllControl,
			inputCompanyCode: new FormControl({ value: this.companyCode, disabled: true })
		});
		this.onChanges();
	}
	findCompanyDetails(companyCode: string) {
		this.isAllCheckBoxSelected = false;
		this.apiHandler.getApiRequest(this.getCompanyFromCompanyCode + '/' + companyCode).subscribe((data) => {
			this.compmayDetailArray = data[0];

			const urlForExistingCompany = this.existingFormForCompany + '/' + companyCode;
			this.apiHandler.getApiRequest(urlForExistingCompany).subscribe((data) => {
				this.documentCheckListArray = data;
				this.existingDocumentsListArray = data;

				const alreadySelectedCheckBoxes = this.documentCheckListArray.map((data) => data.isSelectedFlag);

				if (alreadySelectedCheckBoxes.length > 0 && !alreadySelectedCheckBoxes.includes(false)) {
					alert('We have already Collected Documents !!');
					this.isAllCheckBoxSelected = true;
				}
				if (this.documentCheckListArray.length !== 0) {
					const formControls = this.documentCheckListArray.map((control, index) => new FormControl(control.isSelectedFlag));
					const selectAllControl = new FormControl(false);
					this.form = this.formBuilder.group({
						documentCheckListArray: new FormArray(formControls),
						selectAll: selectAllControl,
						inputCompanyCode: new FormControl({ value: this.companyCode, disabled: true })
					});
				} else {
					const array: any = this.compmayDetailArray;
					this.getDocumentCheckList(array.applicationinfo6);
				}
				this.enableSubmit = true;
			});
		});
	}

	getDocumentCheckList(licenseType: any) {
		const url = this.getDocCheckList + '/' + licenseType;
		this.apiHandler.getApiRequest(url).subscribe((data) => {
			this.documentCheckListArray = data;
			const formControls = this.documentCheckListArray.map((control) => new FormControl(false));
			const selectAllControl = new FormControl(false);
			this.form = this.formBuilder.group({
				documentCheckListArray: new FormArray(formControls),
				selectAll: selectAllControl,
				inputCompanyCode: new FormControl({ value: this.companyCode, disabled: true })
			});
			this.onChanges();
		});
	}

	submitDocumentList(formObject: any) {
		const compmayDetailArray: any = this.compmayDetailArray;
		const companyCode = compmayDetailArray.companyCode;
		const companyID = compmayDetailArray.companyID;
		const checkBoxes = formObject['documentCheckListArray'];
		const formPostData: any[] = [];

		if (this.existingDocumentsListArray.length > 0) {
			this.updateDocumentCheckBox(formObject);

			if (!checkBoxes.includes('false')) {
				const body = { companyID: companyID, isProcessed: true };
				this.apiHandler.putApiRequest(this.companyRoute + '/' + companyID, JSON.stringify(body)).subscribe((data) => {
					this.dialogRef.closeAll();
				});
			}
		} else {
			this.documentCheckListArray.forEach((data, index) => {
				formPostData.push({
					docNameCode: data.docNameCode,
					isSelectedFlag: checkBoxes[index],
					companyCode: companyCode
				});
			});
			const bodyTobePosted = JSON.stringify(formPostData);
			const urlFormPost = this.postCheckBox + '/' + companyCode;
			this.apiHandler.postApiRequest(urlFormPost, bodyTobePosted).subscribe((data) => {});
			if (!checkBoxes.includes('false')) {
				const body = { companyID: companyID, isProcessed: true };
				this.apiHandler.putApiRequest(this.companyRoute + '/' + companyID, JSON.stringify(body)).subscribe((data) => {
					this.dialogRef.closeAll();
				});
			}
		}
	}

	updateDocumentCheckBox(formObject: any) {
		const checkBoxes: string[] = formObject.documentCheckListArray;
		const compmayDetailArray: any = this.compmayDetailArray;
		const companyCode = compmayDetailArray.companyCode;
		const updateCheckBoxUrl = this.putCheckBox + '/' + companyCode;

		let updateArray: any[] = [];
		updateArray = this.documentCheckListArray.map((data, index) => {
			return {
				formCollectionDocsTypeID: data.formCollectionDocsTypeID,
				formCollectionCode: data.formCollectionCode,
				isClosed: data.isClosed,
				isSelectedFlag: checkBoxes[index],
				status: data.status,
				isActive: data.isActive,
				companyCode: data.companyCode,
				docNameCode: data.docNameCode,
				created_By: data.created_By,
				created_Date: data.created_Date,
				edited_By: data.edited_By,
				edited_Date: data.edited_Date,
				revision_ID: data.revision_ID
			};
		});

		this.apiHandler.putApiRequest(updateCheckBoxUrl, JSON.stringify(updateArray)).subscribe((data) => {});
	}
}
