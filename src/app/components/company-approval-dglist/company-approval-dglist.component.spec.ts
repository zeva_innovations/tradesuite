import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyApprovalDGListComponent } from './company-approval-dglist.component';

describe('CompanyApprovalDGListComponent', () => {
  let component: CompanyApprovalDGListComponent;
  let fixture: ComponentFixture<CompanyApprovalDGListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyApprovalDGListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyApprovalDGListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
