import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyRenewalGridComponent } from './company-renewal-grid.component';

describe('CompanyRenewalGridComponent', () => {
  let component: CompanyRenewalGridComponent;
  let fixture: ComponentFixture<CompanyRenewalGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyRenewalGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyRenewalGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
