import { CommonFunctionsService } from 'src/app/services/common-functions.service';
import { CompanyDetailsFormService } from './../../services/company-details-form.service';
import { Component, OnInit, Input, ViewChild, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { MyErrorStateMatcher } from 'src/app/services/customer-details-form.service';
import { FormGroup } from '@angular/forms';
import { DataSourceServiceService } from 'src/app/services/data-source-service.service';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { environment } from 'src/environments/environment';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { ApiHandlerService } from 'src/app/services/api-handler.service';
import { EmitterHandlerService } from 'src/app/services/emitter-handler.service';

@Component({
	selector: 'app-company-renewal-grid',
	templateUrl: './company-renewal-grid.component.html',
	styleUrls: ['./company-renewal-grid.component.scss']
})
export class CompanyRenewalGridComponent implements OnInit {
	constructor(
		private apiHandler: ApiHandlerService,
		private companyDetailsFormService: CompanyDetailsFormService,
		private dataService: DataSourceServiceService,
		private eventHandler: EmitterHandlerService,
		public commonFunction: CommonFunctionsService,
		private cdr: ChangeDetectorRef
	) { }
	displayedColumns = ['companyID', 'companyName', 'licenseNumber', 'registrationNumber', 'companyType', 'companyAreaType', 'industryType', 'country', 'actions'];
	companyRoute = environment.tradeAppRoot + environment.getCompanyDetails;
	companyTypeMaster = environment.tradeAppRoot + environment.companyTypeMaster;
	companyAreaTypeMaster = environment.tradeAppRoot + environment.companyAreaType;
	industryTypeMaster = environment.tradeAppRoot + environment.industryType;

	loggedUserName = this.commonFunction.parseApplicantInfoFromlocalStorage()[5];
	loggedUserId = this.commonFunction.parseApplicantInfoFromlocalStorage()[6];
	applicantsByUser = this.companyRoute + '/get/' + this.loggedUserName + this.loggedUserId;
	@Input() group: FormGroup;
	myjson: any = JSON;
	dataSource = new MatTableDataSource([]);
	editMode = false;
	companyId = 0;
	companyType: string[] = [];
	companyAreaType: string[] = [];
	industryType: string[] = [];
	submitCounter = 0;
	formDataToBeSubmitted = '';
	companyDetailsForRenewal: any;

	@ViewChild(MatSort) sort: MatSort;
	@ViewChild(MatPaginator) paginator: MatPaginator;
	matchFinder = new MyErrorStateMatcher();

	ngOnInit() {
		this.dataService.loadApplicants(this.applicantsByUser);
		this.dataService.applicantSubject.subscribe((val) => {
			this.dataSource = new MatTableDataSource(val);
			this.dataSource.sort = this.sort;
			this.dataSource.paginator = this.paginator;
			return val;
		});
		this.getCompany();
		this.getCompanyAreaType();
		this.getIndustryType();
		this.companyDetailsFormService.formGroup.patchValue({ companyDetails: { applicantCode: this.commonFunction.applicantCodeForCompanyRegistration } });
	}
	get form(): FormGroup {
		return this.companyDetailsFormService.formGroup;
	}
	applicantCode(value: any) {
		this.commonFunction.applicantCodeForCompanyRegistration = value;
	}

	add(body: string) {
		this.submitCounter = this.submitCounter + 1;
		body['created_By'] = this.loggedUserName + this.loggedUserId;
		body['Org_ID'] = this.commonFunction.parseApplicantInfoFromlocalStorage()[2];
		body['Unit_Org_ID'] = this.commonFunction.parseApplicantInfoFromlocalStorage()[3];
		body['Branch_ID'] = this.commonFunction.parseApplicantInfoFromlocalStorage()[4];
		body['applicantCode'] = this.applicantCode;
		body['barCode'] = this.applicantCode + '_' + this.commonFunction.getUTCNow().toString();

		if (this.submitCounter === 1) {
			this.formDataToBeSubmitted = body;
		}
		if (this.applicantCode !== undefined && this.applicantCode !== null && (this.formDataToBeSubmitted !== body || this.submitCounter === 1)) {
			this.dataService.postApplicants(this.companyRoute, body);
		} else if (this.applicantCode === undefined || this.applicantCode === null) {
			alert('You cannot create a company without Applicant Code');
		} else {
			alert('You are submitting the same form details!!');
		}
		this.dataSource.sort = this.sort;
		this.dataSource.paginator = this.paginator;
	}
	edit(applicantID: number) {
		const getApplicantInfo = this.companyRoute + '/GetId/' + applicantID;
		this.apiHandler.getApiRequest(getApplicantInfo).pipe(catchError(() => of([]))).subscribe((company: any) => {
			let companyInfo: any;
			companyInfo = { companyDetails: {} };
			companyInfo.companyDetails['companyName'] = company.companyName;
			companyInfo.companyDetails['applicantCode'] = company.applicantCode;
			companyInfo.companyDetails['tin'] = company.tin;
			companyInfo.companyDetails['emailID'] = company.emailID;
			companyInfo.companyDetails['phoneNo'] = company.phoneNo;
			companyInfo.companyDetails['ieRegCertificateNo'] = company.ieRegCertificateNo;
			companyInfo.companyDetails['address1'] = company.address1;
			companyInfo.companyDetails['address2'] = company.address2;
			companyInfo.companyDetails['address3'] = company.address3;
			companyInfo.companyDetails['city'] = company.city;
			companyInfo.companyDetails['country'] = company.country;
			companyInfo.companyDetails['state'] = company.state;
			companyInfo.companyDetails['pin'] = company.pin;
			companyInfo.companyDetails['companyType'] = company.companyType;
			companyInfo.companyDetails['companyAreaType'] = company.companyAreaType;
			companyInfo.companyDetails['industryType'] = company.industryType;
			companyInfo.companyDetails['remark'] = company.remark;

			this.companyDetailsFormService.formGroup.setValue(companyInfo);
			this.editMode = true;
			this.companyId = applicantID;
		});
	}

	update(parameter: any, body: string) {
		body['companyID'] = parameter;
		body['edited_By'] = this.loggedUserName + this.loggedUserId;
		this.dataService.putApplicants(this.companyRoute, parameter, body);
	}
	delete(applicantID: number) {
		this.dataService.deleteApplicants(this.companyRoute, applicantID);
	}
	attach(applicantID: any) {
		this.eventHandler.emitApplicantId(applicantID);
	}
	applyFilter(filterValue: string) {
		this.dataSource.filter = filterValue.trim().toLowerCase();
	}
	reset() {
		this.form.reset();
		this.applicantCode = null;
	}
	getCompany() {
		this.apiHandler.getApiRequest(this.companyTypeMaster).subscribe((companyTypeMaster) => {
			this.companyType = companyTypeMaster;
		});
	}
	getCompanyAreaType() {
		this.apiHandler.getApiRequest(this.companyAreaTypeMaster).subscribe((companyAreaType) => {
			this.companyAreaType = companyAreaType;
		});
	}
	getIndustryType() {
		this.apiHandler.getApiRequest(this.industryTypeMaster).subscribe((industryType) => {
			this.industryType = industryType;
		});
	}

	ngOnDestroy(): void {
		// Called once, before the instance is destroyed.
		// Add 'implements OnDestroy' to the class.
		this.reset();
		this.editMode = false;
	}

	setRenewal() {
		this.commonFunction.isRenewalFlow = true;
	}
	setCompanyForRenewal(value: any) {
		this.commonFunction.companyDetailsForRenewal = value;
	}
}
