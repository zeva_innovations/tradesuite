import { CommonFunctionsService } from 'src/app/services/common-functions.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import * as _ from 'lodash';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { MatSidenav } from '@angular/material';

@Component({
	selector: 'app-sidenav-bar',
	templateUrl: './sidenav-bar.component.html',
	styleUrls: ['./sidenav-bar.component.scss']
})
export class SidenavBarComponent implements OnInit {
	constructor(public commonFunction: CommonFunctionsService, public authService: AuthenticationService) { }
	loginRoute: ROUTE[] = [
		{
			icon: 'person',
			route: '/login',
			title: 'Login'
		}
	];
	myWorkRoutes: ROUTE[] = [
		{
			icon: 'person_pin',
			route: '/applicantregistration',
			title: 'Applicant Registration'
		},
		{
			icon: 'card_travel',
			route: '/companyregistration',
			title: 'Company Registration'
		},
		{
			icon: 'card_travel',
			route: '/companylists',
			title: 'Company Lists'
		},
		{
			icon: 'assignment_returned',
			route: '/applicationissue',
			title: 'Application Issue'
		},
		{
			icon: 'assignment_turned_in',
			route: '/formcollect',
			title: 'Form Collection'
		},
		{
			icon: 'local_printshop',
			route: '/scandepartment',
			title: 'Scanning Department'
		},
		{
			icon: 'create',
			route: '/dataentry',
			title: 'Data Entry'
		},
		{
			icon: 'done',
			route: '/companyapproval',
			title: 'Director Approval'
		},
		{
			icon: 'money',
			route: '/accountone',
			title: 'Account 1'
		},
		{
			icon: 'attach_money',
			route: '/accounttwo',
			title: 'Account 2'
		},
		{
			icon: 'done_all',
			route: '/companydgapproval',
			title: 'Final Approval'
		},
		{
			icon: 'print',
			route: '/cardprint',
			title: 'Card Print'
		},
		{
			icon: 'credit_card',
			route: '/cardwrite',
			title: 'Card Write'
		},
		{
			icon: 'done_outline',
			route: '/cardissue',
			title: 'Card Issue'
		}
	];
	logOutRoutes: ROUTE[] = [
		{
			icon: 'cancel',
			route: '/login',
			title: 'Logout'
		}
	];

	mainMastersRoutes: ROUTE[] = [
		{
			icon: 'folder',
			route: 'MainMaster',
			title: 'Masters'
		}
	];

	ngOnInit() { }
	public isAuthenticated() {
		return this.commonFunction.isUserAuthenticated();
	}

	getChangeUserRole() {
		const mainRolesArray = this.commonFunction.getRolesBasedOnMenu()[0];
		const modifiedRoleArray = [];
		mainRolesArray.forEach((element) => {
			modifiedRoleArray.push({ route: '/' + element });
		});
		if (!mainRolesArray.includes('MainMaster')) {
			this.mainMastersRoutes = [];
		}
		this.myWorkRoutes = _.intersectionBy(this.myWorkRoutes, modifiedRoleArray, 'route');

		if (mainRolesArray.length === 0) {
			this.myWorkRoutes = [];
		}
	}

	logout() {
		this.authService.logout();
	}
}

interface ROUTE {
	icon?: string;
	route?: string;
	title?: string;
}
