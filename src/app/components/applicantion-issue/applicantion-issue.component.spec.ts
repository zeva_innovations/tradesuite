import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicantionIssueComponent } from './applicantion-issue.component';

describe('ApplicantionIssueComponent', () => {
  let component: ApplicantionIssueComponent;
  let fixture: ComponentFixture<ApplicantionIssueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicantionIssueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicantionIssueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
