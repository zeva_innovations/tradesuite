import { Component, OnInit, Output, Input } from '@angular/core';
import { MatDialog, MatTableDataSource } from '@angular/material';
import { ApiHandlerService } from 'src/app/services/api-handler.service';
import { DataSourceServiceService } from 'src/app/services/data-source-service.service';
import { EmitterHandlerService } from 'src/app/services/emitter-handler.service';
import { environment } from 'src/environments/environment';
import * as _ from 'lodash';
import { CommonFunctionsService } from 'src/app/services/common-functions.service';
import { BehaviorSubject } from 'rxjs';
import { take } from 'rxjs/operators';
@Component({
	selector: 'app-applicantion-issue',
	templateUrl: './applicantion-issue.component.html',
	styleUrls: ['./applicantion-issue.component.scss']
})
export class ApplicantionIssueComponent implements OnInit {
	companyDetailsForApplicationIssue = 'ss';
	constructor(public dialog: MatDialog) { }

	ngOnInit() { }

	openDialog() {
		const dialogRef = this.dialog.open(ApplicationIssueDialog, {
			height: '60%',
			width: '70%'
		});
		dialogRef.afterClosed().subscribe((result) => { });
	}
}

@Component({
	selector: 'dialog-application-issue',
	templateUrl: 'application-issue-dialog.html'
})
export class ApplicationIssueDialog implements OnInit {
	@Input() companyDetailsForApplicationIssue: string;
	companyRoute = environment.tradeAppRoot + environment.getCompanyDetails;
	applyTypeRouter = environment.tradeAppRoot + environment.applyTypeMaster;
	Assign_DocumentForIndustryAPI = environment.tradeAppRoot + environment.assignDocumentForIndustry;
	getPdfDocumentApi = environment.tradeAppRoot + environment.generatePdfDocument;
	loggedUserName = this.commonFunction.parseApplicantInfoFromlocalStorage()[5];
	loggedUserId = this.commonFunction.parseApplicantInfoFromlocalStorage()[6];
	pendingTabRoute = this.companyRoute + '/isApplicaitonIssued0/' + this.loggedUserName + this.loggedUserId;
	applyTypeMaster: string[] = [];
	applyTypeMasterFiltered: string[] = [];
	selectedLevel: any;
	companyObject = '';
	companyCode = '';
	companyID = 0;
	isDownloadApplicationPdf = false;
	isDownloadOMRPdf = false;
	isDownloadApplicationDoc = false;
	downloadApplicationPdfURL = '';
	downloadOMRPdfURL = '';
	downloadapplicationDocPath = '';
	barCode = '';
	isDropdownsLoaded = false;

	dataSource = new MatTableDataSource([]);

	constructor(
		private apiHandler: ApiHandlerService,
		private dataService: DataSourceServiceService,
		private emitHandler: EmitterHandlerService,
		public commonFunction: CommonFunctionsService
	) { }

	ngOnInit() {
		this.emitHandler.passApplicantId.pipe(take(1)).subscribe((inputObject) => {
			this.companyObject = inputObject;
			this.companyCode = inputObject.companyCode;
			this.companyID = inputObject.companyID;
			this.barCode = inputObject.barCode;
			this.getCompany();
		});
	}
	get selectedOrgMod() {
		return this.selectedLevel;
	}

	set selectedOrgMod(value) {
		this.selectedLevel = value;
	}
	getCompany() {
		this.apiHandler.getApiRequest(this.applyTypeRouter).subscribe((applyTypeMaster) => {
			this.applyTypeMaster = applyTypeMaster;
			this.getAllCompaniesFromUniqueRef(this.barCode, applyTypeMaster);
		});
	}

	getCheckListDocuments(inputDropdown: any) {
		let dataArray = [];
		this.apiHandler.getApiRequest(this.Assign_DocumentForIndustryAPI).subscribe((data) => {
			dataArray = _.filter(data, function (user) {
				return user.receiptNameCode === '42-FRESHER ' || user.industryNameCode === '8-Wholesalers';
			});
		});
	}

	updateIssueReceipt(body: any) {
		// this.getCheckListDocuments(body);
		this.apiHandler.getApiRequest(this.getPdfDocumentApi + '/' + body + '-' + this.companyCode).subscribe((data) => {
			const response: any = data;
			const applicationPDF = response.applicationPath;
			const applicationDocPath = response.applicationDocPath;
			const omrPDF = response.applicationAddOnPath;
			const updateCompanyBody: any = {};
			this.downloadApplicationPdfURL = environment.tradeAppRoot + '/api/Company_RegistrationAPI/GetApplicationformate/' + applicationPDF;
			this.downloadOMRPdfURL = environment.tradeAppRoot + '/api/Company_RegistrationAPI/GetApplicationformate/' + omrPDF;
			this.downloadapplicationDocPath = environment.tradeAppRoot + '/api/Company_RegistrationAPI/GetApplicationformate/' + applicationDocPath;
			this.isDownloadApplicationPdf = true;
			if (body.indexOf('IE REG') !== -1) {
				this.isDownloadOMRPdf = false;
			} else {
				this.isDownloadOMRPdf = true;
			}
			this.isDownloadApplicationDoc = true;
			updateCompanyBody['companyID'] = this.companyID;
			updateCompanyBody['applicationinfo6'] = body;
			this.apiHandler.putApiRequest(this.companyRoute + '/' + this.companyID, updateCompanyBody).subscribe((data) => {
				this.dataService.loadApplicants(this.pendingTabRoute);
			});
		});
	}

	getAllCompaniesFromUniqueRef(barCode: string, masterValues) {
		let allTaskArray: any;
		const alreadyIssuedApplication = [];

		allTaskArray = this.apiHandler.getApiRequest(this.companyRoute + '/getBarcode/' + barCode).subscribe((data) => {
			allTaskArray = data;
			allTaskArray.forEach((data) => {
				if (data.applicationinfo6 !== undefined && data.applicationinfo6 !== null) {
					alreadyIssuedApplication.push(data.applicationinfo6.split('-')[1]);
				}
			});
			let getAllDropDownFromMaster = masterValues;

			getAllDropDownFromMaster = _.filter(getAllDropDownFromMaster, function (user) {
				return (
					!alreadyIssuedApplication.includes(user.applyType) &&
					user.companyType === allTaskArray[0].companyType &&
					user.companyAreaType === allTaskArray[0].companyAreaType
				);
			});

			allTaskArray.forEach((data) => {
				if (data.applicationinfo2 === 'RENEWAL') {
					getAllDropDownFromMaster = _.filter(getAllDropDownFromMaster, function (user) {
						return (user.receiptType === 'RENEWAL' || user.receiptType === 'ADD ON' || user.receiptType === 'LOST');
					});
				} else {
					getAllDropDownFromMaster = _.filter(getAllDropDownFromMaster, function (user) {
						return (user.receiptType !== 'RENEWAL' && user.receiptType !== 'ADD ON' && user.receiptType !== 'LOST');
					});
				}
			});

			this.applyTypeMasterFiltered = getAllDropDownFromMaster;
			this.isDropdownsLoaded = true;
			return this.applyTypeMasterFiltered;
		});
	}
	ngOnDestroy(): void {
		// Called once, before the instance is destroyed.
		// Add 'implements OnDestroy' to the class.
	}
}
