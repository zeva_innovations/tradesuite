import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyListAccountOneComponent } from './company-list-account-one.component';

describe('CompanyListAccountOneComponent', () => {
  let component: CompanyListAccountOneComponent;
  let fixture: ComponentFixture<CompanyListAccountOneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyListAccountOneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyListAccountOneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
