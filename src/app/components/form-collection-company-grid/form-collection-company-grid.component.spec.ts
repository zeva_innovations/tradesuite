import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormCollectionCompanyGridComponent } from './form-collection-company-grid.component';

describe('FormCollectionCompanyGridComponent', () => {
  let component: FormCollectionCompanyGridComponent;
  let fixture: ComponentFixture<FormCollectionCompanyGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormCollectionCompanyGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormCollectionCompanyGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
