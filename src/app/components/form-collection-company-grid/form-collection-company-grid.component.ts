import { CommonFunctionsService } from 'src/app/services/common-functions.service';
import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { ApiHandlerService } from 'src/app/services/api-handler.service';
import { CompanyDetailsFormService } from 'src/app/services/company-details-form.service';
import { DataSourceServiceService } from 'src/app/services/data-source-service.service';
import { EmitterHandlerService } from 'src/app/services/emitter-handler.service';
import { environment } from 'src/environments/environment';
import { FormGroup } from '@angular/forms';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { MyErrorStateMatcher } from 'src/app/services/customer-details-form.service';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Component({
	selector: 'form-collection-company-grid',
	templateUrl: './form-collection-company-grid.component.html',
	styleUrls: ['./form-collection-company-grid.component.scss']
})
export class FormCollectionCompanyGridComponent implements OnInit {
	constructor(
		private apiHandler: ApiHandlerService,
		private companyDetailsFormService: CompanyDetailsFormService,
		private dataService: DataSourceServiceService,
		private eventHandler: EmitterHandlerService,
		private commonFunction: CommonFunctionsService
	) { }
	displayedColumns = [
		'companyID',
		'companyCode',
		'applicationinfo6',
		'companyName',
		'companyType',
		'companyAreaType',
		'industryType',
		'country',
		'isApplicaitonIssued',
		'actions'
	];
	companyRoute = environment.tradeAppRoot + environment.getCompanyDetails;
	companyTypeMaster = environment.tradeAppRoot + environment.companyTypeMaster;
	loggedUserName = this.commonFunction.parseApplicantInfoFromlocalStorage()[5];
	loggedUserId = this.commonFunction.parseApplicantInfoFromlocalStorage()[6];
	isFormCollected0 = environment.tradeAppRoot + environment.isFormCollected0;
	isFormCollected1 = environment.tradeAppRoot + environment.isFormCollected1;

	showApprove = true;
	showReject = false;

	@Input() group: FormGroup;
	myjson: any = JSON;
	dataSource = new MatTableDataSource([]);
	editMode = false;
	companyId = 0;
	companyType: string[] = [];
	@ViewChild(MatSort) sort: MatSort;
	@ViewChild(MatPaginator) paginator: MatPaginator;
	matchFinder = new MyErrorStateMatcher();

	ngOnInit() {
		this.dataService.loadApplicants(this.isFormCollected0);
		this.dataService.applicantSubject.subscribe((val) => {
			this.dataSource = new MatTableDataSource(val);
			this.dataSource.sort = this.sort;
			this.dataSource.paginator = this.paginator;
			return val;
		});
		this.getCompany();
	}

	get form(): FormGroup {
		return this.companyDetailsFormService.formGroup;
	}

	add(body: string) {
		this.dataService.postApplicants(this.companyRoute, body);
		this.dataSource.sort = this.sort;
		this.dataSource.paginator = this.paginator;
	}
	edit(applicantID: number) {
		const getApplicantInfo = this.companyRoute + '/' + applicantID;
		this.apiHandler.getApiRequest(getApplicantInfo).pipe(catchError(() => of([]))).subscribe((company: any) => {
			let companyInfo: any;
			companyInfo = { companyDetails: {} };
			companyInfo.companyDetails['companyName'] = company.companyName;
			companyInfo.companyDetails['applicantCode'] = company.applicantCode;
			companyInfo.companyDetails['tin'] = company.tin;
			companyInfo.companyDetails['emailID'] = company.emailID;
			companyInfo.companyDetails['phoneNo'] = company.phoneNo;
			companyInfo.companyDetails['ieRegCertificateNo'] = company.ieRegCertificateNo;
			companyInfo.companyDetails['address1'] = company.address1;
			companyInfo.companyDetails['address2'] = company.address2;
			companyInfo.companyDetails['address3'] = company.address3;
			companyInfo.companyDetails['city'] = company.city;
			companyInfo.companyDetails['country'] = company.country;
			companyInfo.companyDetails['state'] = company.state;
			companyInfo.companyDetails['pin'] = company.pin;
			companyInfo.companyDetails['companyType'] = company.companyType;
			companyInfo.companyDetails['companyAreaType'] = company.companyAreaType;
			companyInfo.companyDetails['industryType'] = company.industryType;
			companyInfo.companyDetails['applicationReceiptType'] = company.applicationReceiptType;
			companyInfo.companyDetails['remark'] = company.remark;
			this.companyDetailsFormService.formGroup.setValue(companyInfo);
			this.editMode = true;
			this.companyId = applicantID;
		});
	}

	update(parameter: any, body: string) {
		body['companyID'] = parameter;
		this.dataService.putApplicants(this.companyRoute, parameter, body);
	}
	delete(applicantID: number) {
		this.dataService.deleteApplicants(this.companyRoute, applicantID);
	}

	applyFilters(filterValue: string) {
		this.dataSource.filter = filterValue.trim().toLowerCase();
	}

	getCompany() {
		this.apiHandler.getApiRequest(this.companyTypeMaster).subscribe((companyTypeMaster) => {
			this.companyType = companyTypeMaster;
		});
	}

	attach(applicantID: any) {
		this.eventHandler.emitApplicantId(applicantID);
	}

	changeTableFilter(tab) {
		if (tab.index === 0) {
			this.showApprove = true;
			this.showReject = false;
			this.loadGridBased(this.isFormCollected0);
		} else if (tab.index === 1) {
			this.showApprove = false;
			this.showReject = true;
			this.loadGridBased(this.isFormCollected1);
		}
	}
	loadGridBased(url: string) {
		this.dataService.loadApplicants(url);
		this.dataService.applicantSubject.subscribe((val) => {
			this.dataSource = new MatTableDataSource(val);
			this.dataSource.sort = this.sort;
			this.dataSource.paginator = this.paginator;
			return val;
		});
	}

	approve(companyID: string) {
		const body = { companyID: companyID, isFormCollection: true };
		this.apiHandler.putApiRequest(this.companyRoute + '/' + companyID, JSON.stringify(body)).subscribe((data) => {
			this.loadGridBased(this.isFormCollected0);
		});
	}

	reject(companyID: string) {
		const body = { companyID: companyID, isFormCollection: false };
		this.apiHandler.putApiRequest(this.companyRoute + '/' + companyID, JSON.stringify(body)).subscribe((data) => {
			this.loadGridBased(this.isFormCollected1);
		});
	}
}
