import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountOneComponent } from './account-one.component';

describe('AccountOneComponent', () => {
  let component: AccountOneComponent;
  let fixture: ComponentFixture<AccountOneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountOneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountOneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
