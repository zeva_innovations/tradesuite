import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardIssueListComponent } from './card-issue-list.component';

describe('CardIssueListComponent', () => {
  let component: CardIssueListComponent;
  let fixture: ComponentFixture<CardIssueListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardIssueListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardIssueListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
