import { CommonFunctionsService } from './../../services/common-functions.service';
import { FileUploadComponent } from './../file-upload/file-upload.component';
import { Observable, of } from 'rxjs';
import { map, first, catchError, finalize } from 'rxjs/operators';
import { Component, OnInit, AfterViewInit, HostListener, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatTable, MatSort } from '@angular/material';
import { ApiHandlerService } from '../../services/api-handler.service';
import { HttpClient } from '@angular/common/http';
import { DataSourceServiceService } from '../../services/data-source-service.service';
import { environment } from '../../../environments/environment';
import { CustomerDetailsFormService } from '../../services/customer-details-form.service';
import { EmitterHandlerService } from '../../services/emitter-handler.service';
import * as moment from 'moment';
import { ApplicantGridComponent } from '../applicant-grid/applicant-grid.component';

@Component({
	selector: 'app-company-registration',
	templateUrl: './company-registration.component.html',
	styleUrls: ['./company-registration.component.scss']
})
export class CompanyRegistrationComponent implements OnInit {
	@ViewChild(ApplicantGridComponent) applicantGrid: ApplicantGridComponent;

	loggedUserName = this.commonFunction.parseApplicantInfoFromlocalStorage()[5];
	loggedUserId = this.commonFunction.parseApplicantInfoFromlocalStorage()[6];

	applicantsRoute = environment.tradeAppRoot + environment.getApplicants;
	applicantsByUser = environment.tradeAppRoot + environment.getApplicants + '/get/' + this.loggedUserName + this.loggedUserId;
	companycreated0 = environment.tradeAppRoot + environment.companycreated0;
	companycreated1 = environment.tradeAppRoot + environment.companycreated1;

	showApprove = true;
	showReject = false;
	showRenew = false;

	rootAppUrl = environment.tradeAppRoot;
	applicantUrl = this.rootAppUrl + environment.getApplicants;
	dataSource = new MatTableDataSource([]);

	displayedColumns = ['applicantID', 'applicantCode', 'firstName', 'emailID', 'phoneNo', 'country', 'actions'];
	@ViewChild(MatSort) sort: MatSort;
	@ViewChild(MatPaginator) paginator: MatPaginator;

	applicantCode(value: any) {
		this.commonFunction.applicantCodeForCompanyRegistration = value;
	}

	constructor(
		private apiHandler: ApiHandlerService,
		private commonFunction: CommonFunctionsService,
		private customerForm: CustomerDetailsFormService,
		private eventHandler: EmitterHandlerService,
		private dataService: DataSourceServiceService
	) {}

	ngOnInit() {
		this.dataService.loadApplicants(this.companycreated0);
		this.dataService.applicantSubject.subscribe((val) => {
			this.dataSource = new MatTableDataSource(val);
			this.dataSource.sort = this.sort;
			this.dataSource.paginator = this.paginator;
			return val;
		});
	}
	ngAfterViewInit() {
		setTimeout(() => {
			this.dataSource.sort = this.sort;
			this.dataSource.paginator = this.paginator;
		}, 2000);
	}
	add(body: string) {
		this.dataService.postApplicants(this.applicantsRoute, body);
		this.dataSource.sort = this.sort;
		this.dataSource.paginator = this.paginator;
	}

	edit(applicantID: number) {
		const getApplicantInfo = this.applicantsRoute + '/' + applicantID;
		this.apiHandler.getApiRequest(getApplicantInfo).pipe(catchError(() => of([]))).subscribe((applicants: any) => {
			let applicantInfo: any;
			applicantInfo = { customerDetails: {} };
			applicantInfo.customerDetails['title'] = applicants.title;
			applicantInfo.customerDetails['firstName'] = applicants.firstName;
			applicantInfo.customerDetails['surName'] = applicants.surName;
			applicantInfo.customerDetails['emailID'] = applicants.emailID;
			applicantInfo.customerDetails['phoneNo'] = applicants.phoneNo;
			applicantInfo.customerDetails['dob'] = moment(applicants.dob);
			applicantInfo.customerDetails['city'] = applicants.city;
			applicantInfo.customerDetails['country'] = applicants.country;
			applicantInfo.customerDetails['state'] = applicants.state;
			applicantInfo.customerDetails['pin'] = applicants.pin;
			applicantInfo.customerDetails['gender'] = applicants.gender;
			applicantInfo.customerDetails['nationality'] = applicants.nationality;
			applicantInfo.customerDetails['passportNo'] = applicants.passportNo;
			applicantInfo.customerDetails['socialSecurityNo'] = applicants.socialSecurityNo;
			applicantInfo.customerDetails['remark1'] = applicants.remark1;
			this.customerForm.formGroup.setValue(applicantInfo);
			this.eventHandler.toggleEditMode();
			this.eventHandler.emitApplicantId(applicantID);
		});
	}
	update(parameter: any, body: string) {
		this.dataService.putApplicants(this.applicantsRoute, parameter, body);
	}
	delete(applicantID: number) {
		this.dataService.deleteApplicants(this.applicantsRoute, applicantID);
	}
	attach(applicantID: any) {
		this.eventHandler.emitApplicantId(applicantID);
	}
	applyFilter(filterValue: string) {
		this.dataSource.filter = filterValue.trim().toLowerCase();
	}

	changeTableFilter(tab) {
		if (tab.index === 0) {
			this.loadGridBased(this.companycreated0);
			this.showApprove = true;
			this.showReject = false;
			this.showRenew = false;
		} else if (tab.index === 1) {
			this.loadGridBased(this.companycreated1);
			this.showApprove = false;
			this.showReject = true;
			this.showRenew = false;
		} else if (tab.index === 2) {
			this.loadGridBased(this.companycreated0);
			this.showApprove = false;
			this.showReject = false;
			this.showRenew = true;
		}
	}

	loadGridBased(url: string) {
		this.dataService.loadApplicants(url);
		this.dataService.applicantSubject.subscribe((val) => {
			this.dataSource = new MatTableDataSource(val);
			this.dataSource.sort = this.sort;
			this.dataSource.paginator = this.paginator;
			return val;
		});
	}
	approve(applicantID: string) {
		const body = { applicantID: applicantID, isCompanyCreated: true };
		this.apiHandler.putApiRequest(this.applicantsRoute + '/' + applicantID, JSON.stringify(body)).subscribe((data) => {
			this.loadGridBased(this.companycreated0);
		});
	}

	reject(applicantID: string) {
		const body = { applicantID: applicantID, isCompanyCreated: false };
		this.apiHandler.putApiRequest(this.applicantsRoute + '/' + applicantID, JSON.stringify(body)).subscribe((data) => {
			this.loadGridBased(this.companycreated1);
		});
	}

	editForRenewal(applicantID: number) {
		this.applicantGrid.edit(applicantID);
	}
}
