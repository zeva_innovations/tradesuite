import { CommonFunctionsService } from './../../services/common-functions.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from '../../services/authentication.service';
import { ApiHandlerService } from '../../services/api-handler.service';
import { first } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
	formGroup: FormGroup;
	loading = false;
	submitted = false;
	returnUrl: string;
	error = '';
	getRoleUrl = environment.tradeAppRoot + '/api/User_Menu_RightAPI/getrights';

	constructor(
		private formBuilder: FormBuilder,
		private route: ActivatedRoute,
		private router: Router,
		private authenticationService: AuthenticationService,
		private apiHandler: ApiHandlerService,
		public commonFunction: CommonFunctionsService,
		private http: HttpClient
	) {
		this.authenticationService.logout();
	}

	ngOnInit() {
		this.formGroup = this.formBuilder.group({
			UserName: ['', Validators.required],
			PasswordHash: ['', Validators.required]
		});

		this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/welcome';
	}

	get f() {
		return this.formGroup.controls;
	}

	onSubmit() {
		if (this.formGroup.invalid) {
			alert('Please enter Username and Passowrd');
			return;
		}

		this.loading = true;
		this.authenticationService.login(this.f.UserName.value, this.f.PasswordHash.value).pipe(first()).subscribe(
			(data) => {
				if (data.userID) {
					this.http
						.get(this.getRoleUrl + '/' + data.userID, {
							headers: new HttpHeaders({
								Authorization: `Bearer ${data.token}`,
								'Content-Type': 'application/json'
							})
						})
						.subscribe((roles) => {
							localStorage.setItem('roles', JSON.stringify(roles));
						});
					this.router.navigate([this.returnUrl]);
				} else if (data.error) {
					alert(data.error);
					this.loading = false;
				}
			},
			(error) => {
				this.error = error;
				this.loading = false;
			}
		);
	}
}
