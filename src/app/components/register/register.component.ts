import { IApplicant } from './../../interfaces/customer-details';
import { FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit, Input, Output, ViewChild, ViewEncapsulation } from '@angular/core';
import { CustomerDetailsFormService } from 'src/app/services/customer-details-form.service';
import { CustomerFormValidatorService } from 'src/app/services/customer-form-validator.service';
import { MatSnackBar } from '@angular/material';
import * as moment from 'moment';
import { environment } from '../../../environments/environment';
import { EmitterHandlerService } from '../../services/emitter-handler.service';
import { ApplicantGridComponent } from '../applicant-grid/applicant-grid.component';
import { CommonFunctionsService } from 'src/app/services/common-functions.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
	selector: 'app-register',
	templateUrl: './register.component.html',
	styleUrls: ['./register.component.scss'],
	providers: [CustomerDetailsFormService, CustomerFormValidatorService],
	encapsulation: ViewEncapsulation.None
})
export class RegisterComponent implements OnInit {
	@ViewChild(ApplicantGridComponent) applicantGrid: ApplicantGridComponent;
	editMode = false;
	applicantID = 0;
	applicantApi = environment.tradeAppRoot + environment.getApplicants;
	submitCounter = 0;
	formDataToBeSubmitted = '';

	loggedUserName = this.commonFunction.parseApplicantInfoFromlocalStorage()[5];
	loggedUserId = this.commonFunction.parseApplicantInfoFromlocalStorage()[6];

	constructor(
		private customerDetailsFormService: CustomerDetailsFormService,
		private matSnackBar: MatSnackBar,
		public commonFunction: CommonFunctionsService,
		private emitHandler: EmitterHandlerService,
		private spinnerService: Ng4LoadingSpinnerService
	) {}
	ngOnInit() {
		this.emitHandler.change.subscribe((ediMode) => {
			this.editMode = ediMode;
		});
		this.emitHandler.passApplicantId.subscribe((applicantID) => {
			this.applicantID = applicantID;
		});

		this.commonFunction.isTransactionCompleted.subscribe((data) => {
			this.reset();
			this.editMode = false;
			this.editMode = false;
		});
	}

	get form(): FormGroup {
		return this.customerDetailsFormService.formGroup;
	}
	reset() {
		this.form.reset();
		this.editMode = false;
	}

	submit(data: IApplicant, applicantID?: number) {
		this.submitCounter = this.submitCounter + 1;
		const updateIdForFormData = data['customerDetails'];

		updateIdForFormData['Org_ID'] = this.commonFunction.parseApplicantInfoFromlocalStorage()[2];
		updateIdForFormData['Unit_Org_ID'] = this.commonFunction.parseApplicantInfoFromlocalStorage()[3];
		updateIdForFormData['Branch_ID'] = this.commonFunction.parseApplicantInfoFromlocalStorage()[4];
		if (applicantID !== undefined && applicantID !== null) {
			updateIdForFormData['ApplicantID'] = applicantID;
			updateIdForFormData['isCompanyCreated'] = false;
			updateIdForFormData['edited_By'] = this.loggedUserName + this.loggedUserId;

			if (this.submitCounter === 1) {
				this.formDataToBeSubmitted = updateIdForFormData;
			}
			if (this.formDataToBeSubmitted !== updateIdForFormData || this.submitCounter === 1) {
				this.applicantGrid.update(applicantID, updateIdForFormData);
			} else {
				alert('You are submitting the same form details!!');
			}
		} else {
			updateIdForFormData['created_By'] = this.loggedUserName + this.loggedUserId;

			if (this.submitCounter === 1) {
				this.formDataToBeSubmitted = updateIdForFormData;
			}
			if (this.formDataToBeSubmitted !== updateIdForFormData || this.submitCounter === 1) {
				this.applicantGrid.add(updateIdForFormData);
			} else {
				alert('You are submitting the same form details!!');
			}
		}
	}

	validateAllFormFields(formGroup: FormGroup) {
		Object.keys(formGroup.controls).forEach((field) => {
			const control = formGroup.get(field);
			if (control instanceof FormControl) {
				control.markAsTouched({ onlySelf: true });
			} else if (control instanceof FormGroup) {
				this.validateAllFormFields(control);
			}
		});
	}

	openSnackBar(message: string, action: string) {
		this.matSnackBar.open('Saved Sucessfully', 'Close', {
			duration: 3000
		});
	}
	updateSnackBar(message: string, action: string) {
		this.matSnackBar.open('Updated Sucessfully', 'Close', {
			duration: 3000
		});
	}
}
