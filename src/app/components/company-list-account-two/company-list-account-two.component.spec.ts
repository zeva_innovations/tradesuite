import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyListAccountTwoComponent } from './company-list-account-two.component';

describe('CompanyListAccountTwoComponent', () => {
  let component: CompanyListAccountTwoComponent;
  let fixture: ComponentFixture<CompanyListAccountTwoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyListAccountTwoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyListAccountTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
