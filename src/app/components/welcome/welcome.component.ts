import { AuthGuard } from './../../services/guards.service';
import { Component, OnInit } from '@angular/core';
import { CommonFunctionsService } from 'src/app/services/common-functions.service';
import * as _ from 'lodash';
@Component({
	selector: 'app-welcome',
	templateUrl: './welcome.component.html',
	styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit {
	constructor(public authGuard: AuthGuard, private commonFunction: CommonFunctionsService) {
		setTimeout(() => {
			this.getChangeUserRole();
		}, 2000);
	}
	myWorkRoutes: ROUTE[] = [
		{
			icon: 'person_pin',
			route: '/applicantregistration',
			title: 'Applicant Registration'
		},
		{
			icon: 'card_travel',
			route: '/companyregistration',
			title: 'Company Registration'
		},
		{
			icon: 'card_travel',
			route: '/companylists',
			title: 'Company Lists'
		},
		{
			icon: 'assignment_returned',
			route: '/applicationissue',
			title: 'Application Issue'
		},
		{
			icon: 'assignment_turned_in',
			route: '/formcollect',
			title: 'Form Collection'
		},
		{
			icon: 'local_printshop',
			route: '/scandepartment',
			title: 'Scanning Department'
		},
		{
			icon: 'create',
			route: '/dataentry',
			title: 'Data Entry'
		},
		{
			icon: 'done',
			route: '/companyapproval',
			title: 'Director Approval'
		},
		{
			icon: 'money',
			route: '/accountone',
			title: 'Account 1'
		},
		{
			icon: 'attach_money',
			route: '/accounttwo',
			title: 'Account 2'
		},
		{
			icon: 'done_all',
			route: '/companydgapproval',
			title: 'Final Approval'
		},
		{
			icon: 'print',
			route: '/cardprint',
			title: 'Card Print'
		},
		{
			icon: 'credit_card',
			route: '/cardwrite',
			title: 'Card Write'
		},
		{
			icon: 'done_outline',
			route: '/cardissue',
			title: 'Card Issue'
		},
		{
			icon: 'folder',
			route: '/MainMaster',
			title: 'Masters'
		}
	];

	ngOnInit() { }

	getChangeUserRole() {
		const mainRolesArray = this.commonFunction.getRolesBasedOnMenu()[0];
		const modifiedRoleArray = [];
		mainRolesArray.forEach((element) => {
			modifiedRoleArray.push({ route: '/' + element });
		});
		this.myWorkRoutes = _.intersectionBy(this.myWorkRoutes, modifiedRoleArray, 'route');

		if (mainRolesArray.length === 0) {
			this.myWorkRoutes = [];
		}
	}
}

interface ROUTE {
	icon?: string;
	route?: string;
	title?: string;
}
