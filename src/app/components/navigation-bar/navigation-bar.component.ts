import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { environment } from 'src/environments/environment.prod';
import { CommonFunctionsService } from 'src/app/services/common-functions.service';

@Component({
	selector: 'app-navigation-bar',
	templateUrl: './navigation-bar.component.html',
	styleUrls: ['./navigation-bar.component.scss']
})
export class NavigationBarComponent implements OnInit {
	@Output() toggleSidenav = new EventEmitter<void>();
	helpPageUrl = '';

	constructor(private router: Router, public commonFunction: CommonFunctionsService) {}
	loggedUserName = this.commonFunction.parseApplicantInfoFromlocalStorage()[5];
	ngOnInit() {
		this.router.events.subscribe((event) => {
			if (event instanceof NavigationEnd) {
				this.helpPageUrl = '';
				this.helpPageUrl = environment.tradeAppRoot + '/api/HelpController/GetApplicationformate' + event.url + '_pdf';
			}
		});
	}
}
