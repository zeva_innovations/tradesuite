import { CommonFunctionsService } from 'src/app/services/common-functions.service';
import { CompanyDetailsFormService } from './../../services/company-details-form.service';
import { Component, OnInit, Input, ViewChild, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { MyErrorStateMatcher } from 'src/app/services/customer-details-form.service';
import { FormGroup } from '@angular/forms';
import { DataSourceServiceService } from 'src/app/services/data-source-service.service';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { environment } from 'src/environments/environment';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { ApiHandlerService } from 'src/app/services/api-handler.service';
import { EmitterHandlerService } from 'src/app/services/emitter-handler.service';
import * as _ from 'lodash';

@Component({
	selector: 'app-company-lists',
	templateUrl: './company-lists.component.html',
	styleUrls: ['./company-lists.component.scss'],
	changeDetection: ChangeDetectionStrategy.Default
})
export class CompanyListsComponent implements OnInit {
	constructor(
		private apiHandler: ApiHandlerService,
		private companyDetailsFormService: CompanyDetailsFormService,
		private dataService: DataSourceServiceService,
		private eventHandler: EmitterHandlerService,
		public commonFunction: CommonFunctionsService,
		private cdr: ChangeDetectorRef
	) { }
	displayedColumns = ['companyID', 'companyCode', 'applicationinfo6', 'companyName', 'companyType', 'companyAreaType', 'industryType', 'country', 'actions'];
	companyRoute = environment.tradeAppRoot + environment.getCompanyDetails;
	companyTypeMaster = environment.tradeAppRoot + environment.companyTypeMaster;
	companyAreaTypeMaster = environment.tradeAppRoot + environment.companyAreaType;
	industryTypeMaster = environment.tradeAppRoot + environment.industryType;

	loggedUserName = this.commonFunction.parseApplicantInfoFromlocalStorage()[5];
	loggedUserId = this.commonFunction.parseApplicantInfoFromlocalStorage()[6];
	applicantsByUser = this.companyRoute + '/get/' + this.loggedUserName + this.loggedUserId;
	@Input() group: FormGroup;
	myjson: any = JSON;
	dataSource = new MatTableDataSource([]);
	editMode = true;
	viewMode = false;
	companyId = 0;
	companyType: string[] = [];
	companyAreaType: string[] = [];
	industryType: string[] = [];
	submitCounter = 0;
	formDataToBeSubmitted = '';
	companyDetailsForRenewal: any;
	showEditGrid = false;

	getCoutryDetails = environment.tradeAppRoot + environment.allCountries;
	getStateDetails = environment.tradeAppRoot + environment.getState;
	getCiyDetails = environment.tradeAppRoot + environment.getCity;
	countries = [];
	states = [];
	cities = [];

	@ViewChild(MatSort) sort: MatSort;
	@ViewChild(MatPaginator) paginator: MatPaginator;
	matchFinder = new MyErrorStateMatcher();
	totalRowsLegnth = 0;

	get applicantCode(): string {
		return this.commonFunction.applicantCodeForCompanyRegistration;
	}
	set applicantCode(value: string) {
		this.commonFunction.applicantCodeForCompanyRegistration = value;
	}
	handleRenewalFlow() {
		if (
			this.commonFunction.companyDetailsForRenewal !== undefined &&
			this.commonFunction.companyDetailsForRenewal !== null &&
			this.commonFunction.companyDetailsForRenewal !== '' &&
			this.commonFunction.isRenewalFlow === true
		) {
			this.companyDetailsForRenewal = this.commonFunction.companyDetailsForRenewal;
			this.companyDetailsFormService.formGroup.patchValue({ companyDetails: this.companyDetailsForRenewal });
		}
	}
	ngOnInit() {
		this.getCountryStateCity();
		this.updateCountryStateCity();
		this.dataService.loadApplicants(this.applicantsByUser);
		this.dataService.applicantSubject.subscribe((val) => {
			this.dataSource = new MatTableDataSource(val);
			this.dataSource.sort = this.sort;
			this.dataSource.paginator = this.paginator;

			return val;
		});
		this.getCompany();
		this.getCompanyAreaType();
		this.getIndustryType();
		this.handleRenewalFlow();
		this.companyDetailsFormService.formGroup.patchValue({ companyDetails: { applicantCode: this.commonFunction.applicantCodeForCompanyRegistration } });
	}
	get form(): FormGroup {
		return this.companyDetailsFormService.formGroup;
	}

	add(body: string) {
		this.submitCounter = this.submitCounter + 1;
		body['created_By'] = this.loggedUserName + this.loggedUserId;
		body['Org_ID'] = this.commonFunction.parseApplicantInfoFromlocalStorage()[2];
		body['Unit_Org_ID'] = this.commonFunction.parseApplicantInfoFromlocalStorage()[3];
		body['Branch_ID'] = this.commonFunction.parseApplicantInfoFromlocalStorage()[4];
		body['applicantCode'] = this.applicantCode;
		body['barCode'] = this.applicantCode + '_' + this.commonFunction.getUTCNow().toString();

		if (this.submitCounter === 1) {
			this.formDataToBeSubmitted = body;
		}
		if (this.applicantCode !== undefined && this.applicantCode !== null && (this.formDataToBeSubmitted !== body || this.submitCounter === 1)) {
			if (this.commonFunction.isRenewalFlow) {
				alert('You are in Renewal Flow!!');
				body['applicationinfo2'] = 'RENEWAL';
			}
			this.dataService.postApplicants(this.companyRoute, body);
		} else if (this.applicantCode === undefined || this.applicantCode === null) {
			alert('You cannot create a company without Applicant Code');
		} else {
			alert('You are submitting the same form details!!');
		}
		this.dataSource.sort = this.sort;
		this.dataSource.paginator = this.paginator;
	}
	edit(applicantID: number) {
		const getApplicantInfo = this.companyRoute + '/GetId/' + applicantID;
		this.apiHandler.getApiRequest(getApplicantInfo).pipe(catchError(() => of([]))).subscribe((company: any) => {
			let companyInfo: any;
			companyInfo = { companyDetails: {} };
			companyInfo.companyDetails['companyName'] = company.companyName;
			companyInfo.companyDetails['applicantCode'] = company.applicantCode;
			companyInfo.companyDetails['tin'] = company.tin;
			companyInfo.companyDetails['emailID'] = company.emailID;
			companyInfo.companyDetails['phoneNo'] = company.phoneNo;
			companyInfo.companyDetails['ieRegCertificateNo'] = company.ieRegCertificateNo;
			companyInfo.companyDetails['address1'] = company.address1;
			companyInfo.companyDetails['address2'] = company.address2;
			companyInfo.companyDetails['address3'] = company.address3;
			companyInfo.companyDetails['city'] = company.city;
			companyInfo.companyDetails['country'] = company.country;
			companyInfo.companyDetails['state'] = company.state;
			companyInfo.companyDetails['pin'] = company.pin;
			companyInfo.companyDetails['companyType'] = company.companyType;
			companyInfo.companyDetails['companyAreaType'] = company.companyAreaType;
			companyInfo.companyDetails['industryType'] = company.industryType;
			companyInfo.companyDetails['applicationinfo1'] = company.applicationinfo1;
			companyInfo.companyDetails['remark'] = company.remark;

			this.companyDetailsFormService.formGroup.setValue(companyInfo);
			this.editMode = true;
			this.companyId = applicantID;
			this.showEditGrid = true;
		});
	}

	update(parameter: any, body: string) {
		body['companyID'] = parameter;
		body['edited_By'] = this.loggedUserName + this.loggedUserId;
		this.dataService.putApplicants(this.companyRoute, parameter, body);
		setTimeout(() => {
			this.showEditGrid = false;
		}, 2000);
	}
	delete(applicantID: number) {
		this.dataService.deleteApplicants(this.companyRoute, applicantID);
	}
	attach(applicantID: any) {
		this.eventHandler.emitApplicantId(applicantID);
	}
	applyFilter(filterValue: string) {
		this.dataSource.filter = filterValue.trim().toLowerCase();
	}
	reset() {
		this.form.reset();
		this.applicantCode = null;
		this.showEditGrid = false;
		this.editMode = false;
	}
	getCompany() {
		this.apiHandler.getApiRequest(this.companyTypeMaster).subscribe((companyTypeMaster) => {
			this.companyType = companyTypeMaster;
		});
	}
	getCompanyAreaType() {
		this.apiHandler.getApiRequest(this.companyAreaTypeMaster).subscribe((companyAreaType) => {
			this.companyAreaType = companyAreaType;
		});
	}
	getIndustryType() {
		this.apiHandler.getApiRequest(this.industryTypeMaster).subscribe((industryType) => {
			this.industryType = industryType;
		});
	}

	enableViewMode() {
		this.viewMode = true;
	}
	diableViewMode() {
		this.viewMode = false;
	}
	ngOnDestroy(): void {
		// Called once, before the instance is destroyed.
		// Add 'implements OnDestroy' to the class.
		this.reset();
		this.editMode = false;
	}
	getCountryStateCity() {
		this.countries = [];
		this.apiHandler.getApiRequest(this.getCoutryDetails).subscribe((data: any) => {
			data.forEach((element) => {
				this.countries.push(element.country);
			});
			this.countries = _.uniqBy(this.countries);
		});
	}

	getStateByCountry(country: string) {
		this.states = [];
		this.apiHandler.getApiRequest(this.getStateDetails + '/' + country).subscribe((data: any) => {
			data.forEach((element) => {
				this.states.push(element.state);
			});
			this.states = _.uniqBy(this.states);
		});
	}
	getCityByState(state: string) {
		this.cities = [];
		this.apiHandler.getApiRequest(this.getCiyDetails + '/' + state).subscribe((data: any) => {
			data.forEach((element) => {
				this.cities.push(element.city);
			});
			this.cities = _.uniqBy(this.cities);
		});
	}

	updateCountryStateCity() {
		this.form.get('companyDetails.country').valueChanges.subscribe(() => {
			let countryValue = this.form.get('companyDetails.country').value;
			this.getStateByCountry(countryValue);
			this.cdr.detectChanges();
		});
		this.form.get('companyDetails.state').valueChanges.subscribe(() => {
			let stateValue = this.form.get('companyDetails.state').value;
			this.getCityByState(stateValue);
			this.cdr.detectChanges();
		});
	}
}
