import { CommonFunctionsService } from "src/app/services/common-functions.service";
import { Component, OnInit } from "@angular/core";
import { MatDialog, MatTableDataSource } from "@angular/material";
import { ApiHandlerService } from "src/app/services/api-handler.service";
import { DataSourceServiceService } from "src/app/services/data-source-service.service";
import { EmitterHandlerService } from "src/app/services/emitter-handler.service";
import { environment } from "src/environments/environment";
import * as _ from "lodash";
import * as moment from "moment";
@Component({
	selector: "company-approval",
	templateUrl: "./approval.component.html",
	styleUrls: ["./approval.component.scss"]
})
export class ApprovalComponent implements OnInit {
	constructor(public dialog: MatDialog) { }

	ngOnInit() { }

	openDialog() {
		const dialogRef = this.dialog.open(ApprovalDialogComponent, {
			height: "50%",
			width: "75%"
		});
		dialogRef.afterClosed().subscribe(result => { });
	}
}

@Component({
	selector: "approval-dialog",
	templateUrl: "approval-dialog.html"
})
export class ApprovalDialogComponent implements OnInit {
	companyRoute = environment.tradeAppRoot + environment.getCompanyDetails;
	applyTypeRouter = environment.tradeAppRoot + environment.applyTypeMaster;
	Assign_DocumentForIndustryAPI =
		environment.tradeAppRoot + environment.assignDocumentForIndustry;
	getPdfDocumentApi =
		environment.tradeAppRoot + environment.generatePdfDocument;
	isUGApproved0 = environment.tradeAppRoot + environment.isUGApproved0;
	applyTypeMaster: string[] = [];
	isAlreadyApproved = false;
	fromDate: any;
	isAddOnFlow = false;

	dataSource = new MatTableDataSource([]);
	fromDateModel: any;
	toDateModel: any;
	toDate: any;
	companyObject: any;
	companyCode = "";
	companyID = 0;
	licenseType = "";

	displayIssueButton = false;
	invoicePrintCertification1Url =
		environment.tradeAppRoot + environment.invoicePrintCertification1;

	invoicePrint1Url = environment.tradeAppRoot + environment.invoicePrint1;
	licenseRoute = environment.tradeAppRoot + environment.licenseRegistration;

	invoicePrintCertButtonUrl1 = "";
	invoicePrintCertButtonUrl2 = "";
	invoicePrintCertButtonUrl3 = "";
	invoicePrintButtonUrl1 = "";
	invoicePrintButtonUrl2 = "";
	invoicePrintButtonUrl3 = "";
	isNotCertificationPdf = false;
	buttonColor = "#3933FF";

	isDgApproval = false;
	constructor(
		private apiHandler: ApiHandlerService,
		private dataService: DataSourceServiceService,
		private emitHandler: EmitterHandlerService,
		public commonFunction: CommonFunctionsService
	) {
		this.commonFunction.isDgApproval = false;
	}

	ngOnInit() {
		this.getCompany();
		this.emitHandler.passApplicantId.subscribe(inputObject => {
			this.companyObject = inputObject;
			this.companyCode = inputObject.companyCode;
			this.companyID = inputObject.companyID;
			this.licenseType = inputObject.applicationinfo6;

			if (inputObject.applicationinfo3 != null) {
				this.fromDateModel = inputObject.applicationinfo3;
				this.toDateModel = inputObject.applicationinfo4;
				this.isAlreadyApproved = true;
			}

			const licenseFlow = this.licenseType.split("-");
			if (licenseFlow[5].includes("ADD ON")) {
				//This will disable the editing the dates for from and to date
				this.isAddOnFlow = true;
			}
		});

	}

	setToDate() {
		if (this.licenseType !== null) {
			const array = this.licenseType.split("-");
			const durationOfLicense = array[2];
			let new_date;
			if (durationOfLicense.includes("Month")) {
				new_date = moment(this.fromDateModel, "DD-MM-YYYY").add(
					durationOfLicense.split(" ")[0],
					"month"
				);
			} else if (durationOfLicense.includes("Year")) {
				new_date = moment(this.fromDateModel, "DD-MM-YYYY").add(
					durationOfLicense.split(" ")[0],
					"year"
				);
			}
			new_date = moment(new_date, "DD-MM-YYYY").subtract(1, "days");
			this.toDateModel = new_date;
		} else {
			alert("Please Set the License For the Company First!!");
		}
	}

	getCompany() {
		this.apiHandler
			.getApiRequest(this.applyTypeRouter)
			.subscribe(applyTypeMaster => {
				this.applyTypeMaster = applyTypeMaster;
			});
	}

	approveLicense() {
		this.displayIssueButton = false;
		this.isNotCertificationPdf = false;
		this.isDgApproval = this.commonFunction.isDgApproval;
		const body = {
			companyID: this.companyID,
			applicationinfo3: moment(this.fromDateModel, "DD-MM-YYYY").add(
				moment().utcOffset(),
				"minutes"
			),
			applicationinfo4: moment(this.toDateModel, "DD-MM-YYYY").add(
				moment().utcOffset(),
				"minutes"
			)
		};
		let licenseType: any;
		licenseType = this.companyObject;

		const licenseValue = licenseType.applicationinfo6;
		const companyCode = licenseType.companyCode;
		const licenseFlow = licenseValue.split("-");
		let applicationPath = "";
		let applicationDocPath = "";
		let applicationAddOnPath = "";

		this.apiHandler
			.putApiRequest(
				this.companyRoute + "/" + this.companyID,
				JSON.stringify(body)
			)
			.subscribe(data => {
				this.dataService.loadApplicants(this.isUGApproved0);
				this.dataService.applicantSubject.subscribe(val => {
					this.dataSource = new MatTableDataSource(val);
					//return val;
				});




				if (licenseFlow[1].includes("Certification")) {
					this.apiHandler
						.getApiRequest(
							this.invoicePrintCertification1Url +
							"/" +
							licenseValue +
							"-" +
							companyCode
						)
						.subscribe(data => {
							let response: any;
							response = data;

							if (
								response.applicationPath !== undefined &&
								response.applicationPath !== null
							) {
								applicationPath = response.applicationPath;
							}

							if (
								response.applicationDocPath !== undefined &&
								response.applicationDocPath !== null
							) {
								applicationDocPath = response.applicationDocPath;
							}
							if (
								response.applicationAddOnPath !== undefined &&
								response.applicationAddOnPath !== null
							) {
								applicationAddOnPath = response.applicationAddOnPath;
							}

							this.invoicePrintCertButtonUrl1 =
								environment.tradeAppRoot +
								"/api/Accounts_RegistrationAPI/GetApplicationformate/" +
								applicationPath;
							this.invoicePrintCertButtonUrl2 =
								environment.tradeAppRoot +
								"/api/Accounts_RegistrationAPI/GetApplicationformate/" +
								applicationDocPath;
							this.invoicePrintCertButtonUrl3 =
								environment.tradeAppRoot +
								"/api/Accounts_RegistrationAPI/GetApplicationformate/" +
								applicationAddOnPath;

							this.displayIssueButton = true;
							this.buttonColor = "#484848";
						});
				} else {
					this.apiHandler
						.getApiRequest(
							this.invoicePrint1Url + "/" + licenseValue + "-" + companyCode
						)
						.pipe()
						.subscribe(data => {
							let response: any;
							response = data;
							if (
								response.applicationPath !== undefined &&
								response.applicationPath !== null
							) {
								applicationPath = response.applicationPath;
							}

							if (
								response.applicationDocPath !== undefined &&
								response.applicationDocPath !== null
							) {
								applicationDocPath = response.applicationDocPath;
							}
							if (
								response.applicationAddOnPath !== undefined &&
								response.applicationAddOnPath !== null
							) {
								applicationAddOnPath = response.applicationAddOnPath;
							}
							this.invoicePrintCertButtonUrl1 =
								environment.tradeAppRoot +
								"/api/Accounts_RegistrationAPI/GetApplicationformate/" +
								applicationPath;
							this.invoicePrintCertButtonUrl2 =
								environment.tradeAppRoot +
								"/api/Accounts_RegistrationAPI/GetApplicationformate/" +
								applicationDocPath;
							this.invoicePrintCertButtonUrl3 =
								environment.tradeAppRoot +
								"/api/Accounts_RegistrationAPI/GetApplicationformate/" +
								applicationAddOnPath;
							this.displayIssueButton = true;
							this.isNotCertificationPdf = true;
						});
				}
			});

		const licenseTypeFormat = this.licenseType.split("-");
		const licenseBody = {
			companyCode: this.companyCode,
			licenceType: licenseTypeFormat[1],
			applyType: this.licenseType
		};

		if (this.isDgApproval === true) {
			this.apiHandler
				.postApiRequest(this.licenseRoute, JSON.stringify(licenseBody))
				.subscribe((data: any) => {
					alert("License Number :" + data.licenceNo + " generate successfully");
				});
		}
	}
}
