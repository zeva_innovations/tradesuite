import { ApiHandlerService } from './../../services/api-handler.service';
import { MyErrorStateMatcher } from './../../services/customer-details-form.service';
import { Component, OnInit, Input } from '@angular/core';
import { CustomerDetailsFormService } from '../../services/customer-details-form.service';
import { FormGroup, FormControl, FormGroupDirective, NgForm } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material';
import { environment } from 'src/environments/environment';
import * as _ from 'lodash';
@Component({
	selector: 'app-customer-details',
	templateUrl: './customer-details.component.html',
	styleUrls: ['./customer-details.component.scss']
})
export class CustomerDetailsComponent extends MyErrorStateMatcher implements OnInit {
	getCoutryDetails = environment.tradeAppRoot + environment.allCountries;
	getStateDetails = environment.tradeAppRoot + environment.getState;
	getCiyDetails = environment.tradeAppRoot + environment.getCity;
	countries = [];
	states = [];
	cities = [];
	constructor(private apihandler: ApiHandlerService) {
		super();
	}
	@Input() group: FormGroup;
	myjson: any = JSON;

	matchFinder = new MyErrorStateMatcher();
	ngOnInit() {
		this.getCountryStateCity();
		this.updateCountryStateCity();
	}

	getCountryStateCity() {
		this.countries = [];
		this.apihandler.getApiRequest(this.getCoutryDetails).subscribe((data: any) => {
			data.forEach((element) => {
				this.countries.push(element.country);
			});
			this.countries = _.uniqBy(this.countries);
		});
	}

	getStateByCountry(country: string) {
		this.states = [];
		this.apihandler.getApiRequest(this.getStateDetails + '/' + country).subscribe((data: any) => {
			data.forEach((element) => {
				this.states.push(element.state);
			});
			this.states = _.uniqBy(this.states);
		});
	}
	getCityByState(state: string) {
		this.cities = [];
		this.apihandler.getApiRequest(this.getCiyDetails + '/' + state).subscribe((data: any) => {
			data.forEach((element) => {
				this.cities.push(element.city);
			});
			this.cities = _.uniqBy(this.cities);
		});
	}

	updateCountryStateCity() {
		this.group.controls['country'].valueChanges.subscribe(() => {
			this.getStateByCountry(this.group.controls['country'].value);
		});
		this.group.controls['state'].valueChanges.subscribe(() => {
			this.getCityByState(this.group.controls['state'].value);
		});
	}
}
