import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicationIssueCompanyGridComponent } from './application-issue-company-grid.component';

describe('ApplicationIssueCompanyGridComponent', () => {
  let component: ApplicationIssueCompanyGridComponent;
  let fixture: ComponentFixture<ApplicationIssueCompanyGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicationIssueCompanyGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicationIssueCompanyGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
