import { CommonFunctionsService } from 'src/app/services/common-functions.service';
import { Component, OnInit, Input, ViewChild, Output } from '@angular/core';
import { ApiHandlerService } from 'src/app/services/api-handler.service';
import { CompanyDetailsFormService } from 'src/app/services/company-details-form.service';
import { DataSourceServiceService } from 'src/app/services/data-source-service.service';
import { EmitterHandlerService } from 'src/app/services/emitter-handler.service';
import { environment } from 'src/environments/environment';
import { FormGroup } from '@angular/forms';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { MyErrorStateMatcher } from 'src/app/services/customer-details-form.service';
import { catchError } from 'rxjs/operators';
import { of, BehaviorSubject } from 'rxjs';

@Component({
	selector: 'app-application-issue-company-grid',
	templateUrl: './application-issue-company-grid.component.html',
	styleUrls: ['./application-issue-company-grid.component.scss']
})
export class ApplicationIssueCompanyGridComponent implements OnInit {
	constructor(
		private apiHandler: ApiHandlerService,
		private companyDetailsFormService: CompanyDetailsFormService,
		private dataService: DataSourceServiceService,
		private eventHandler: EmitterHandlerService,
		private commonFunction: CommonFunctionsService
	) { }
	displayedColumns = [
		'companyID',
		'companyCode',
		'applicationinfo6',
		'companyName',
		'companyType',
		'companyAreaType',
		'industryType',
		'country',
		'isApplicaitonIssued',
		'actions'
	];
	companyRoute = environment.tradeAppRoot + environment.getCompanyDetails;
	companyTypeMaster = environment.tradeAppRoot + environment.companyTypeMaster;
	loggedUserName = this.commonFunction.parseApplicantInfoFromlocalStorage()[5];
	loggedUserId = this.commonFunction.parseApplicantInfoFromlocalStorage()[6];
	pendingTabRoute = this.companyRoute + '/isApplicaitonIssued0/' + this.loggedUserName + this.loggedUserId;
	approvedTabRoute = this.companyRoute + '/isApplicaitonIssued1/' + this.loggedUserName + this.loggedUserId;
	processedTabRoute = this.companyRoute + '/isProcessed/' + this.loggedUserName + this.loggedUserId;
	@Input() group: FormGroup;
	myjson: any = JSON;
	dataSource = new MatTableDataSource([]);
	editMode = false;
	companyId = 0;
	companyType: string[] = [];
	@ViewChild(MatSort) sort: MatSort;
	@ViewChild(MatPaginator) paginator: MatPaginator;
	matchFinder = new MyErrorStateMatcher();
	showApprove = true;
	showReject = false;
	companyDetailsForApplicationIssue = '';
	ngOnInit() {
		this.dataService.loadApplicants(this.pendingTabRoute);
		this.dataService.applicantSubject.subscribe((val) => {
			this.dataSource = new MatTableDataSource(val);
			this.dataSource.sort = this.sort;
			this.dataSource.paginator = this.paginator;
			return val;
		});
		this.getCompany();
	}

	get form(): FormGroup {
		return this.companyDetailsFormService.formGroup;
	}

	add(body: string) {
		this.dataService.postApplicants(this.companyRoute, body);
		this.dataSource.sort = this.sort;
		this.dataSource.paginator = this.paginator;
	}
	edit(applicantID: number) {
		const getApplicantInfo = this.companyRoute + '/' + applicantID;
		this.apiHandler.getApiRequest(getApplicantInfo).pipe(catchError(() => of([]))).subscribe((company: any) => {
			let companyInfo: any;
			companyInfo = { companyDetails: {} };
			companyInfo.companyDetails['companyName'] = company.companyName;
			companyInfo.companyDetails['applicantCode'] = company.applicantCode;
			companyInfo.companyDetails['tin'] = company.tin;
			companyInfo.companyDetails['emailID'] = company.emailID;
			companyInfo.companyDetails['phoneNo'] = company.phoneNo;
			companyInfo.companyDetails['ieRegCertificateNo'] = company.ieRegCertificateNo;
			companyInfo.companyDetails['address1'] = company.address1;
			companyInfo.companyDetails['address2'] = company.address2;
			companyInfo.companyDetails['address3'] = company.address3;
			companyInfo.companyDetails['city'] = company.city;
			companyInfo.companyDetails['country'] = company.country;
			companyInfo.companyDetails['state'] = company.state;
			companyInfo.companyDetails['pin'] = company.pin;
			companyInfo.companyDetails['companyType'] = company.companyType;
			companyInfo.companyDetails['companyAreaType'] = company.companyAreaType;
			companyInfo.companyDetails['industryType'] = company.industryType;
			companyInfo.companyDetails['applicationReceiptType'] = company.applicationReceiptType;
			companyInfo.companyDetails['remark'] = company.remark;
			this.companyDetailsFormService.formGroup.setValue(companyInfo);
			this.editMode = true;
			this.companyId = applicantID;
		});
	}

	update(parameter: any, body: string) {
		body['companyID'] = parameter;
		this.dataService.putApplicants(this.companyRoute, parameter, body);
	}
	delete(applicantID: number) {
		this.dataService.deleteApplicants(this.companyRoute, applicantID);
	}

	applyFilters(filterValue: string) {
		this.dataSource.filter = filterValue.trim().toLowerCase();
	}

	getCompany() {
		this.apiHandler.getApiRequest(this.companyTypeMaster).subscribe((companyTypeMaster) => {
			this.companyType = companyTypeMaster;
		});
	}

	attach(applicantID: any) {
		this.eventHandler.emitApplicantId(applicantID);
		this.companyDetailsForApplicationIssue = applicantID;
	}

	changeTableFilter(tab) {
		if (tab.index === 0) {
			this.loadGridBased(this.pendingTabRoute);
			this.showApprove = true;
			this.showReject = false;
		} else if (tab.index === 1) {
			this.loadGridBased(this.approvedTabRoute);
			this.showApprove = false;
			this.showReject = true;
		} else if (tab.index === 2) {
			this.loadGridBased(this.processedTabRoute);
			this.showApprove = false;
			this.showReject = false;
		}
	}
	loadGridBased(url: string) {
		this.dataService.loadApplicants(url);
		this.dataService.applicantSubject.subscribe((val) => {
			this.dataSource = new MatTableDataSource(val);
			this.dataSource.sort = this.sort;
			this.dataSource.paginator = this.paginator;
			return val;
		});
	}

	approve(companyID: string) {
		const body = { companyID: companyID, isApplicaitonIssued: true };
		this.apiHandler.putApiRequest(this.companyRoute + '/' + companyID, JSON.stringify(body)).subscribe((data) => {
			this.loadGridBased(this.pendingTabRoute);
		});
	}

	reject(companyID: string) {
		const body = { companyID: companyID, isApplicaitonIssued: false };
		this.apiHandler.putApiRequest(this.companyRoute + '/' + companyID, JSON.stringify(body)).subscribe((data) => {
			this.loadGridBased(this.approvedTabRoute);
		});
	}

	clone(companyID: string) {
		const getCompanyInfo = this.companyRoute + '/GetId/' + companyID;
		this.apiHandler.getApiRequest(getCompanyInfo).subscribe((companyDetails) => {
			const companyObject: any = companyDetails;
			delete companyObject.companyID;
			delete companyObject.companyCode;
			this.apiHandler.postApiRequest(this.companyRoute, companyObject).subscribe((data) => {
				this.loadGridBased(this.pendingTabRoute);
			});
		});
	}
	isGroup(index, item): boolean {
		return item.companyName === 'COMPANYNAM';
	}
}
