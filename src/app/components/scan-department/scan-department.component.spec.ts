import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScanDepartmentComponent } from './scan-department.component';

describe('ScanDepartmentComponent', () => {
  let component: ScanDepartmentComponent;
  let fixture: ComponentFixture<ScanDepartmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScanDepartmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScanDepartmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
