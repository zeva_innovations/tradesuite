import { Component, OnInit } from '@angular/core';
import { MatDialog, MatTableDataSource } from '@angular/material';
import { ApiHandlerService } from 'src/app/services/api-handler.service';
import { DataSourceServiceService } from 'src/app/services/data-source-service.service';
import { EmitterHandlerService } from 'src/app/services/emitter-handler.service';
import { environment } from 'src/environments/environment';
import * as _ from 'lodash';
import * as moment from 'moment';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { CommonFunctionsService } from 'src/app/services/common-functions.service';
import { catchError, take } from 'rxjs/operators';
import { of } from 'rxjs';
@Component({
	selector: 'app-account-two',
	templateUrl: './account-two.component.html',
	styleUrls: ['./account-two.component.scss']
})
export class AccountTwoComponent implements OnInit {
	constructor(public dialog: MatDialog) { }

	ngOnInit() { }

	openDialog() {
		const dialogRef = this.dialog.open(AccountTwoDialogComponent, {
			height: '80%',
			width: '80%'
		});
		dialogRef.afterClosed().subscribe((result) => { });
	}
}

@Component({
	selector: 'account-two-dialog',
	templateUrl: 'account-two-dialog.html',
	styleUrls: ['./account-two.component.scss']
})
export class AccountTwoDialogComponent implements OnInit {
	bankMaster = environment.tradeAppRoot + environment.bankMaster;
	paymentMaster = environment.tradeAppRoot + environment.paymentMaster;
	accountTwoRoute = environment.tradeAppRoot + environment.account2registration;
	/*accounttweRoute = environment.tradeAppRoot + environment.account2registration;*/
	accountoneRoute = environment.tradeAppRoot + environment.account1registration;
	form: FormGroup;
	dataSource1 = new MatTableDataSource([]);
	displayedColumns = ['acconutID', 'companyCode', 'paidType', 'bankName', 'actions'];
	isAlreadyHaveBankDetails = false;
	companyObject: any;
	companyCode = '';
	companyID = 0;
	editMode = false;
	masterId = 0;
	accountId = 0;
	submitCounter = 0;
	formDataToBeSubmitted = '';
	chequeInfo = 'Number';
	paymentMode = '';
	form15Number = 0;

	licenseType = '';
	account: string[] = [];
	payment: string[] = [];
	bank: string[] = [];

	constructor(
		private formBuilder: FormBuilder,
		public commonFunction: CommonFunctionsService,
		private apiHandler: ApiHandlerService,
		private dataService: DataSourceServiceService,
		private emitHandler: EmitterHandlerService
	) { }
	ngOnInit() {
		this.dataService.dialogSubject.next([]);
		this.createForm();
		this.form.valueChanges.subscribe((data) => {
			this.chequeInfo = 'Number';
			if (data.bankDetails.paidType === 'Cheque') {
				this.paymentMode = data.bankDetails.paidType;
				this.chequeInfo = 'Cheque Number';
			} else if (data.bankDetails.paidType === 'BankerCheque') {
				this.paymentMode = data.bankDetails.paidType;
				this.chequeInfo = 'Bankers Cheque Number';
			} else if (data.bankDetails.paidType === 'BankTransfer') {
				this.paymentMode = data.bankDetails.paidType;
				this.chequeInfo = 'Transfer Number';
			} else if (data.bankDetails.paidType === 'BankDeposit') {
				this.paymentMode = data.bankDetails.paidType;
				this.chequeInfo = 'Deposit Number';
			}
		});

		this.getPayment();
		this.getBank();

		this.emitHandler.passApplicantId.pipe(take(1)).subscribe((inputObject) => {
			this.dataService.dialogSubject.next([]);
			this.companyObject = inputObject;
			this.companyCode = inputObject.companyCode;
			this.companyID = inputObject.companyID;
			this.licenseType = inputObject.applicationinfo6;
			this.apiHandler.getApiRequest(this.accountoneRoute + '/getAccCompanyCode/' + this.companyCode).subscribe((data: any) => {
				this.form15Number = data[0]['receiptNumber15'];
			});
			this.dataService.loadDataForDialog(this.accountTwoRoute + '/getAccCompanyCode2/' + this.companyCode);
			this.dataService.dialogSubject.subscribe((val) => {
				this.dataSource1 = new MatTableDataSource(val);
				return val;
			});
		});
	}

	createForm() {
		this.form = this.formBuilder.group({
			bankDetails: this.formBuilder.group({
				bankName: new FormControl('', Validators.required),
				bankTransactionNo: new FormControl(''),
				bankTransactionNo2: new FormControl(''),
				paidType: new FormControl('', Validators.required),
				amountPaid: new FormControl('', Validators.required),
				receiptbank: new FormControl(''),
				remark1: new FormControl('')
			})
		});
	}
	getPayment() {
		this.apiHandler.getApiRequest(this.paymentMaster).subscribe((paymentApi) => {
			this.payment = paymentApi;
		});
	}
	getBank() {
		this.apiHandler.getApiRequest(this.bankMaster).subscribe((bankApi) => {
			this.bank = bankApi;
		});
	}

	reset() {
		this.form.reset();
	}
	submitBankDetails(body: any) {
		this.submitCounter = this.submitCounter + 1;
		body['companyCode'] = this.companyCode;
		body['typeOfApplyID'] = this.licenseType;
		body['whatApplyFor'] = this.licenseType;

		if (this.submitCounter === 1) {
			this.formDataToBeSubmitted = body;
		}
		if (this.formDataToBeSubmitted !== body || this.submitCounter === 1) {
			this.apiHandler.postApiRequest(this.accountTwoRoute, body).subscribe((data) => {
				this.dataService.loadDataForDialog(this.accountTwoRoute + '/getAccCompanyCode2/' + this.companyCode);
				this.dataService.dialogSubject.subscribe((val) => {
					this.dataSource1 = new MatTableDataSource(val);
					return val;
				});
				this.reset();
			});
		} else {
			alert('You already Submitted Same Bank Details');
		}
	}

	update(body: any) {
		body['acconutID'] = this.accountId;
		this.dataService.putDialogGrid2(this.accountTwoRoute, this.accountId, body, this.companyCode);
		this.reset();
	}

	edit(accountoneInfo: any) {
		let bankInfo: any;
		this.accountId = accountoneInfo.acconutID;
		bankInfo = { bankDetails: {} };
		bankInfo.bankDetails['bankName'] = accountoneInfo.bankName;
		bankInfo.bankDetails['paidType'] = accountoneInfo.paidType;
		bankInfo.bankDetails['receiptbank'] = accountoneInfo.receiptbank;
		bankInfo.bankDetails['amountPaid'] = accountoneInfo.amountPaid;
		bankInfo.bankDetails['bankTransactionNo'] = accountoneInfo.bankTransactionNo;
		bankInfo.bankDetails['bankTransactionNo2'] = accountoneInfo.bankTransactionNo2;
		bankInfo.bankDetails['remark1'] = accountoneInfo.remark1;
		this.form.patchValue(bankInfo);
		this.editMode = true;
	}
}
