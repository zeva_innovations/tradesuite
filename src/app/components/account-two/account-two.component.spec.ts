import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountTwoComponent } from './account-two.component';

describe('AccountTwoComponent', () => {
  let component: AccountTwoComponent;
  let fixture: ComponentFixture<AccountTwoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountTwoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
