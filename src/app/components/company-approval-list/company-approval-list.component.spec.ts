import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyApprovalListComponent } from './company-approval-list.component';

describe('CompanyApprovalListComponent', () => {
  let component: CompanyApprovalListComponent;
  let fixture: ComponentFixture<CompanyApprovalListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyApprovalListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyApprovalListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
