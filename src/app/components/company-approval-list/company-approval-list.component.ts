import { CommonFunctionsService } from 'src/app/services/common-functions.service';
import { CompanyDetailsFormService } from './../../services/company-details-form.service';
import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { MyErrorStateMatcher } from 'src/app/services/customer-details-form.service';
import { FormGroup } from '@angular/forms';
import { DataSourceServiceService } from 'src/app/services/data-source-service.service';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { environment } from 'src/environments/environment';
import * as moment from 'moment';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { ApiHandlerService } from 'src/app/services/api-handler.service';
import { EmitterHandlerService } from 'src/app/services/emitter-handler.service';

@Component({
	selector: 'app-company-approval-list',
	templateUrl: './company-approval-list.component.html',
	styleUrls: ['./company-approval-list.component.scss']
})
export class CompanyApprovalListComponent implements OnInit {
	constructor(
		private apiHandler: ApiHandlerService,
		private companyDetailsFormService: CompanyDetailsFormService,
		private dataService: DataSourceServiceService,
		private eventHandler: EmitterHandlerService,
		public commonFunction: CommonFunctionsService
	) { }
	displayedColumns = ['companyID', 'companyCode', 'applicationinfo6', 'companyName', 'companyType', 'companyAreaType', 'industryType', 'country', 'actions'];
	companyRoute = environment.tradeAppRoot + environment.getCompanyDetails;
	companyTypeMaster = environment.tradeAppRoot + environment.companyTypeMaster;
	companyAreaTypeMaster = environment.tradeAppRoot + environment.companyAreaType;
	industryTypeMaster = environment.tradeAppRoot + environment.industryType;

	loggedUserName = this.commonFunction.parseApplicantInfoFromlocalStorage()[5];
	loggedUserId = this.commonFunction.parseApplicantInfoFromlocalStorage()[6];
	applicantsByUser = this.companyRoute + '/get/' + this.loggedUserName + this.loggedUserId;

	isUGApproved0 = environment.tradeAppRoot + environment.isUGApproved0;
	isUGApproved1 = environment.tradeAppRoot + environment.isUGApproved1;

	showApprove = true;
	showReject = false;
	downloadCompanyInfo = environment.tradeAppRoot + '/api/Accounts_RegistrationAPI/GetCompanyApplicantformateview';
	@Input() group: FormGroup;
	myjson: any = JSON;
	dataSource = new MatTableDataSource([]);
	editMode = false;
	companyId = 0;
	companyType: string[] = [];
	companyAreaType: string[] = [];
	industryType: string[] = [];
	@ViewChild(MatSort) sort: MatSort;
	@ViewChild(MatPaginator) paginator: MatPaginator;
	matchFinder = new MyErrorStateMatcher();

	get applicantCode(): string {
		return this.commonFunction.applicantCodeForCompanyRegistration;
	}
	set applicantCode(value: string) {
		this.commonFunction.applicantCodeForCompanyRegistration = value;
	}

	ngOnInit() {
		this.commonFunction.goBackUrl = 'companyapproval';
		this.commonFunction.isDgApproval = false;
		this.dataService.loadApplicants(this.isUGApproved0);
		this.dataService.applicantSubject.subscribe((val) => {
			this.dataSource = new MatTableDataSource(val);
			this.dataSource.sort = this.sort;
			this.dataSource.paginator = this.paginator;
			return val;
		});
		this.getCompany();
		this.getCompanyAreaType();
		this.getIndustryType();
	}
	get form(): FormGroup {
		return this.companyDetailsFormService.formGroup;
	}

	add(body: string) {
		body['created_By'] = this.loggedUserName + this.loggedUserId;
		body['Org_ID'] = this.commonFunction.parseApplicantInfoFromlocalStorage()[2];
		body['Unit_Org_ID'] = this.commonFunction.parseApplicantInfoFromlocalStorage()[3];
		body['Branch_ID'] = this.commonFunction.parseApplicantInfoFromlocalStorage()[4];
		body['applicantCode'] = this.applicantCode;
		body['barCode'] = this.applicantCode + '_' + this.commonFunction.getUTCNow().toString();
		this.dataService.postApplicants(this.companyRoute, body);
		this.dataSource.sort = this.sort;
		this.dataSource.paginator = this.paginator;
	}
	edit(applicantID: number) {
		const getApplicantInfo = this.companyRoute + '/GetId/' + applicantID;
		this.apiHandler.getApiRequest(getApplicantInfo).pipe(catchError(() => of([]))).subscribe((company: any) => {
			let companyInfo: any;
			companyInfo = { companyDetails: {} };
			companyInfo.companyDetails['companyName'] = company.companyName;
			companyInfo.companyDetails['applicantCode'] = company.applicantCode;
			companyInfo.companyDetails['tin'] = company.tin;
			companyInfo.companyDetails['emailID'] = company.emailID;
			companyInfo.companyDetails['phoneNo'] = company.phoneNo;
			companyInfo.companyDetails['ieRegCertificateNo'] = company.ieRegCertificateNo;
			companyInfo.companyDetails['address1'] = company.address1;
			companyInfo.companyDetails['address2'] = company.address2;
			companyInfo.companyDetails['address3'] = company.address3;
			companyInfo.companyDetails['city'] = company.city;
			companyInfo.companyDetails['country'] = company.country;
			companyInfo.companyDetails['state'] = company.state;
			companyInfo.companyDetails['pin'] = company.pin;
			companyInfo.companyDetails['companyType'] = company.companyType;
			companyInfo.companyDetails['companyAreaType'] = company.companyAreaType;
			companyInfo.companyDetails['industryType'] = company.industryType;
			companyInfo.companyDetails['remark'] = company.remark;

			this.companyDetailsFormService.formGroup.setValue(companyInfo);
			this.editMode = true;
			this.companyId = applicantID;
		});
	}

	update(parameter: any, body: string) {
		body['companyID'] = parameter;
		body['edited_By'] = this.loggedUserName + this.loggedUserId;
		this.dataService.putApplicants(this.companyRoute, parameter, body);
	}
	delete(applicantID: number) {
		this.dataService.deleteApplicants(this.companyRoute, applicantID);
	}
	attach(applicantID: any) {
		this.eventHandler.emitApplicantId(applicantID);
	}
	applyFilter(filterValue: string) {
		this.dataSource.filter = filterValue.trim().toLowerCase();
	}
	reset() {
		this.form.reset();
	}
	getCompany() {
		this.apiHandler.getApiRequest(this.companyTypeMaster).subscribe((companyTypeMaster) => {
			this.companyType = companyTypeMaster;
		});
	}
	getCompanyAreaType() {
		this.apiHandler.getApiRequest(this.companyAreaTypeMaster).subscribe((companyAreaType) => {
			this.companyAreaType = companyAreaType;
		});
	}
	getIndustryType() {
		this.apiHandler.getApiRequest(this.industryTypeMaster).subscribe((industryType) => {
			this.industryType = industryType;
		});
	}

	companyDetails(value: any) {
		this.commonFunction.applicantCodeForCompanyRegistration = value;
	}

	changeTableFilter(tab) {
		if (tab.index === 0) {
			this.loadGridBased(this.isUGApproved0);
			this.showApprove = true;
			this.showReject = false;
		} else if (tab.index === 1) {
			this.loadGridBased(this.isUGApproved1);
			this.showApprove = false;
			this.showReject = true;
		}
	}
	loadGridBased(url: string) {
		this.dataService.loadApplicants(url);
		this.dataService.applicantSubject.subscribe((val) => {
			this.dataSource = new MatTableDataSource(val);
			this.dataSource.sort = this.sort;
			this.dataSource.paginator = this.paginator;
			return val;
		});
	}

	approve(companyID: string) {
		const body = { companyID: companyID, isApproved_UG: true };
		this.apiHandler.putApiRequest(this.companyRoute + '/' + companyID, JSON.stringify(body)).subscribe((data) => {
			this.loadGridBased(this.isUGApproved0);
		});
	}

	reject(companyID: string) {
		const body = { companyID: companyID, isApproved_UG: false };
		this.apiHandler.putApiRequest(this.companyRoute + '/' + companyID, JSON.stringify(body)).subscribe((data) => {
			this.loadGridBased(this.isUGApproved1);
		});
	}

	downloadFile(companyCode: any) {
		this.apiHandler.getApiRequest(this.downloadCompanyInfo + '/' + companyCode).subscribe((data) => {
			let res: any;
			res = data;
			const fileName = res.applicationPath;
			const wnd = window.open(environment.tradeAppRoot + '/api/Accounts_RegistrationAPI/GetApplicationformate/' + fileName, '_blank');
			setTimeout(function () {
				wnd.close();
			}, 3000);
			return false;
		});
	}
}
