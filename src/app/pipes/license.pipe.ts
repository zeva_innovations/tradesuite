import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'license'
})
export class LicensePipe implements PipeTransform {

  transform(value: string, [separator]): string {
    let transformedLicense = '';

    if (value !== undefined && value !== null) {
      let splits = value.split(separator);
      transformedLicense = splits.length > 0 ? splits[1] : '';
    }
    return transformedLicense;

  }

}
