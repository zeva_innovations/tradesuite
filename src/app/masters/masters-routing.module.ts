import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BankComponent } from './components/bank/bank.component';
import { DocumentassigntypeComponent } from './components/documentassigntype/documentassigntype.component';
import { EmployeeGradeComponent } from './components/employee-grade/employee-grade.component';
import { EmployeeaddressComponent } from './components/employeeaddress/employeeaddress.component';
import { EmployeeCarrierComponent } from './components/employee-carrier/employee-carrier.component';
import { EmployeeComponent } from './components/employee/employee.component';
import { EmployeedependentComponent } from './components/employeedependent/employeedependent.component';
import { EmployeeeducationComponent } from './components/employeeeducation/employeeeducation.component';
import { EmployeeLeaveComponent } from './components/employee-leave/employee-leave.component';
import { EmployeeSalaryComponent } from './components/employee-salary/employee-salary.component';
import { MainMenuMasterComponent } from './components/main-menu-master/main-menu-master.component';
import { UserRoleComponent } from './components/user-role/user-role.component';
import { BankratetypeComponent } from './components/bankratetype/bankratetype.component';
import { DefecttypeComponent } from './components/defecttype/defecttype.component';
import { FeeTypeComponent } from './components/fee-type/fee-type.component';
import { HsMainCodeComponent } from './components/hs-main-code/hs-main-code.component';
import { HssubheadComponent } from './components/hssubhead/hssubhead.component';
import { DocumentTypeComponent } from './components/document-type/document-type.component';
import { CompanyTypeComponent } from './components/company-type/company-type.component';
import { CompanyAreaTypeComponent } from './components/company-area-type/company-area-type.component';
import { CurrencyTypeComponent } from './components/currency-type/currency-type.component';
import { DepartmentReceiptTypeComponent } from './components/department-receipt-type/department-receipt-type.component';
import { DepartmentTypeComponent } from './components/department-type/department-type.component';
import { GroupTypeComponent } from './components/group-type/group-type.component';
import { HsChapterComponent } from './components/hs-chapter/hs-chapter.component';
import { HsHeadComponent } from './components/hs-head/hs-head.component';
import { IndustrytypeComponent } from './components/industrytype/industrytype.component';
import { LocationAreaComponent } from './components/location-area/location-area.component';
import { CompanyDocumentComponent } from './components/company-document/company-document.component';
import { NationalityComponent } from './components/nationality/nationality.component';
import { OrganizationLogoComponent } from './components/organization-logo/organization-logo.component';
import { OrganizationComponent } from './components/organization/organization.component';
import { PaymentModeComponent } from './components/payment-mode/payment-mode.component';
import { ReceiptTypeComponent } from './components/receipt-type/receipt-type.component';
import { ReporttemplateTypeComponent } from './components/reporttemplate-type/reporttemplate-type.component';
import { ReportHsComponent } from './components/report-hs/report-hs.component';
import { TaxComponent } from './components/tax/tax.component';
import { TitleTypeComponent } from './components/title-type/title-type.component';
import { TransportTypeComponent } from './components/transport-type/transport-type.component';
import { UnitBranchComponent } from './components/unit-branch/unit-branch.component';
import { UnitHSComponent } from './components/unit-hs/unit-hs.component';
import { UnitComponent } from './components/unit/unit.component';
import { UnitTypeComponent } from './components/unit-type/unit-type.component';
import { YearComponent } from './components/year/year.component';
import { TypeofapplyComponent } from './components/typeofapply/typeofapply.component';
import { MainmastersComponent } from './components/mainmasters/mainmasters.component';
import {LocationComponent} from './components/location/location.component';
const routes: Routes = [
	{
		path: '',
		component: MainmastersComponent,
		children: [
			{ path: 'documentType', component: DocumentTypeComponent },
			{ path: 'companyType', component: CompanyTypeComponent },
			{ path: 'Bank', component: BankComponent },
			{ path: 'BankRateType', component: BankratetypeComponent },
			{ path: 'DefectTemplate', component: DefecttypeComponent },
			{ path: 'FeeType', component: FeeTypeComponent },
			{ path: 'companyAreaType', component: CompanyAreaTypeComponent },
			{ path: 'currency', component: CurrencyTypeComponent },
			{ path: 'departmentreceiptType', component: DepartmentReceiptTypeComponent },
			{ path: 'departmentType', component: DepartmentTypeComponent },
			{ path: 'groupType', component: GroupTypeComponent },
			{ path: 'HsChapter', component: HsChapterComponent },
			{ path: 'HsHead', component: HsHeadComponent },
			{ path: 'IndustryType', component: IndustrytypeComponent },
			{ path: 'LocationArea', component: LocationAreaComponent },
			{ path: 'CompanyDocument', component: CompanyDocumentComponent },
			{ path: 'Nationality', component: NationalityComponent },
			{ path: 'OrganizationLogo', component: OrganizationLogoComponent },
			{ path: 'Organization', component: OrganizationComponent },
			{ path: 'Payment', component: PaymentModeComponent },
			{ path: 'ReceiptType', component: ReceiptTypeComponent },
			{ path: 'ReportTemplate', component: ReporttemplateTypeComponent },
			{ path: 'ReportHs', component: ReportHsComponent },
			{ path: 'Tax', component: TaxComponent },
			{ path: 'TitleType', component: TitleTypeComponent },
			{ path: 'TransportType', component: TransportTypeComponent },
			{ path: 'UnitBranch', component: UnitBranchComponent },
			{ path: 'UnitHS', component: UnitHSComponent },
			{ path: 'Unit', component: UnitComponent },
			{ path: 'UnitType', component: UnitTypeComponent },
			{ path: 'Year', component: YearComponent },
			{ path: 'TypeOfApply', component: TypeofapplyComponent },
			{ path: 'HsMain', component: HsMainCodeComponent },
			{ path: 'HsSub', component: HssubheadComponent },
			{ path: 'DocumentAssignType', component: DocumentassigntypeComponent },
			{ path: 'EmployeeGrade', component: EmployeeGradeComponent },
			{ path: 'Employee', component: EmployeeComponent },
			{ path: 'EmployeeAddress', component: EmployeeaddressComponent },
			{ path: 'EmployeeCarrier', component: EmployeeCarrierComponent },
			{ path: 'EmployeeDependent', component: EmployeedependentComponent },
			{ path: 'EmployeeEducation', component: EmployeeeducationComponent },
			{ path: 'EmployeeLeave', component: EmployeeLeaveComponent },
			{ path: 'EmployeeSalary', component: EmployeeSalaryComponent },
			{ path: 'MainMenuMaster', component: MainMenuMasterComponent },
			{ path: 'UnitRoleMaster', component: UserRoleComponent },
      { path: 'Location', component: LocationComponent }
		]
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class MastersRoutingModule {}
