import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MastersRoutingModule } from './masters-routing.module';
import { BankComponent } from './components/bank/bank.component';
import { DocumentTypeComponent } from './components/document-type/document-type.component';
import { CompanyTypeComponent } from './components/company-type/company-type.component';
import { CompanyAreaTypeComponent } from './components/company-area-type/company-area-type.component';
import { CurrencyTypeComponent } from './components/currency-type/currency-type.component';
import { DepartmentReceiptTypeComponent } from './components/department-receipt-type/department-receipt-type.component';
import { DepartmentTypeComponent } from './components/department-type/department-type.component';
import { GroupTypeComponent } from './components/group-type/group-type.component';
import { HsChapterComponent } from './components/hs-chapter/hs-chapter.component';
import { HsHeadComponent } from './components/hs-head/hs-head.component';
import { IndustrytypeComponent } from './components/industrytype/industrytype.component';
import { LocationAreaComponent } from './components/location-area/location-area.component';
import { CompanyDocumentComponent } from './components/company-document/company-document.component';
import { NationalityComponent } from './components/nationality/nationality.component';
import { OrganizationLogoComponent } from './components/organization-logo/organization-logo.component';
import { OrganizationComponent } from './components/organization/organization.component';
import { PaymentModeComponent } from './components/payment-mode/payment-mode.component';
import { ReceiptTypeComponent } from './components/receipt-type/receipt-type.component';
import { ReporttemplateTypeComponent } from './components/reporttemplate-type/reporttemplate-type.component';
import { ReportHsComponent } from './components/report-hs/report-hs.component';
import { SetupPerRateComponent } from './components/setup-per-rate/setup-per-rate.component';
import { TaxComponent } from './components/tax/tax.component';
import { TitleTypeComponent } from './components/title-type/title-type.component';
import { TransportTypeComponent } from './components/transport-type/transport-type.component';
import { UnitBranchComponent } from './components/unit-branch/unit-branch.component';
import { UnitHSComponent } from './components/unit-hs/unit-hs.component';
import { UnitComponent } from './components/unit/unit.component';
import { UnitTypeComponent } from './components/unit-type/unit-type.component';
import { YearComponent } from './components/year/year.component';
import { TypeofapplyComponent } from './components/typeofapply/typeofapply.component';
import { BankratetypeComponent } from './components/bankratetype/bankratetype.component';
import { DefecttypeComponent } from './components/defecttype/defecttype.component';
import { FeeTypeComponent } from './components/fee-type/fee-type.component';
import { HsMainCodeComponent } from './components/hs-main-code/hs-main-code.component';
import { HssubheadComponent } from './components/hssubhead/hssubhead.component';
import { DocumentassigntypeComponent } from './components/documentassigntype/documentassigntype.component';
import { EmployeeComponent } from './components/employee/employee.component';
import { EmployeeaddressComponent } from './components/employeeaddress/employeeaddress.component';
import { EmployeeSalaryComponent } from './components/employee-salary/employee-salary.component';
import { EmployeedependentComponent } from './components/employeedependent/employeedependent.component';
import { EmployeeLeaveComponent } from './components/employee-leave/employee-leave.component';
import { EmployeeeducationComponent } from './components/employeeeducation/employeeeducation.component';
import { EmployeeCarrierComponent } from './components/employee-carrier/employee-carrier.component';
import { EmployeeGradeComponent } from './components/employee-grade/employee-grade.component';
import { MainMenuMasterComponent } from './components/main-menu-master/main-menu-master.component';
import { UserRoleComponent } from './components/user-role/user-role.component';
import { MainmastersComponent } from './components/mainmasters/mainmasters.component';
import { SidenavbarmastersComponent } from './components/sidenavbarmasters/sidenavbarmasters.component';

import {
	MatButtonModule,
	MatCheckboxModule,
	MatFormFieldModule,
	MatIconModule,
	MatInputModule,
	MatListModule,
	MatSelectModule,
	MatToolbarModule,
	MatDatepickerModule,
	MatSnackBarModule,
	MatGridListModule,
	MatTableModule,
	MatProgressSpinnerModule,
	MatPaginatorModule,
	MatSortModule,
	MatDialogModule,
	MatCardModule,
	MatSidenavModule,
	MatMenuModule,
	MatTabsModule,
	MatChipsModule,
	MatTooltipModule,
	MatExpansionModule,
	MatProgressBarModule
} from '@angular/material';
import { LocationComponent } from './components/location/location.component';

@NgModule({
	declarations: [
		MainmastersComponent,
		SidenavbarmastersComponent,
		BankComponent,
		CompanyTypeComponent,
		CompanyAreaTypeComponent,
		CurrencyTypeComponent,
		DepartmentReceiptTypeComponent,
		DepartmentTypeComponent,
		GroupTypeComponent,
		HsChapterComponent,
		HsHeadComponent,
		IndustrytypeComponent,
		LocationAreaComponent,
		CompanyDocumentComponent,
		NationalityComponent,
		OrganizationLogoComponent,
		OrganizationComponent,
		PaymentModeComponent,
		ReceiptTypeComponent,
		ReporttemplateTypeComponent,
		ReportHsComponent,
		SetupPerRateComponent,
		TaxComponent,
		TitleTypeComponent,
		TransportTypeComponent,
		UnitBranchComponent,
		UnitHSComponent,
		UnitComponent,
		UnitTypeComponent,
		YearComponent,
		TypeofapplyComponent,
		BankratetypeComponent,
		DefecttypeComponent,
		FeeTypeComponent,
		HsMainCodeComponent,
		HssubheadComponent,
		DocumentassigntypeComponent,
		EmployeeComponent,
		EmployeeaddressComponent,
		EmployeeCarrierComponent,
		EmployeedependentComponent,
		EmployeeeducationComponent,
		EmployeeLeaveComponent,
		EmployeeSalaryComponent,
		EmployeeGradeComponent,
		MainMenuMasterComponent,
		UserRoleComponent,
		DocumentTypeComponent,
		LocationComponent
	],
	imports: [
		CommonModule,
		SharedModule,
		MastersRoutingModule,
		FormsModule,
		ReactiveFormsModule,
		MatButtonModule,
		MatCheckboxModule,
		MatFormFieldModule,
		MatIconModule,
		MatInputModule,
		MatListModule,
		MatSelectModule,
		MatToolbarModule,
		MatDatepickerModule,
		MatSnackBarModule,
		MatGridListModule,
		MatTableModule,
		MatProgressSpinnerModule,
		MatPaginatorModule,
		MatSortModule,
		MatDialogModule,
		MatCardModule,
		MatSidenavModule,
		MatMenuModule,
		MatTabsModule,
		MatChipsModule,
		MatTooltipModule,
		MatExpansionModule,
		MatProgressBarModule
	]
})
export class MastersModule {}
