import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {ApiHandlerService} from '../../../services/api-handler.service';
import {DataSourceMasterService} from '../../../services/data-source-master.service';
import {CommonFunctionsService} from '../../../services/common-functions.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MatPaginator, MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';
import {environment} from '../../../../environments/environment';
import {MyErrorStateMatcher} from '../../../services/customer-details-form.service';
import {catchError} from 'rxjs/operators';
import {of} from 'rxjs';

@Component({
  selector: 'app-employee-salary',
  templateUrl: './employee-salary.component.html',
  styleUrls: ['./employee-salary.component.scss']
})
export class EmployeeSalaryComponent implements OnInit {

  constructor(
    private apiHandler: ApiHandlerService,
    private dataService: DataSourceMasterService,
    public commonFunction: CommonFunctionsService,
    private formBuilder: FormBuilder,
    private matSnackBar: MatSnackBar,
  ) {}
  displayedColumns = ['employee_Salary_Master_ID', 'emp_ID', 'basicSalary', 'pf', 'hra', 'transport', 'bonus', 'actions'];
  EmployeeSalaryMaster = environment.tradeAppRoot + environment.EmployeeSalaryMaster;

  loggedUserName = this.commonFunction.parseApplicantInfoFromlocalStorage()[5];
  loggedUserId = this.commonFunction.parseApplicantInfoFromlocalStorage()[6];

  @Input() group: FormGroup;
  myjson: any = JSON;
  dataSource = new MatTableDataSource([]);
  editMode = false;
  masterId = 0;
  companyType: string[] = [];
  companyAreaType: string[] = [];
  industryType: string[] = [];
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  matchFinder = new MyErrorStateMatcher();
  form: FormGroup;

  ngOnInit() {
    this.dataService.loadApplicants(this.EmployeeSalaryMaster);
    this.dataService.dataSubject.subscribe((val) => {
      this.dataSource = new MatTableDataSource(val);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      return val;
    });

    this.form = this.formBuilder.group({
      employeeSalaryMasters: this.formBuilder.group({
        emp_ID: new FormControl('', [Validators.required]),
        basicSalary: new FormControl('', [Validators.required]),
        pf: new FormControl('', [Validators.required]),
        hra: new FormControl('', [Validators.required]),
        transport: new FormControl('', [Validators.required]),
        bonus: new FormControl('', [Validators.required]),
        status: new FormControl('', [Validators.required])
      })
    });
  }

  add(body: string) {
    body['created_By'] = this.loggedUserName + this.loggedUserId;
    this.dataService.postApplicants(this.EmployeeSalaryMaster, body);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.reset();
  }
  edit(anyId: number) {
    this.masterId = anyId;
    const getMasterInfo = this.EmployeeSalaryMaster + '/' + anyId;
    this.apiHandler.getApiRequest(getMasterInfo).pipe(catchError(() => of([]))).subscribe((employeeSalary: any) => {
      let employeeSalaryObject: any;
      employeeSalaryObject = { employeeSalaryMasters: {} };
      employeeSalaryObject.employeeSalaryMasters['emp_ID'] = employeeSalary.emp_ID;
      employeeSalaryObject.employeeSalaryMasters['basicSalary'] = employeeSalary.basicSalary;
      employeeSalaryObject.employeeSalaryMasters['pf'] = employeeSalary.pf;
      employeeSalaryObject.employeeSalaryMasters['hra'] = employeeSalary.hra;
      employeeSalaryObject.employeeSalaryMasters['transport'] = employeeSalary.transport;
      employeeSalaryObject.employeeSalaryMasters['bonus'] = employeeSalary.bonus;
      employeeSalaryObject.employeeSalaryMasters['status'] = employeeSalary.status;


      this.form.setValue(employeeSalaryObject);
      this.editMode = true;
    });
  }

  update(body: string) {
    body['employee_Salary_Master_ID'] = this.masterId;
    body['edited_By'] = this.loggedUserName + this.loggedUserId;
    this.dataService.putApplicants(this.EmployeeSalaryMaster, this.masterId, body);
    this.reset();
  }
  delete(applicantID: number) {
    this.dataService.deleteApplicants(this.EmployeeSalaryMaster, applicantID);
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  reset() {
    this.form.reset();
  }
  openSnackBar(message: string, action: string) {
    this.matSnackBar.open('Saved Sucessfully', 'Close', {
      duration: 3000
    });
  }
  updateSnackBar(message: string, action: string) {
    this.matSnackBar.open('Updated Sucessfully', 'Close', {
      duration: 3000
    });
  }
  deleteSnackBar(message: string, action: string) {
    this.matSnackBar.open('Deleted Sucessfully', 'Close', {
      duration: 3000
    });
  }
}
