import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ApiHandlerService } from '../../../services/api-handler.service';
import { DataSourceMasterService } from '../../../services/data-source-master.service';
import { CommonFunctionsService } from '../../../services/common-functions.service';
import { MatPaginator, MatSnackBar, MatSort, MatTableDataSource } from '@angular/material';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { environment } from '../../../../environments/environment';
import { MyErrorStateMatcher } from '../../../services/customer-details-form.service';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import * as _ from 'lodash';
@Component({
	selector: 'app-user-role',
	templateUrl: './user-role.component.html',
	styleUrls: ['./user-role.component.scss']
})
export class UserRoleComponent implements OnInit {
	constructor(
		private apiHandler: ApiHandlerService,
		private dataService: DataSourceMasterService,
		public commonFunction: CommonFunctionsService,
		private matSnackBar: MatSnackBar,
		private formBuilder: FormBuilder
	) {}
	displayedColumns = [
		'user_Menu_Right_ID',
		'user_Code',
		'reaD_Access',
		'writE_Access',
		'deletE_Access',
		'ediT_Access',
		'prinT_Access',
		'approvaL_Access',
		'fulL_Access',
		'canceL_Access',
		'actions'
	];
	UserRoleMaster = environment.tradeAppRoot + environment.UserRoleMaster;
	organizationMaster = environment.tradeAppRoot + environment.organizationMaster;
	EmployeeMasters = environment.tradeAppRoot + environment.EmployeeMasters;
	unitMaster = environment.tradeAppRoot + environment.unitMaster;

	unitBranchMaster = environment.tradeAppRoot + environment.unitBranchMaster;
	menuLinkMaster = environment.tradeAppRoot + environment.menuLinkMaster;
	loggedUserName = this.commonFunction.parseApplicantInfoFromlocalStorage()[5];
	loggedUserId = this.commonFunction.parseApplicantInfoFromlocalStorage()[6];

	@Input() group: FormGroup;
	myjson: any = JSON;
	dataSource = new MatTableDataSource([]);
	editMode = false;
	masterId = 0;
	employeeMaster: string[] = [];
	organisationMaster: string[] = [];
	UnitorganisationMaster: string[] = [];
	BranchorganisationMaster: string[] = [];
	menuLinks: string[] = [];
	companyType: string[] = [];
	companyAreaType: string[] = [];
	industryType: string[] = [];
	@ViewChild(MatSort) sort: MatSort;
	@ViewChild(MatPaginator) paginator: MatPaginator;
	matchFinder = new MyErrorStateMatcher();
	form: FormGroup;

	ngOnInit() {
		this.dataService.loadApplicants(this.UserRoleMaster);
		this.dataService.dataSubject.subscribe((val) => {
			this.dataSource = new MatTableDataSource(val);
			this.dataSource.sort = this.sort;
			this.dataSource.paginator = this.paginator;
			return val;
		});
		this.getEmployee();
		this.getMenuRoutes();
		this.form = this.formBuilder.group({
			userRoleMasters: this.formBuilder.group({
				user_Code: new FormControl('', [Validators.required]),
				mainMenu: new FormControl('', [Validators.required]),
				reaD_Access: new FormControl('', [Validators.required]),
				writE_Access: new FormControl('', [Validators.required]),
				deletE_Access: new FormControl('', [Validators.required]),
				ediT_Access: new FormControl('', [Validators.required]),
				prinT_Access: new FormControl('', [Validators.required]),
				approvaL_Access: new FormControl('', [Validators.required]),
				fulL_Access: new FormControl('', [Validators.required]),
				canceL_Access: new FormControl('', [Validators.required]),
				status: new FormControl('', [Validators.required])
			})
		});
	}

	getEmployee() {
		this.apiHandler.getApiRequest(this.EmployeeMasters).subscribe((yearApi) => {
			this.employeeMaster = yearApi;
		});
	}
	getOrganisation() {
		this.apiHandler.getApiRequest(this.organizationMaster).subscribe((yearApi) => {
			this.organisationMaster = yearApi;
		});
	}
	getUnitOrg() {
		this.apiHandler.getApiRequest(this.unitMaster).subscribe((yearApi) => {
			this.UnitorganisationMaster = yearApi;
		});
	}
	getUniBranchOrg() {
		this.apiHandler.getApiRequest(this.unitBranchMaster).subscribe((yearApi) => {
			this.BranchorganisationMaster = yearApi;
		});
	}
	add(body: string) {
		const selectedControllerName = body['mainMenu'];
		let menucode = _.find(this.menuLinks, { controller: selectedControllerName });
		menucode = menucode['id'];
		body['created_By'] = this.loggedUserName + this.loggedUserId;
		body['menu_Code'] = menucode;

		this.dataService.postApplicants(this.UserRoleMaster, body);
		this.dataSource.sort = this.sort;
		this.dataSource.paginator = this.paginator;
		this.reset();
		this.editMode = false;
	}
	edit(anyId: number) {
		this.masterId = anyId;
		const getMasterInfo = this.UserRoleMaster + '/' + anyId;
		this.apiHandler.getApiRequest(getMasterInfo).pipe(catchError(() => of([]))).subscribe((userType: any) => {
			let unitRoleObject: any;

			unitRoleObject = { userRoleMasters: {} };
			unitRoleObject.userRoleMasters['user_Code'] = userType.user_Code;
			unitRoleObject.userRoleMasters['mainMenu'] = userType.mainMenu;
			unitRoleObject.userRoleMasters['reaD_Access'] = userType.reaD_Access;
			unitRoleObject.userRoleMasters['writE_Access'] = userType.writE_Access;
			unitRoleObject.userRoleMasters['deletE_Access'] = userType.deletE_Access;
			unitRoleObject.userRoleMasters['ediT_Access'] = userType.ediT_Access;
			unitRoleObject.userRoleMasters['prinT_Access'] = userType.prinT_Access;
			unitRoleObject.userRoleMasters['approvaL_Access'] = userType.approvaL_Access;
			unitRoleObject.userRoleMasters['fulL_Access'] = userType.fulL_Access;
			unitRoleObject.userRoleMasters['canceL_Access'] = userType.canceL_Access;
			unitRoleObject.userRoleMasters['status'] = userType.status;
			this.form.setValue(unitRoleObject);
			this.editMode = true;
		});
	}

	update(body: string) {
		body['user_Menu_Right_ID'] = this.masterId;
		body['edited_By'] = this.loggedUserName + this.loggedUserId;
		const selectedControllerName = body['mainMenu'];
		let menucode = _.find(this.menuLinks, { controller: selectedControllerName });
		menucode = menucode['id'];
		body['menu_Code'] = menucode;
		this.dataService.putApplicants(this.UserRoleMaster, this.masterId, body);
		this.reset();
		this.editMode = false;
	}
	delete(applicantID: number) {
		this.dataService.deleteApplicants(this.UserRoleMaster, applicantID);
	}
	openSnackBar(message: string, action: string) {
		this.matSnackBar.open('Saved Sucessfully', 'Close', {
			duration: 3000
		});
	}
	updateSnackBar(message: string, action: string) {
		this.matSnackBar.open('Updated Sucessfully', 'Close', {
			duration: 3000
		});
	}
	deleteSnackBar(message: string, action: string) {
		this.matSnackBar.open('Deleted Sucessfully', 'Close', {
			duration: 3000
		});
	}
	applyFilter(filterValue: string) {
		this.dataSource.filter = filterValue.trim().toLowerCase();
	}
	reset() {
		this.form.reset();
		this.editMode = false;
	}
	getMenuRoutes() {
		this.apiHandler.getApiRequest(this.menuLinkMaster).subscribe((yearApi) => {
			this.menuLinks = yearApi;
		});
	}
}
