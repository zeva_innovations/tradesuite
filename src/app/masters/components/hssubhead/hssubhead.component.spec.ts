import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HssubheadComponent } from './hssubhead.component';

describe('HssubheadComponent', () => {
  let component: HssubheadComponent;
  let fixture: ComponentFixture<HssubheadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HssubheadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HssubheadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
