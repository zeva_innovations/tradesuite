import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ApiHandlerService } from '../../../services/api-handler.service';
import { DataSourceMasterService } from '../../../services/data-source-master.service';
import { CommonFunctionsService } from '../../../services/common-functions.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatPaginator, MatSnackBar, MatSort, MatTableDataSource } from '@angular/material';
import { environment } from '../../../../environments/environment';
import { MyErrorStateMatcher } from '../../../services/customer-details-form.service';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Component({
	selector: 'app-employeedependent',
	templateUrl: './employeedependent.component.html',
	styleUrls: ['./employeedependent.component.scss']
})
export class EmployeedependentComponent implements OnInit {
	constructor(
		private apiHandler: ApiHandlerService,
		private dataService: DataSourceMasterService,
		public commonFunction: CommonFunctionsService,
		private formBuilder: FormBuilder,
		private matSnackBar: MatSnackBar
	) {}
	displayedColumns = ['employee_Dependents_Master_ID', 'emp_ID', 'dependent_type', 'dependent_Name', 'doB_of_Dependent', 'dependent_Status', 'actions'];
	EmployeeDependentMaster = environment.tradeAppRoot + environment.EmployeeDependentMaster;

	loggedUserName = this.commonFunction.parseApplicantInfoFromlocalStorage()[5];
	loggedUserId = this.commonFunction.parseApplicantInfoFromlocalStorage()[6];

	@Input() group: FormGroup;
	myjson: any = JSON;
	dataSource = new MatTableDataSource([]);
	editMode = false;
	masterId = 0;
	companyType: string[] = [];
	companyAreaType: string[] = [];
	industryType: string[] = [];
	@ViewChild(MatSort) sort: MatSort;
	@ViewChild(MatPaginator) paginator: MatPaginator;
	matchFinder = new MyErrorStateMatcher();
	form: FormGroup;

	ngOnInit() {
		this.dataService.loadApplicants(this.EmployeeDependentMaster);
		this.dataService.dataSubject.subscribe((val) => {
			this.dataSource = new MatTableDataSource(val);
			this.dataSource.sort = this.sort;
			this.dataSource.paginator = this.paginator;
			return val;
		});

		this.form = this.formBuilder.group({
			employeeDependentMasters: this.formBuilder.group({
				emp_ID: new FormControl('', [Validators.required]),
				dependent_type: new FormControl('', [Validators.required]),
				dependent_Name: new FormControl('', [Validators.required]),
				doB_of_Dependent: new FormControl({ value: '', disabled: false }),
				dependent_Status: new FormControl('', [Validators.required]),
				status: new FormControl('', [Validators.required])
			})
		});
	}

	add(body: string) {
		body['created_By'] = this.loggedUserName + this.loggedUserId;
		this.dataService.postApplicants(this.EmployeeDependentMaster, body);
		this.dataSource.sort = this.sort;
		this.dataSource.paginator = this.paginator;
		this.reset();
	}
	edit(anyId: number) {
		this.masterId = anyId;
		const getMasterInfo = this.EmployeeDependentMaster + '/' + anyId;
		this.apiHandler.getApiRequest(getMasterInfo).pipe(catchError(() => of([]))).subscribe((employeeAddress: any) => {
			let employeeAddressObject: any;
			employeeAddressObject = { employeeDependentMasters: {} };
			employeeAddressObject.employeeDependentMasters['emp_ID'] = employeeAddress.emp_ID;
			employeeAddressObject.employeeDependentMasters['dependent_type'] = employeeAddress.dependent_type;
			employeeAddressObject.employeeDependentMasters['dependent_Name'] = employeeAddress.dependent_Name;
			employeeAddressObject.employeeDependentMasters['doB_of_Dependent'] = employeeAddress.doB_of_Dependent;
			employeeAddressObject.employeeDependentMasters['dependent_Status'] = employeeAddress.dependent_Status;
			employeeAddressObject.employeeDependentMasters['status'] = employeeAddress.status;

			this.form.setValue(employeeAddressObject);
			this.editMode = true;
		});
	}

	update(body: string) {
		body['employee_Dependents_Master_ID'] = this.masterId;
		body['edited_By'] = this.loggedUserName + this.loggedUserId;
		this.dataService.putApplicants(this.EmployeeDependentMaster, this.masterId, body);
		this.reset();
	}
	delete(applicantID: number) {
		this.dataService.deleteApplicants(this.EmployeeDependentMaster, applicantID);
	}

	applyFilter(filterValue: string) {
		this.dataSource.filter = filterValue.trim().toLowerCase();
	}
	reset() {
		this.form.reset();
	}
	openSnackBar(message: string, action: string) {
		this.matSnackBar.open('Saved Sucessfully', 'Close', {
			duration: 3000
		});
	}
	updateSnackBar(message: string, action: string) {
		this.matSnackBar.open('Updated Sucessfully', 'Close', {
			duration: 3000
		});
	}
	deleteSnackBar(message: string, action: string) {
		this.matSnackBar.open('Deleted Sucessfully', 'Close', {
			duration: 3000
		});
	}
	checkDates(control: FormControl) {
		if (control.value === '' || control.value === null) {
			return { dobNull: true };
		}
		return null;
	}
}
