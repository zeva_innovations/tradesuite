import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeedependentComponent } from './employeedependent.component';

describe('EmployeedependentComponent', () => {
  let component: EmployeedependentComponent;
  let fixture: ComponentFixture<EmployeedependentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeedependentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeedependentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
