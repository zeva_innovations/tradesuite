import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnitHSComponent } from './unit-hs.component';

describe('UnitHSComponent', () => {
  let component: UnitHSComponent;
  let fixture: ComponentFixture<UnitHSComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnitHSComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnitHSComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
