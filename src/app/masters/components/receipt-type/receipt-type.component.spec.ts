import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReceiptTypeComponent } from './receipt-type.component';

describe('ReceiptTypeComponent', () => {
  let component: ReceiptTypeComponent;
  let fixture: ComponentFixture<ReceiptTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReceiptTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReceiptTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
