import { CommonFunctionsService } from '../../../services/common-functions.service';
import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'app-sidenavbarmasters',
	templateUrl: './sidenavbarmasters.component.html',
	styleUrls: ['./sidenavbarmasters.component.scss']
})
export class SidenavbarmastersComponent implements OnInit {
	constructor(private commonFunction: CommonFunctionsService) {}

	loginRoute: ROUTE[] = [
		{
			icon: 'home',
			route: '/welcome',
			title: 'Home'
		}
	];
	otherRoutes: ROUTE[] = [
		{
			icon: 'accessibility',
			route: '/MainMaster/Location',
			title: 'Location'
		},
		{
			icon: 'accessibility',
			route: '/MainMaster/IndustryType',
			title: 'Industry Type'
		},
		/**
		{
			icon: 'accessibility',
			route: '/MainMaster/departmentType',
			title: 'Department Type'
		},
		{
			icon: 'accessibility',
			route: '/MainMaster/groupType',
			title: 'Group Type'
		},
		{
			icon: 'accessibility',
			route: '/MainMaster/UnitHS',
			title: 'UnitHS'
		},


		{
			icon: 'accessibility',
			route: '/MainMaster/LocationArea',
			title: 'Location Area'
		},
		{
			icon: 'accessibility',
			route: '/MainMaster/departmentreceiptType',
			title: 'Receipt Type'
		},

		{
			icon: 'accessibility',
			route: '/MainMaster/Nationality',
			title: 'Nationality'
		},

		{
			icon: 'accessibility',
			route: '/MainMaster/TitleType',
			title: 'TitleType'
		},
			*/
		{
			icon: 'accessibility',
			route: '/MainMaster/TransportType',
			title: 'TransportType'
		}
	];

	logOutRoutes: ROUTE[] = [
		{
			icon: 'cancel',
			route: '/login',
			title: 'Logout'
		}
	];

	hsMasterRoutes: ROUTE[] = [
		{
			icon: 'accessibility',
			route: '/MainMaster/HsHead',
			title: 'HS Head'
		},
		{
			icon: 'accessibility',
			route: '/MainMaster/HsMain',
			title: 'HS Main'
		},
		{
			icon: 'accessibility',
			route: '/MainMaster/HsSub',
			title: 'HS Sub'
		},
		{
			icon: 'accessibility',
			route: '/MainMaster/HsChapter',
			title: 'HS Chapter'
		}
	];
	applyTypeRoutes: ROUTE[] = [
		{
			icon: 'accessibility',
			route: '/MainMaster/TypeOfApply',
			title: 'Apply Type'
		},
		{
			icon: 'accessibility',
			route: '/MainMaster/Year',
			title: 'Year'
		},
		{
			icon: 'accessibility',
			route: '/MainMaster/companyType',
			title: 'Company Type'
		},
		{
			icon: 'accessibility',
			route: '/MainMaster/ReceiptType',
			title: 'ReceiptType'
		},

		{
			icon: 'accessibility',
			route: '/MainMaster/companyAreaType',
			title: 'Company Area Type'
		}
	];
	bankmasters: ROUTE[] = [
		{
			icon: 'accessibility',
			route: '/MainMaster/Bank',
			title: 'Bank'
		},
		{
			icon: 'accessibility',
			route: '/MainMaster/BankRateType',
			title: 'BankRateType'
		},
		{
			icon: 'accessibility',
			route: '/MainMaster/currency',
			title: 'Currency'
		}
	];
	organizationmasters: ROUTE[] = [
		{
			icon: 'accessibility',
			route: '/MainMaster/OrganizationLogo',
			title: 'Organization Logo'
		},
		{
			icon: 'accessibility',
			route: '/MainMaster/Organization',
			title: 'Organization'
		},
		{
			icon: 'accessibility',
			route: '/MainMaster/UnitBranch',
			title: 'UnitBranch'
		},

		{
			icon: 'accessibility',
			route: '/MainMaster/Unit',
			title: 'Unit'
		},
		{
			icon: 'accessibility',
			route: '/MainMaster/UnitType',
			title: 'UnitType'
		}
	];
	accountMasters: ROUTE[] = [
		{
			icon: 'accessibility',
			route: '/MainMaster/Payment',
			title: 'Payment'
		},
		{
			icon: 'accessibility',
			route: '/MainMaster/Tax',
			title: 'Tax'
		},
		{
			icon: 'accessibility',
			route: '/MainMaster/FeeType',
			title: 'FeeType'
		}
	];
	documentMasters: ROUTE[] = [
		{
			icon: 'accessibility',
			route: '/MainMaster/documentType',
			title: 'Document Type'
		},

		{
			icon: 'accessibility',
			route: '/MainMaster/CompanyDocument',
			title: 'CompanyDocument'
		},
		{
			icon: 'accessibility',
			route: '/MainMaster/DefectTemplate',
			title: 'DefectTemplate'
		},

		{
			icon: 'accessibility',
			route: '/MainMaster/DocumentAssignType',
			title: 'DocumentAssignType'
		}
	];
	reportMasters: ROUTE[] = [
		{
			icon: 'accessibility',
			route: '/MainMaster/ReportTemplate',
			title: 'ReportTemplate'
		},
		{
			icon: 'accessibility',
			route: '/MainMaster/ReportHs',
			title: 'ReportHs'
		}
	];
	employeeMasters: ROUTE[] = [
		{
			icon: 'accessibility',
			route: '/MainMaster/UnitRoleMaster',
			title: 'UserRole'
		},
		{
			icon: 'accessibility',
			route: '/MainMaster/Employee',
			title: 'Employee'
		},
		{
			icon: 'accessibility',
			route: '/MainMaster/EmployeeGrade',
			title: 'Employee Grade'
		},
		{
			icon: 'accessibility',
			route: '/MainMaster/EmployeeCarrier',
			title: 'Employee Carrier'
		},
		{
			icon: 'accessibility',
			route: '/MainMaster/EmployeeDependent',
			title: 'Employee Dependent'
		},
		{
			icon: 'accessibility',
			route: '/MainMaster/EmployeeEducation',
			title: 'Employee Education'
		},
		{
			icon: 'accessibility',
			route: '/MainMaster/EmployeeLeave',
			title: 'Employee Leave'
		},
		{
			icon: 'accessibility',
			route: '/MainMaster/EmployeeSalary',
			title: 'Employee Salary'
		}
	];
	step = 0;

	ngOnInit() {}
	public isAuthenticated() {
		return this.commonFunction.isUserAuthenticated();
	}
	setStep(index: number) {
		this.step = index;
	}

	/*	nextStep() {
		this.step++;
	}

	prevStep() {
		this.step--;
	}*/
}

interface ROUTE {
	icon?: string;
	route?: string;
	title?: string;
}
