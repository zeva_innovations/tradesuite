import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SidenavbarmastersComponent } from './sidenavbarmasters.component';

describe('SidenavbarmastersComponent', () => {
  let component: SidenavbarmastersComponent;
  let fixture: ComponentFixture<SidenavbarmastersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SidenavbarmastersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SidenavbarmastersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
