import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SetupPerRateComponent } from './setup-per-rate.component';

describe('SetupPerRateComponent', () => {
  let component: SetupPerRateComponent;
  let fixture: ComponentFixture<SetupPerRateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SetupPerRateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SetupPerRateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
