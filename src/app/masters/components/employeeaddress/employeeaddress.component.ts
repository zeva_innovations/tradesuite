import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {ApiHandlerService} from '../../../services/api-handler.service';
import {DataSourceMasterService} from '../../../services/data-source-master.service';
import {CommonFunctionsService} from '../../../services/common-functions.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MatPaginator, MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';
import {environment} from '../../../../environments/environment';
import {MyErrorStateMatcher} from '../../../services/customer-details-form.service';
import {catchError} from 'rxjs/operators';
import {of} from 'rxjs';

@Component({
  selector: 'app-employeeaddress',
  templateUrl: './employeeaddress.component.html',
  styleUrls: ['./employeeaddress.component.scss']
})
export class EmployeeaddressComponent implements OnInit {

  constructor(
    private apiHandler: ApiHandlerService,
    private dataService: DataSourceMasterService,
    public commonFunction: CommonFunctionsService,
    private formBuilder: FormBuilder,
    private matSnackBar: MatSnackBar,
  ) {}
  displayedColumns = ['employee_Address_Master_ID', 'address_Type', 'address1', 'city', 'mobile_Number', 'actions'];
  EmployeeAddressMaster = environment.tradeAppRoot + environment.EmployeeAddressMaster;

  loggedUserName = this.commonFunction.parseApplicantInfoFromlocalStorage()[5];
  loggedUserId = this.commonFunction.parseApplicantInfoFromlocalStorage()[6];

  @Input() group: FormGroup;
  myjson: any = JSON;
  dataSource = new MatTableDataSource([]);
  editMode = false;
  masterId = 0;
  companyType: string[] = [];
  companyAreaType: string[] = [];
  industryType: string[] = [];
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  matchFinder = new MyErrorStateMatcher();
  form: FormGroup;

  ngOnInit() {
    this.dataService.loadApplicants(this.EmployeeAddressMaster);
    this.dataService.dataSubject.subscribe((val) => {
      this.dataSource = new MatTableDataSource(val);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      return val;
    });

    this.form = this.formBuilder.group({
      employeeAddressMasters: this.formBuilder.group({
        address_Type: new FormControl('', [Validators.required]),
        address1: new FormControl('', [Validators.required]),
        address2: new FormControl('', [Validators.required]),
        address3: new FormControl('', [Validators.required]),
        city: new FormControl('', [Validators.required]),
        state: new FormControl('', [Validators.required]),
        country: new FormControl('', [Validators.required]),
        city_STD_Code: new FormControl('', [Validators.required]),
        country_STD_code: new FormControl('', [Validators.required]),
        postal_Code: new FormControl('', [Validators.required]),
        mobile_Number: new FormControl('', [Validators.required]),
        land_Line_Number: new FormControl('', [Validators.required]),
        email_ID: new FormControl('', [Validators.required]),
        status: new FormControl('', [Validators.required]),

      })
    });
  }

  add(body: string) {
    body['created_By'] = this.loggedUserName + this.loggedUserId;
    this.dataService.postApplicants(this.EmployeeAddressMaster, body);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.reset();
  }
  edit(anyId: number) {
    this.masterId = anyId;
    const getMasterInfo = this.EmployeeAddressMaster + '/' + anyId;
    this.apiHandler.getApiRequest(getMasterInfo).pipe(catchError(() => of([]))).subscribe((employeeAddress: any) => {
      let employeeAddressObject: any;
      employeeAddressObject = { employeeAddressMasters: {} };
      employeeAddressObject.employeeAddressMasters['address_Type'] = employeeAddress.address_Type;
      employeeAddressObject.employeeAddressMasters['address1'] = employeeAddress.address1;
      employeeAddressObject.employeeAddressMasters['address2'] = employeeAddress.address2;
      employeeAddressObject.employeeAddressMasters['address3'] = employeeAddress.address3;
      employeeAddressObject.employeeAddressMasters['city'] = employeeAddress.city;
      employeeAddressObject.employeeAddressMasters['state'] = employeeAddress.state;
      employeeAddressObject.employeeAddressMasters['country'] = employeeAddress.country;
      employeeAddressObject.employeeAddressMasters['city_STD_Code'] = employeeAddress.city_STD_Code;
      employeeAddressObject.employeeAddressMasters['country_STD_code'] = employeeAddress.country_STD_code;
      employeeAddressObject.employeeAddressMasters['postal_Code'] = employeeAddress.postal_Code;
      employeeAddressObject.employeeAddressMasters['mobile_Number'] = employeeAddress.mobile_Number;
      employeeAddressObject.employeeAddressMasters['land_Line_Number'] = employeeAddress.land_Line_Number;
      employeeAddressObject.employeeAddressMasters['email_ID'] = employeeAddress.email_ID;
      employeeAddressObject.employeeAddressMasters['status'] = employeeAddress.status;

      this.form.setValue(employeeAddressObject);
      this.editMode = true;
    });
  }

  update(body: string) {
    body['employee_Address_Master_ID'] = this.masterId;
    body['edited_By'] = this.loggedUserName + this.loggedUserId;
    this.dataService.putApplicants(this.EmployeeAddressMaster, this.masterId, body);
    this.reset();
  }
  delete(applicantID: number) {
    this.dataService.deleteApplicants(this.EmployeeAddressMaster, applicantID);
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  reset() {
    this.form.reset();
  }
  openSnackBar(message: string, action: string) {
    this.matSnackBar.open('Saved Sucessfully', 'Close', {
      duration: 3000
    });
  }
  updateSnackBar(message: string, action: string) {
    this.matSnackBar.open('Updated Sucessfully', 'Close', {
      duration: 3000
    });
  }
  deleteSnackBar(message: string, action: string) {
    this.matSnackBar.open('Deleted Sucessfully', 'Close', {
      duration: 3000
    });
  }
}
