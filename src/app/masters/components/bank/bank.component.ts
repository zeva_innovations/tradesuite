import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {ApiHandlerService} from '../../../services/api-handler.service';
import {DataSourceMasterService} from '../../../services/data-source-master.service';
import {CommonFunctionsService} from '../../../services/common-functions.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {environment} from '../../../../environments/environment';
import {MatPaginator, MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';
import {MyErrorStateMatcher} from '../../../services/customer-details-form.service';
import {catchError} from 'rxjs/operators';
import {of} from 'rxjs';

@Component({
  selector: 'app-bank',
  templateUrl: './bank.component.html',
  styleUrls: ['./bank.component.scss']
})
export class BankComponent implements OnInit {

  constructor(
    private apiHandler: ApiHandlerService,
    private dataService: DataSourceMasterService,
    public commonFunction: CommonFunctionsService,
    private matSnackBar: MatSnackBar,
    private formBuilder: FormBuilder
  ) {}
  displayedColumns = ['bankmasterId', 'bank_Code', 'bankName', 'bankAccountNo', 'bankAddress1', 'manager', 'pincode', 'emailId', 'website', 'actions'];
  bankMaster = environment.tradeAppRoot + environment.bankMaster;

  loggedUserName = this.commonFunction.parseApplicantInfoFromlocalStorage()[5];
  loggedUserId = this.commonFunction.parseApplicantInfoFromlocalStorage()[6];

  @Input() group: FormGroup;
  myjson: any = JSON;
  dataSource = new MatTableDataSource([]);
  editMode = false;
  masterId = 0;
  companyType: string[] = [];
  companyAreaType: string[] = [];
  industryType: string[] = [];
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  matchFinder = new MyErrorStateMatcher();
  form: FormGroup;

  ngOnInit() {
    this.dataService.loadApplicants(this.bankMaster);
    this.dataService.dataSubject.subscribe((val) => {
      this.dataSource = new MatTableDataSource(val);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      return val;
    });

    this.form = this.formBuilder.group({
      bankMasters: this.formBuilder.group({

        bankName: new FormControl(null, [Validators.required]),
        bankAddress1: new FormControl(null, [Validators.required]),
        bankAddress2: new FormControl(null),
        bankAddress3: new FormControl(null),
        bankAccountNo:new FormControl(null, [Validators.required]),
        manager: new FormControl(null, [Validators.required]),
        city: new FormControl(null, [Validators.required]),
        state: new FormControl(null, [Validators.required]),
        pincode: new FormControl(null, [Validators.required]),
      
        faxno: new FormControl(null, [Validators.required]),
        emailId: new FormControl(null, [Validators.required]),
        website: new FormControl(null, [Validators.required]),
        status: new FormControl(null, [Validators.required]),
      })
    });
  }

  add(body: string) {
    body['created_By'] = this.loggedUserName + this.loggedUserId;
    this.dataService.postApplicants(this.bankMaster, body);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.reset(); this.editMode = false;
  }
  edit(anyId: number) {
    this.masterId = anyId;
    const getMasterInfo = this.bankMaster + '/' + anyId;
    this.apiHandler.getApiRequest(getMasterInfo).pipe(catchError(() => of([]))).subscribe((bankType: any) => {
      let bankObject: any;
      bankObject = { bankMasters: {} };
      bankObject.bankMasters['bankName'] = bankType.bankName;
      bankObject.bankMasters['bankAccountNo'] = bankType.bankAccountNo;
      bankObject.bankMasters['bankAddress1'] = bankType.bankAddress1;
      bankObject.bankMasters['bankAddress2'] = bankType.bankAddress2;
      bankObject.bankMasters['bankAddress3'] = bankType.bankAddress3;
      bankObject.bankMasters['manager'] = bankType.manager;
      bankObject.bankMasters['city'] = bankType.city;
      bankObject.bankMasters['state'] = bankType.state;
      bankObject.bankMasters['pincode'] = bankType.pincode;
  
      bankObject.bankMasters['faxno'] = bankType.faxno;
      bankObject.bankMasters['emailId'] = bankType.emailId;
      bankObject.bankMasters['website'] = bankType.website;
      bankObject.bankMasters['status'] = bankType.status;

      this.form.setValue(bankObject);
      this.editMode = true;
    });
  }

  update(body: string) {
    body['bankmasterId'] = this.masterId;
    body['edited_By'] = this.loggedUserName + this.loggedUserId;
    this.dataService.putApplicants(this.bankMaster, this.masterId, body);
    this.reset(); this.editMode = false;
  }
  delete(applicantID: number) {
    this.dataService.deleteApplicants(this.bankMaster, applicantID);
  }
  openSnackBar(message: string, action: string) {
    this.matSnackBar.open('Saved Sucessfully', 'Close', {
      duration: 3000
    });
  }
  updateSnackBar(message: string, action: string) {
    this.matSnackBar.open('Updated Sucessfully', 'Close', {
      duration: 3000
    });
  }
  deleteSnackBar(message: string, action: string) {
    this.matSnackBar.open('Deleted Sucessfully', 'Close', {
      duration: 3000
    });
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  reset() {
    this.form.reset();
  }
}
