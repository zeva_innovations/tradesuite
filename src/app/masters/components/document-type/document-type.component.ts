import { CommonFunctionsService } from 'src/app/services/common-functions.service';
import { Component, OnInit, Input, ViewChild, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { MyErrorStateMatcher } from 'src/app/services/customer-details-form.service';
import { FormGroup, FormBuilder, FormControl, Validators, ValidationErrors } from '@angular/forms';
import { DataSourceServiceService } from 'src/app/services/data-source-service.service';
import {MatTableDataSource, MatSort, MatPaginator, MatSnackBar} from '@angular/material';
import { environment } from 'src/environments/environment';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { ApiHandlerService } from 'src/app/services/api-handler.service';
import { EmitterHandlerService } from 'src/app/services/emitter-handler.service';
import { DataSourceMasterService } from 'src/app/services/data-source-master.service';

@Component({
	selector: 'app-document-type',
	templateUrl: './document-type.component.html',
	styleUrls: ['./document-type.component.scss']
})
export class DocumentTypeComponent implements OnInit {

	constructor(
		private apiHandler: ApiHandlerService,
		private dataService: DataSourceMasterService,
		public commonFunction: CommonFunctionsService,
		private formBuilder: FormBuilder,
    private matSnackBar: MatSnackBar,
	) {}
	displayedColumns = ['doc_Type_ID', 'docCode', 'docDescription', 'actions'];
	documentTypeMaster = environment.tradeAppRoot + environment.documentTypeMaster;

	loggedUserName = this.commonFunction.parseApplicantInfoFromlocalStorage()[5];
	loggedUserId = this.commonFunction.parseApplicantInfoFromlocalStorage()[6];

	@Input() group: FormGroup;
	myjson: any = JSON;
	dataSource = new MatTableDataSource([]);
	editMode = false;
	masterId = 0;
	companyType: string[] = [];
	companyAreaType: string[] = [];
	industryType: string[] = [];
	@ViewChild(MatSort) sort: MatSort;
	@ViewChild(MatPaginator) paginator: MatPaginator;
	matchFinder = new MyErrorStateMatcher();
	form: FormGroup;

	ngOnInit() {
		this.dataService.loadApplicants(this.documentTypeMaster);
		this.dataService.dataSubject.subscribe((val) => {
			this.dataSource = new MatTableDataSource(val);
			this.dataSource.sort = this.sort;
			this.dataSource.paginator = this.paginator;
			return val;
		});

		this.form = this.formBuilder.group({
			documentTypeMasters: this.formBuilder.group({
				docDescription: new FormControl('', [Validators.required]),
        status: new FormControl('', [Validators.required])
			})
		});
	}

	add(body: string) {
		body['created_By'] = this.loggedUserName + this.loggedUserId;
		this.dataService.postApplicants(this.documentTypeMaster, body);
		this.dataSource.sort = this.sort;
		this.dataSource.paginator = this.paginator;
		this.reset(); this.editMode = false;
	}
	edit(anyId: number) {
		this.masterId = anyId;
		const getMasterInfo = this.documentTypeMaster + '/' + anyId;
		this.apiHandler.getApiRequest(getMasterInfo).pipe(catchError(() => of([]))).subscribe((documentType: any) => {
			let documentTypeObject: any;
			documentTypeObject = { documentTypeMasters: {} };
			documentTypeObject.documentTypeMasters['docDescription'] = documentType.docDescription;
      documentTypeObject.documentTypeMasters['status'] = documentType.status;
			this.form.setValue(documentTypeObject);
			this.editMode = true;
		});
	}

	update(body: string) {
		body['doc_Type_ID'] = this.masterId;
		body['edited_By'] = this.loggedUserName + this.loggedUserId;
		this.dataService.putApplicants(this.documentTypeMaster, this.masterId, body);
		this.reset(); this.editMode = false;
	}
	delete(applicantID: number) {
		this.dataService.deleteApplicants(this.documentTypeMaster, applicantID);
	}

	applyFilter(filterValue: string) {
		this.dataSource.filter = filterValue.trim().toLowerCase();
	}
	reset() {
		this.form.reset();
	}
  openSnackBar(message: string, action: string) {
    this.matSnackBar.open('Saved Sucessfully', 'Close', {
      duration: 3000
    });
  }
  updateSnackBar(message: string, action: string) {
    this.matSnackBar.open('Updated Sucessfully', 'Close', {
      duration: 3000
    });
  }
  deleteSnackBar(message: string, action: string) {
    this.matSnackBar.open('Deleted Sucessfully', 'Close', {
      duration: 3000
    });
  }
}
