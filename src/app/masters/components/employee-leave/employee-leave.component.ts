import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {ApiHandlerService} from '../../../services/api-handler.service';
import {DataSourceMasterService} from '../../../services/data-source-master.service';
import {CommonFunctionsService} from '../../../services/common-functions.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MatPaginator, MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';
import {environment} from '../../../../environments/environment';
import {MyErrorStateMatcher} from '../../../services/customer-details-form.service';
import {catchError} from 'rxjs/operators';
import {of} from 'rxjs';

@Component({
  selector: 'app-employee-leave',
  templateUrl: './employee-leave.component.html',
  styleUrls: ['./employee-leave.component.scss']
})
export class EmployeeLeaveComponent implements OnInit {

  constructor(
    private apiHandler: ApiHandlerService,
    private dataService: DataSourceMasterService,
    public commonFunction: CommonFunctionsService,
    private formBuilder: FormBuilder,
    private matSnackBar: MatSnackBar,
  ) {}
  displayedColumns = ['employee_Leave_Master_ID', 'emp_ID', 'leave_code', 'no_Of_Days', 'actions'];
  EmployeeLeaveMaster = environment.tradeAppRoot + environment.EmployeeLeaveMaster;

  loggedUserName = this.commonFunction.parseApplicantInfoFromlocalStorage()[5];
  loggedUserId = this.commonFunction.parseApplicantInfoFromlocalStorage()[6];

  @Input() group: FormGroup;
  myjson: any = JSON;
  dataSource = new MatTableDataSource([]);
  editMode = false;
  masterId = 0;
  companyType: string[] = [];
  companyAreaType: string[] = [];
  industryType: string[] = [];
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  matchFinder = new MyErrorStateMatcher();
  form: FormGroup;

  ngOnInit() {
    this.dataService.loadApplicants(this.EmployeeLeaveMaster);
    this.dataService.dataSubject.subscribe((val) => {
      this.dataSource = new MatTableDataSource(val);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      return val;
    });

    this.form = this.formBuilder.group({
      employeeLeaveMasters: this.formBuilder.group({
        emp_ID: new FormControl('', [Validators.required]),
        leave_code: new FormControl('', [Validators.required]),
        no_Of_Days: new FormControl('', [Validators.required]),
        status: new FormControl('', [Validators.required])
      })
    });
  }

  add(body: string) {
    body['created_By'] = this.loggedUserName + this.loggedUserId;
    this.dataService.postApplicants(this.EmployeeLeaveMaster, body);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.reset();
  }
  edit(anyId: number) {
    this.masterId = anyId;
    const getMasterInfo = this.EmployeeLeaveMaster + '/' + anyId;
    this.apiHandler.getApiRequest(getMasterInfo).pipe(catchError(() => of([]))).subscribe((employeeLeave: any) => {
      let employeeLeaveObject: any;
      employeeLeaveObject = { employeeLeaveMasters: {} };
      employeeLeaveObject.employeeLeaveMasters['emp_ID'] = employeeLeave.emp_ID;
      employeeLeaveObject.employeeLeaveMasters['leave_code'] = employeeLeave.leave_code;
      employeeLeaveObject.employeeLeaveMasters['no_Of_Days'] = employeeLeave.no_Of_Days;
      employeeLeaveObject.employeeLeaveMasters['status'] = employeeLeave.status;


      this.form.setValue(employeeLeaveObject);
      this.editMode = true;
    });
  }

  update(body: string) {
    body['employee_Leave_Master_ID'] = this.masterId;
    body['edited_By'] = this.loggedUserName + this.loggedUserId;
    this.dataService.putApplicants(this.EmployeeLeaveMaster, this.masterId, body);
    this.reset();
  }
  delete(applicantID: number) {
    this.dataService.deleteApplicants(this.EmployeeLeaveMaster, applicantID);
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  reset() {
    this.form.reset();
  }
  openSnackBar(message: string, action: string) {
    this.matSnackBar.open('Saved Sucessfully', 'Close', {
      duration: 3000
    });
  }
  updateSnackBar(message: string, action: string) {
    this.matSnackBar.open('Updated Sucessfully', 'Close', {
      duration: 3000
    });
  }
  deleteSnackBar(message: string, action: string) {
    this.matSnackBar.open('Deleted Sucessfully', 'Close', {
      duration: 3000
    });
  }
}
