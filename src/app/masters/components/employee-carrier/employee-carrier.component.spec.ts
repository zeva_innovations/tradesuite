import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeCarrierComponent } from './employee-carrier.component';

describe('EmployeeCarrierComponent', () => {
  let component: EmployeeCarrierComponent;
  let fixture: ComponentFixture<EmployeeCarrierComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeCarrierComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeCarrierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
