import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ApiHandlerService } from '../../../services/api-handler.service';
import { DataSourceMasterService } from '../../../services/data-source-master.service';
import { CommonFunctionsService } from '../../../services/common-functions.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatPaginator, MatSnackBar, MatSort, MatTableDataSource } from '@angular/material';
import { environment } from '../../../../environments/environment';
import { MyErrorStateMatcher } from '../../../services/customer-details-form.service';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Component({
	selector: 'app-employee-carrier',
	templateUrl: './employee-carrier.component.html',
	styleUrls: ['./employee-carrier.component.scss']
})
export class EmployeeCarrierComponent implements OnInit {
	constructor(
		private apiHandler: ApiHandlerService,
		private dataService: DataSourceMasterService,
		public commonFunction: CommonFunctionsService,
		private formBuilder: FormBuilder,
		private matSnackBar: MatSnackBar
	) {}
	displayedColumns = ['employee_Carrier_Master_ID', 'emp_ID', 'buildup_Detail', 'description', 'pdfCopy', 'buildup_Type', 'actions'];
	EmployeeCarrier = environment.tradeAppRoot + environment.EmployeeCarrier;

	loggedUserName = this.commonFunction.parseApplicantInfoFromlocalStorage()[5];
	loggedUserId = this.commonFunction.parseApplicantInfoFromlocalStorage()[6];

	@Input() group: FormGroup;
	myjson: any = JSON;
	dataSource = new MatTableDataSource([]);
	editMode = false;
	masterId = 0;

	@ViewChild(MatSort) sort: MatSort;
	@ViewChild(MatPaginator) paginator: MatPaginator;
	matchFinder = new MyErrorStateMatcher();
	form: FormGroup;

	ngOnInit() {
		this.dataService.loadApplicants(this.EmployeeCarrier);
		this.dataService.dataSubject.subscribe((val) => {
			this.dataSource = new MatTableDataSource(val);
			this.dataSource.sort = this.sort;
			this.dataSource.paginator = this.paginator;
			return val;
		});

		this.form = this.formBuilder.group({
			employeeCarrierMasters: this.formBuilder.group({
				emp_ID: new FormControl('', [Validators.required]),
				buildup_Detail: new FormControl('', [Validators.required]),
				description: new FormControl('', [Validators.required]),
				pdfCopy: new FormControl('', [Validators.required]),
				buildup_Type: new FormControl('', [Validators.required]),
				status: new FormControl('', [Validators.required])
			})
		});
	}

	add(body: string) {
		body['created_By'] = this.loggedUserName + this.loggedUserId;
		this.dataService.postApplicants(this.EmployeeCarrier, body);
		this.dataSource.sort = this.sort;
		this.dataSource.paginator = this.paginator;
		this.reset();
	}
	edit(anyId: number) {
		this.masterId = anyId;
		const getMasterInfo = this.EmployeeCarrier + '/' + anyId;
		this.apiHandler.getApiRequest(getMasterInfo).pipe(catchError(() => of([]))).subscribe((employeeCarrier: any) => {
			let employeeCarrierObject: any;
			employeeCarrierObject = { employeeCarrierMasters: {} };
			employeeCarrierObject.employeeCarrierMasters['emp_ID'] = employeeCarrier.emp_ID;
			employeeCarrierObject.employeeCarrierMasters['buildup_Detail'] = employeeCarrier.buildup_Detail;
			employeeCarrierObject.employeeCarrierMasters['description'] = employeeCarrier.description;
			employeeCarrierObject.employeeCarrierMasters['pdfCopy'] = employeeCarrier.pdfCopy;
			employeeCarrierObject.employeeCarrierMasters['buildup_Type'] = employeeCarrier.buildup_Type;
			employeeCarrierObject.employeeCarrierMasters['status'] = employeeCarrier.status;

			this.form.setValue(employeeCarrierObject);
			this.editMode = true;
		});
	}

	update(body: string) {
		body['employee_Carrier_Master_ID'] = this.masterId;
		body['edited_By'] = this.loggedUserName + this.loggedUserId;
		this.dataService.putApplicants(this.EmployeeCarrier, this.masterId, body);
		this.reset();
	}
	delete(applicantID: number) {
		this.dataService.deleteApplicants(this.EmployeeCarrier, applicantID);
	}

	applyFilter(filterValue: string) {
		this.dataSource.filter = filterValue.trim().toLowerCase();
	}
	reset() {
		this.form.reset();
	}
	openSnackBar(message: string, action: string) {
		this.matSnackBar.open('Saved Sucessfully', 'Close', {
			duration: 3000
		});
	}
	updateSnackBar(message: string, action: string) {
		this.matSnackBar.open('Updated Sucessfully', 'Close', {
			duration: 3000
		});
	}
	deleteSnackBar(message: string, action: string) {
		this.matSnackBar.open('Deleted Sucessfully', 'Close', {
			duration: 3000
		});
	}
}
