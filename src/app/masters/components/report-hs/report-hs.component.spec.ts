import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportHsComponent } from './report-hs.component';

describe('ReportHsComponent', () => {
  let component: ReportHsComponent;
  let fixture: ComponentFixture<ReportHsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportHsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportHsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
