import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {ApiHandlerService} from '../../../services/api-handler.service';
import {DataSourceMasterService} from '../../../services/data-source-master.service';
import {CommonFunctionsService} from '../../../services/common-functions.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {environment} from '../../../../environments/environment';
import {MatPaginator, MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';
import {MyErrorStateMatcher} from '../../../services/customer-details-form.service';
import {catchError} from 'rxjs/operators';
import {of} from 'rxjs';

@Component({
  selector: 'app-unit-branch',
  templateUrl: './unit-branch.component.html',
  styleUrls: ['./unit-branch.component.scss']
})
export class UnitBranchComponent implements OnInit {

  constructor(
    private apiHandler: ApiHandlerService,
    private dataService: DataSourceMasterService,
    public commonFunction: CommonFunctionsService,
    private matSnackBar: MatSnackBar,
    private formBuilder: FormBuilder
  ) {}
  displayedColumns = ['id', 'description', 'remarks', 'image', 'actions'];
  unitBranchMaster = environment.tradeAppRoot + environment.unitBranchMaster;

  loggedUserName = this.commonFunction.parseApplicantInfoFromlocalStorage()[5];
  loggedUserId = this.commonFunction.parseApplicantInfoFromlocalStorage()[6];

  @Input() group: FormGroup;
  myjson: any = JSON;
  dataSource = new MatTableDataSource([]);
  editMode = false;
  masterId = 0;
  companyType: string[] = [];
  companyAreaType: string[] = [];
  industryType: string[] = [];
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  matchFinder = new MyErrorStateMatcher();
  form: FormGroup;

  ngOnInit() {
    this.dataService.loadApplicants(this.unitBranchMaster);
    this.dataService.dataSubject.subscribe((val) => {
      this.dataSource = new MatTableDataSource(val);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      return val;
    });

    this.form = this.formBuilder.group({
      unitbranchMasters: this.formBuilder.group({
        description: new FormControl('', [Validators.required]),
        remarks: new FormControl('', [Validators.required]),
        image: new FormControl('', [Validators.required]),
        status: new FormControl('', [Validators.required])
      })
    });
  }

  add(body: string) {
    body['created_By'] = this.loggedUserName + this.loggedUserId;
    this.dataService.postApplicants(this.unitBranchMaster, body);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.reset(); this.editMode = false;
  }
  edit(anyId: number) {
    this.masterId = anyId;
    const getMasterInfo = this.unitBranchMaster + '/' + anyId;
    this.apiHandler.getApiRequest(getMasterInfo).pipe(catchError(() => of([]))).subscribe((transportType: any) => {
      let transportObject: any;
      transportObject = { unitbranchMasters: {} };
      transportObject.unitbranchMasters['description'] = transportType.description;
      transportObject.unitbranchMasters['remarks'] = transportType.remarks;
      transportObject.unitbranchMasters['image'] = transportType.image;
      transportObject.unitbranchMasters['status'] = transportType.status;

      this.form.setValue(transportObject);
      this.editMode = true;
    });
  }

  update(body: string) {
    body['id'] = this.masterId;
    body['edited_By'] = this.loggedUserName + this.loggedUserId;
    this.dataService.putApplicants(this.unitBranchMaster, this.masterId, body);
    this.reset(); this.editMode = false;
  }
  delete(applicantID: number) {
    this.dataService.deleteApplicants(this.unitBranchMaster, applicantID);
  }
  openSnackBar(message: string, action: string) {
    this.matSnackBar.open('Saved Sucessfully', 'Close', {
      duration: 3000
    });
  }
  updateSnackBar(message: string, action: string) {
    this.matSnackBar.open('Updated Sucessfully', 'Close', {
      duration: 3000
    });
  }
  deleteSnackBar(message: string, action: string) {
    this.matSnackBar.open('Deleted Sucessfully', 'Close', {
      duration: 3000
    });
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  reset() {
    this.form.reset();
  }
}

