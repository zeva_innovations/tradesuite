import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnitBranchComponent } from './unit-branch.component';

describe('UnitBranchComponent', () => {
  let component: UnitBranchComponent;
  let fixture: ComponentFixture<UnitBranchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnitBranchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnitBranchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
