import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DepartmentReceiptTypeComponent } from './department-receipt-type.component';

describe('DepartmentReceiptTypeComponent', () => {
  let component: DepartmentReceiptTypeComponent;
  let fixture: ComponentFixture<DepartmentReceiptTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DepartmentReceiptTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DepartmentReceiptTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
