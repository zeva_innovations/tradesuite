import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HsHeadComponent } from './hs-head.component';

describe('HsHeadComponent', () => {
  let component: HsHeadComponent;
  let fixture: ComponentFixture<HsHeadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HsHeadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HsHeadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
