import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ApiHandlerService } from '../../../services/api-handler.service';
import { DataSourceMasterService } from '../../../services/data-source-master.service';
import { CommonFunctionsService } from '../../../services/common-functions.service';
import { MatPaginator, MatSnackBar, MatSort, MatTableDataSource } from '@angular/material';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { environment } from '../../../../environments/environment';
import { MyErrorStateMatcher } from '../../../services/customer-details-form.service';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Component({
	selector: 'app-hs-main-code',
	templateUrl: './hs-main-code.component.html',
	styleUrls: ['./hs-main-code.component.scss']
})
export class HsMainCodeComponent implements OnInit {
	constructor(
		private apiHandler: ApiHandlerService,
		private dataService: DataSourceMasterService,
		public commonFunction: CommonFunctionsService,
		private matSnackBar: MatSnackBar,
		private formBuilder: FormBuilder
	) {}
	displayedColumns = [
		'hsid',
		'hsmmCode',
		'hscmCode',
		'hshmCode',
		'hssmCode',
		'hCustomMCode',
		'customHeader',
		'receiptType',
		'currencyType',
		'remark',
		'actions'
	];
	HSMainMaster = environment.tradeAppRoot + environment.HSMainMaster;

	loggedUserName = this.commonFunction.parseApplicantInfoFromlocalStorage()[5];
	loggedUserId = this.commonFunction.parseApplicantInfoFromlocalStorage()[6];

	@Input() group: FormGroup;
	myjson: any = JSON;
	dataSource = new MatTableDataSource([]);
	editMode = false;
	masterId = 0;
	companyType: string[] = [];
	companyAreaType: string[] = [];
	industryType: string[] = [];
	@ViewChild(MatSort) sort: MatSort;
	@ViewChild(MatPaginator) paginator: MatPaginator;
	matchFinder = new MyErrorStateMatcher();
	form: FormGroup;

	ngOnInit() {
		this.dataService.loadApplicants(this.HSMainMaster);
		this.dataService.dataSubject.subscribe((val) => {
			this.dataSource = new MatTableDataSource(val);
			this.dataSource.sort = this.sort;
			this.dataSource.paginator = this.paginator;
			return val;
		});

		this.form = this.formBuilder.group({
			hsMainMasters: this.formBuilder.group({
				hsmmCode: new FormControl('', [Validators.required]),
				hscmCode: new FormControl('', [Validators.required]),
				hshmCode: new FormControl('', [Validators.required]),
				hssmCode: new FormControl('', [Validators.required]),
				hCustomMCode: new FormControl('', [Validators.required]),
				customHeader: new FormControl('', [Validators.required]),
				receiptType: new FormControl('', [Validators.required]),
				currencyType: new FormControl('', [Validators.required]),
				unit: new FormControl('', [Validators.required]),
				quantity: new FormControl('', [Validators.required]),
				iPrice: new FormControl('', [Validators.required]),
				ePrice: new FormControl('', [Validators.required]),
        xPrice: new FormControl('', [Validators.required]),
				countryOrgin: new FormControl('', [Validators.required]),
				countryCode: new FormControl('', [Validators.required]),
				status: new FormControl('', [Validators.required]),
				remark: new FormControl('', [Validators.required])
			})
		});
	}

	add(body: string) {
		body['created_By'] = this.loggedUserName + this.loggedUserId;
		this.dataService.postApplicants(this.HSMainMaster, body);
		this.dataSource.sort = this.sort;
		this.dataSource.paginator = this.paginator;
		this.reset();
		this.editMode = false;
	}
	edit(anyId: number) {
		this.masterId = anyId;
		const getMasterInfo = this.HSMainMaster + '/' + anyId;
		this.apiHandler.getApiRequest(getMasterInfo).pipe(catchError(() => of([]))).subscribe((hsMain: any) => {
			let hsMainObject: any;
			hsMainObject = { hsMainMasters: {} };
			hsMainObject.hsMainMasters['hsmmCode'] = hsMain.hsmmCode;
			hsMainObject.hsMainMasters['hscmCode'] = hsMain.hscmCode;
			hsMainObject.hsMainMasters['hshmCode'] = hsMain.hshmCode;
			hsMainObject.hsMainMasters['hssmCode'] = hsMain.hssmCode;
			hsMainObject.hsMainMasters['hCustomMCode'] = hsMain.hCustomMCode;
			hsMainObject.hsMainMasters['customHeader'] = hsMain.customHeader;
			hsMainObject.hsMainMasters['receiptType'] = hsMain.receiptType;
			hsMainObject.hsMainMasters['currencyType'] = hsMain.currencyType;
			hsMainObject.hsMainMasters['unit'] = hsMain.unit;
			hsMainObject.hsMainMasters['quantity'] = hsMain.quantity;
			hsMainObject.hsMainMasters['iPrice'] = hsMain.iPrice;
			hsMainObject.hsMainMasters['ePrice'] = hsMain.ePrice;
			hsMainObject.hsMainMasters['xPrice'] = hsMain.xPrice;
			hsMainObject.hsMainMasters['countryOrgin'] = hsMain.countryOrgin;
			hsMainObject.hsMainMasters['countryCode'] = hsMain.countryCode;
			hsMainObject.hsMainMasters['status'] = hsMain.status;
			hsMainObject.hsMainMasters['remark'] = hsMain.remark;
			this.form.setValue(hsMainObject);
			this.editMode = true;
		});
	}

	update(body: string) {
		body['hsid'] = this.masterId;
		body['edited_By'] = this.loggedUserName + this.loggedUserId;
		this.dataService.putApplicants(this.HSMainMaster, this.masterId, body);
		this.reset();
		this.editMode = false;
	}
	delete(applicantID: number) {
		this.dataService.deleteApplicants(this.HSMainMaster, applicantID);
	}

	applyFilter(filterValue: string) {
		this.dataSource.filter = filterValue.trim().toLowerCase();
	}
	reset() {
		this.form.reset();
	}
	openSnackBar(message: string, action: string) {
		this.matSnackBar.open('Saved Sucessfully', 'Close', {
			duration: 3000
		});
	}
	updateSnackBar(message: string, action: string) {
		this.matSnackBar.open('Updated Sucessfully', 'Close', {
			duration: 3000
		});
	}
	deleteSnackBar(message: string, action: string) {
		this.matSnackBar.open('Deleted Sucessfully', 'Close', {
			duration: 3000
		});
	}
}
