import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HsMainCodeComponent } from './hs-main-code.component';

describe('HsMainCodeComponent', () => {
  let component: HsMainCodeComponent;
  let fixture: ComponentFixture<HsMainCodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HsMainCodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HsMainCodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
