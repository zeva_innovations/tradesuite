import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DefecttypeComponent } from './defecttype.component';

describe('DefecttypeComponent', () => {
  let component: DefecttypeComponent;
  let fixture: ComponentFixture<DefecttypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DefecttypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DefecttypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
