import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyAreaTypeComponent } from './company-area-type.component';

describe('CompanyAreaTypeComponent', () => {
  let component: CompanyAreaTypeComponent;
  let fixture: ComponentFixture<CompanyAreaTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyAreaTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyAreaTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
