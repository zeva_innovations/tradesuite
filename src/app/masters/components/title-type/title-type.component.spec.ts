import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TitleTypeComponent } from './title-type.component';

describe('TitleTypeComponent', () => {
  let component: TitleTypeComponent;
  let fixture: ComponentFixture<TitleTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TitleTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TitleTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
