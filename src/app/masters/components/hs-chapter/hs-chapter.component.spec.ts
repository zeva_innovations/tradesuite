import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HsChapterComponent } from './hs-chapter.component';

describe('HsChapterComponent', () => {
  let component: HsChapterComponent;
  let fixture: ComponentFixture<HsChapterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HsChapterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HsChapterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
