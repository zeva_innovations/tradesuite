import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ApiHandlerService } from '../../../services/api-handler.service';
import { DataSourceMasterService } from '../../../services/data-source-master.service';
import { CommonFunctionsService } from '../../../services/common-functions.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { environment } from '../../../../environments/environment';
import { MatPaginator, MatSnackBar, MatSort, MatTableDataSource } from '@angular/material';
import { MyErrorStateMatcher } from '../../../services/customer-details-form.service';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Component({
	selector: 'app-hs-chapter',
	templateUrl: './hs-chapter.component.html',
	styleUrls: ['./hs-chapter.component.scss']
})
export class HsChapterComponent implements OnInit {
	constructor(
		private apiHandler: ApiHandlerService,
		private dataService: DataSourceMasterService,
		public commonFunction: CommonFunctionsService,
		private matSnackBar: MatSnackBar,
		private formBuilder: FormBuilder
	) {}
	displayedColumns = ['hsc', 'hscCode', 'hsChapterName', 'remark', 'actions'];
	hsChapterMaster = environment.tradeAppRoot + environment.hsChapterMaster;

	loggedUserName = this.commonFunction.parseApplicantInfoFromlocalStorage()[5];
	loggedUserId = this.commonFunction.parseApplicantInfoFromlocalStorage()[6];

	@Input() group: FormGroup;
	myjson: any = JSON;
	dataSource = new MatTableDataSource([]);
	editMode = false;
	masterId = 0;
	companyType: string[] = [];
	companyAreaType: string[] = [];
	industryType: string[] = [];
	@ViewChild(MatSort) sort: MatSort;
	@ViewChild(MatPaginator) paginator: MatPaginator;
	matchFinder = new MyErrorStateMatcher();
	form: FormGroup;

	ngOnInit() {
		this.dataService.loadApplicants(this.hsChapterMaster);
		this.dataService.dataSubject.subscribe((val) => {
			this.dataSource = new MatTableDataSource(val);
			this.dataSource.sort = this.sort;
			this.dataSource.paginator = this.paginator;
			return val;
		});

		this.form = this.formBuilder.group({
			hschapterMasters: this.formBuilder.group({
				hscCode: new FormControl('', [Validators.required]),
				hsChapterName: new FormControl('', [Validators.required]),
				status: new FormControl('', [Validators.required]),
				remark: new FormControl('', [Validators.required])
			})
		});
	}

	add(body: string) {
		body['created_By'] = this.loggedUserName + this.loggedUserId;
		this.dataService.postApplicants(this.hsChapterMaster, body);
		this.dataSource.sort = this.sort;
		this.dataSource.paginator = this.paginator;
		this.reset();
		this.editMode = false;
	}
	edit(anyId: number) {
		this.masterId = anyId;
		const getMasterInfo = this.hsChapterMaster + '/' + anyId;
		this.apiHandler.getApiRequest(getMasterInfo).pipe(catchError(() => of([]))).subscribe((hschapter: any) => {
			let hsChapterObject: any;
			hsChapterObject = { hschapterMasters: {} };
			hsChapterObject.hschapterMasters['hscCode'] = hschapter.hscCode;
			hsChapterObject.hschapterMasters['hsChapterName'] = hschapter.hsChapterName;
			hsChapterObject.hschapterMasters['status'] = hschapter.status;
			hsChapterObject.hschapterMasters['remark'] = hschapter.remark;
			this.form.setValue(hsChapterObject);
			this.editMode = true;
		});
	}

	update(body: string) {
		body['hsc'] = this.masterId;
		body['edited_By'] = this.loggedUserName + this.loggedUserId;
		this.dataService.putApplicants(this.hsChapterMaster, this.masterId, body);
		this.reset();
		this.editMode = false;
	}
	delete(applicantID: number) {
		this.dataService.deleteApplicants(this.hsChapterMaster, applicantID);
	}

	applyFilter(filterValue: string) {
		this.dataSource.filter = filterValue.trim().toLowerCase();
	}
	reset() {
		this.form.reset();
	}
	openSnackBar(message: string, action: string) {
		this.matSnackBar.open('Saved Sucessfully', 'Close', {
			duration: 3000
		});
	}
	updateSnackBar(message: string, action: string) {
		this.matSnackBar.open('Updated Sucessfully', 'Close', {
			duration: 3000
		});
	}
	deleteSnackBar(message: string, action: string) {
		this.matSnackBar.open('Deleted Sucessfully', 'Close', {
			duration: 3000
		});
	}
}
