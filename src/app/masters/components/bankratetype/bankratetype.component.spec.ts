import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BankratetypeComponent } from './bankratetype.component';

describe('BankratetypeComponent', () => {
  let component: BankratetypeComponent;
  let fixture: ComponentFixture<BankratetypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BankratetypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BankratetypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
