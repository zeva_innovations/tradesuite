import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {ApiHandlerService} from '../../../services/api-handler.service';
import {DataSourceMasterService} from '../../../services/data-source-master.service';
import {CommonFunctionsService} from '../../../services/common-functions.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {environment} from '../../../../environments/environment';
import {MatPaginator, MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';
import {MyErrorStateMatcher} from '../../../services/customer-details-form.service';
import {catchError} from 'rxjs/operators';
import {of} from 'rxjs';

@Component({
  selector: 'app-bankratetype',
  templateUrl: './bankratetype.component.html',
  styleUrls: ['./bankratetype.component.scss']
})
export class BankratetypeComponent implements OnInit {

  constructor(
    private apiHandler: ApiHandlerService,
    private dataService: DataSourceMasterService,
    public commonFunction: CommonFunctionsService,
    private matSnackBar: MatSnackBar,
    private formBuilder: FormBuilder
  ) {}
  displayedColumns = ['bankRateID', 'bankRateMasterCode', 'bankCurrenyMaster', 'bankRateMaster', 'bankRatePerUnit', 'toBankCurreny', 'toBankRateMaster', 'toBankRatePerUnit', 'actions'];
  bankRateType = environment.tradeAppRoot + environment.bankRateType;

  loggedUserName = this.commonFunction.parseApplicantInfoFromlocalStorage()[5];
  loggedUserId = this.commonFunction.parseApplicantInfoFromlocalStorage()[6];

  @Input() group: FormGroup;
  myjson: any = JSON;
  dataSource = new MatTableDataSource([]);
  editMode = false;
  masterId = 0;
  companyType: string[] = [];
  companyAreaType: string[] = [];
  industryType: string[] = [];
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  matchFinder = new MyErrorStateMatcher();
  form: FormGroup;

  ngOnInit() {
    this.dataService.loadApplicants(this.bankRateType);
    this.dataService.dataSubject.subscribe((val) => {
      this.dataSource = new MatTableDataSource(val);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      return val;
    });

    this.form = this.formBuilder.group({
      bankrateMasters: this.formBuilder.group({

        bankCurrenyMaster: new FormControl(null, [Validators.required]),
        bankRateMaster: new FormControl(null, [Validators.required]),
        bankRatePerUnit: new FormControl(null, [Validators.required]),
        toBankCurreny: new FormControl(null, [Validators.required]),
        toBankRateMaster: new FormControl(null, [Validators.required]),
        toBankRatePerUnit: new FormControl(null, [Validators.required]),
        status: new FormControl(null, [Validators.required]),
        remark: new FormControl(null, [Validators.required]),

      })
    });
  }

  add(body: string) {
    body['created_By'] = this.loggedUserName + this.loggedUserId;
    this.dataService.postApplicants(this.bankRateType, body);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.reset(); this.editMode = false;
  }
  edit(anyId: number) {
    this.masterId = anyId;
    const getMasterInfo = this.bankRateType + '/' + anyId;
    this.apiHandler.getApiRequest(getMasterInfo).pipe(catchError(() => of([]))).subscribe((bankrateType: any) => {
      let bankrateObject: any;
      bankrateObject = { bankrateMasters: {} };
      bankrateObject.bankrateMasters['bankCurrenyMaster'] = bankrateType.bankCurrenyMaster;
      bankrateObject.bankrateMasters['bankRateMaster'] = bankrateType.bankRateMaster;
      bankrateObject.bankrateMasters['bankRatePerUnit'] = bankrateType.bankRatePerUnit;
      bankrateObject.bankrateMasters['toBankCurreny'] = bankrateType.toBankCurreny;
      bankrateObject.bankrateMasters['toBankRateMaster'] = bankrateType.toBankRateMaster;
      bankrateObject.bankrateMasters['toBankRatePerUnit'] = bankrateType.toBankRatePerUnit;
      bankrateObject.bankrateMasters['status'] = bankrateType.status;
      bankrateObject.bankrateMasters['remark'] = bankrateType.remark;


      this.form.setValue(bankrateObject);
      this.editMode = true;
    });
  }

  update(body: string) {
    body['bankRateID'] = this.masterId;
    body['edited_By'] = this.loggedUserName + this.loggedUserId;
    this.dataService.putApplicants(this.bankRateType, this.masterId, body);
    this.reset(); this.editMode = false;
  }
  delete(applicantID: number) {
    this.dataService.deleteApplicants(this.bankRateType, applicantID);
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  openSnackBar(message: string, action: string) {
    this.matSnackBar.open('Saved Sucessfully', 'Close', {
      duration: 3000
    });
  }
  updateSnackBar(message: string, action: string) {
    this.matSnackBar.open('Updated Sucessfully', 'Close', {
      duration: 3000
    });
  }
  deleteSnackBar(message: string, action: string) {
    this.matSnackBar.open('Deleted Sucessfully', 'Close', {
      duration: 3000
    });
  }
  reset() {
    this.form.reset();
  }
}
