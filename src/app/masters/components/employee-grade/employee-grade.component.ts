import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {ApiHandlerService} from '../../../services/api-handler.service';
import {DataSourceMasterService} from '../../../services/data-source-master.service';
import {CommonFunctionsService} from '../../../services/common-functions.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MatPaginator, MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';
import {environment} from '../../../../environments/environment';
import {MyErrorStateMatcher} from '../../../services/customer-details-form.service';
import {catchError} from 'rxjs/operators';
import {of} from 'rxjs';

@Component({
  selector: 'app-employee-grade',
  templateUrl: './employee-grade.component.html',
  styleUrls: ['./employee-grade.component.scss']
})
export class EmployeeGradeComponent implements OnInit {

  constructor(
    private apiHandler: ApiHandlerService,
    private dataService: DataSourceMasterService,
    public commonFunction: CommonFunctionsService,
    private formBuilder: FormBuilder,
    private matSnackBar: MatSnackBar,
  ) {}
  displayedColumns = ['grade_ID', 'grade_Code', 'grade_Description', 'grade_Level', 'designation_ID', 'staff_Category', 'actions'];
  EmployeeGradeMaster = environment.tradeAppRoot + environment.EmployeeGradeMaster;

  loggedUserName = this.commonFunction.parseApplicantInfoFromlocalStorage()[5];
  loggedUserId = this.commonFunction.parseApplicantInfoFromlocalStorage()[6];

  @Input() group: FormGroup;
  myjson: any = JSON;
  dataSource = new MatTableDataSource([]);
  editMode = false;
  masterId = 0;
  companyType: string[] = [];
  companyAreaType: string[] = [];
  industryType: string[] = [];
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  matchFinder = new MyErrorStateMatcher();
  form: FormGroup;

  ngOnInit() {
    this.dataService.loadApplicants(this.EmployeeGradeMaster);
    this.dataService.dataSubject.subscribe((val) => {
      this.dataSource = new MatTableDataSource(val);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      return val;
    });

    this.form = this.formBuilder.group({
      employeeGradeMasters: this.formBuilder.group({
        grade_Description: new FormControl('', [Validators.required]),
        grade_Level: new FormControl('', [Validators.required]),
        designation_ID: new FormControl('', [Validators.required]),
        staff_Category: new FormControl('', [Validators.required]),
        status: new FormControl('', [Validators.required])
      })
    });
  }

  add(body: string) {
    body['created_By'] = this.loggedUserName + this.loggedUserId;
    this.dataService.postApplicants(this.EmployeeGradeMaster, body);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.reset();
  }
  edit(anyId: number) {
    this.masterId = anyId;
    const getMasterInfo = this.EmployeeGradeMaster + '/' + anyId;
    this.apiHandler.getApiRequest(getMasterInfo).pipe(catchError(() => of([]))).subscribe((employeeGrade: any) => {
      let employeeGradeObject: any;
      employeeGradeObject = { employeeGradeMasters: {} };
      employeeGradeObject.employeeGradeMasters['grade_Description'] = employeeGrade.grade_Description;
      employeeGradeObject.employeeGradeMasters['grade_Level'] = employeeGrade.grade_Level;
      employeeGradeObject.employeeGradeMasters['designation_ID'] = employeeGrade.designation_ID;
      employeeGradeObject.employeeGradeMasters['staff_Category'] = employeeGrade.staff_Category;
      employeeGradeObject.employeeGradeMasters['status'] = employeeGrade.status;

      this.form.setValue(employeeGradeObject);
      this.editMode = true;
    });
  }

  update(body: string) {
    body['grade_ID'] = this.masterId;
    body['edited_By'] = this.loggedUserName + this.loggedUserId;
    this.dataService.putApplicants(this.EmployeeGradeMaster, this.masterId, body);
    this.reset();
  }
  delete(applicantID: number) {
    this.dataService.deleteApplicants(this.EmployeeGradeMaster, applicantID);
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  reset() {
    this.form.reset();
  }
  openSnackBar(message: string, action: string) {
    this.matSnackBar.open('Saved Sucessfully', 'Close', {
      duration: 3000
    });
  }
  updateSnackBar(message: string, action: string) {
    this.matSnackBar.open('Updated Sucessfully', 'Close', {
      duration: 3000
    });
  }
  deleteSnackBar(message: string, action: string) {
    this.matSnackBar.open('Deleted Sucessfully', 'Close', {
      duration: 3000
    });
  }
}
