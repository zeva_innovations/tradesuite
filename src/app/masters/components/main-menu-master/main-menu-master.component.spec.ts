import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainMenuMasterComponent } from './main-menu-master.component';

describe('MainMenuMasterComponent', () => {
  let component: MainMenuMasterComponent;
  let fixture: ComponentFixture<MainMenuMasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainMenuMasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainMenuMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
