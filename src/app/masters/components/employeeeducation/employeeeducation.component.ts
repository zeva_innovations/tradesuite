import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ApiHandlerService } from '../../../services/api-handler.service';
import { DataSourceMasterService } from '../../../services/data-source-master.service';
import { CommonFunctionsService } from '../../../services/common-functions.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatPaginator, MatSnackBar, MatSort, MatTableDataSource } from '@angular/material';
import { environment } from '../../../../environments/environment';
import { MyErrorStateMatcher } from '../../../services/customer-details-form.service';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Component({
	selector: 'app-employeeeducation',
	templateUrl: './employeeeducation.component.html',
	styleUrls: ['./employeeeducation.component.scss']
})
export class EmployeeeducationComponent implements OnInit {
	constructor(
		private apiHandler: ApiHandlerService,
		private dataService: DataSourceMasterService,
		public commonFunction: CommonFunctionsService,
		private formBuilder: FormBuilder,
		private matSnackBar: MatSnackBar
	) {}
	displayedColumns = [
		'employee_Education_Master_ID',
		'emp_ID',
		'education_Type',
		'name_of_Institution',
		'name_of_Board_University',
		'fromYear',
		'toYear',
		'description',
		'pdfCopy',
		'actions'
	];
	EmployeeEducation = environment.tradeAppRoot + environment.EmployeeEducation;

	loggedUserName = this.commonFunction.parseApplicantInfoFromlocalStorage()[5];
	loggedUserId = this.commonFunction.parseApplicantInfoFromlocalStorage()[6];

	@Input() group: FormGroup;
	myjson: any = JSON;
	dataSource = new MatTableDataSource([]);
	editMode = false;
	masterId = 0;
	companyType: string[] = [];
	companyAreaType: string[] = [];
	industryType: string[] = [];
	@ViewChild(MatSort) sort: MatSort;
	@ViewChild(MatPaginator) paginator: MatPaginator;
	matchFinder = new MyErrorStateMatcher();
	form: FormGroup;

	ngOnInit() {
		this.dataService.loadApplicants(this.EmployeeEducation);
		this.dataService.dataSubject.subscribe((val) => {
			this.dataSource = new MatTableDataSource(val);
			this.dataSource.sort = this.sort;
			this.dataSource.paginator = this.paginator;
			return val;
		});

		this.form = this.formBuilder.group({
			employeeEducationMasters: this.formBuilder.group({
				emp_ID: new FormControl('', [Validators.required]),
				education_Type: new FormControl('', [Validators.required]),
				name_of_Institution: new FormControl('', [Validators.required]),
				name_of_Board_University: new FormControl('', [Validators.required]),
				fromYear: new FormControl({ value: '', disabled: false }),
				toYear: new FormControl({ value: '', disabled: false }),
				description: new FormControl('', [Validators.required]),
				pdfCopy: new FormControl('', [Validators.required]),
				status: new FormControl('', [Validators.required])
			})
		});
	}

	add(body: string) {
		body['created_By'] = this.loggedUserName + this.loggedUserId;
		this.dataService.postApplicants(this.EmployeeEducation, body);
		this.dataSource.sort = this.sort;
		this.dataSource.paginator = this.paginator;
		this.reset();
	}
	edit(anyId: number) {
		this.masterId = anyId;
		const getMasterInfo = this.EmployeeEducation + '/' + anyId;
		this.apiHandler.getApiRequest(getMasterInfo).pipe(catchError(() => of([]))).subscribe((employeeAddress: any) => {
			let employeeAddressObject: any;
			employeeAddressObject = { employeeEducationMasters: {} };
			employeeAddressObject.employeeEducationMasters['emp_ID'] = employeeAddress.emp_ID;
			employeeAddressObject.employeeEducationMasters['education_Type'] = employeeAddress.education_Type;
			employeeAddressObject.employeeEducationMasters['name_of_Institution'] = employeeAddress.name_of_Institution;
			employeeAddressObject.employeeEducationMasters['name_of_Board_University'] = employeeAddress.name_of_Board_University;
			employeeAddressObject.employeeEducationMasters['fromYear'] = employeeAddress.fromYear;
			employeeAddressObject.employeeEducationMasters['toYear'] = employeeAddress.toYear;
			employeeAddressObject.employeeEducationMasters['description'] = employeeAddress.description;
			employeeAddressObject.employeeEducationMasters['pdfCopy'] = employeeAddress.pdfCopy;
			employeeAddressObject.employeeEducationMasters['status'] = employeeAddress.status;
					this.form.setValue(employeeAddressObject);
			this.editMode = true;
		});
	}

	update(body: string) {
		body['employee_Education_Master_ID'] = this.masterId;
		body['edited_By'] = this.loggedUserName + this.loggedUserId;
		this.dataService.putApplicants(this.EmployeeEducation, this.masterId, body);
		this.reset();
	}
	delete(applicantID: number) {
		this.dataService.deleteApplicants(this.EmployeeEducation, applicantID);
	}

	applyFilter(filterValue: string) {
		this.dataSource.filter = filterValue.trim().toLowerCase();
	}
	reset() {
		this.form.reset();
	}
	openSnackBar(message: string, action: string) {
		this.matSnackBar.open('Saved Sucessfully', 'Close', {
			duration: 3000
		});
	}
	updateSnackBar(message: string, action: string) {
		this.matSnackBar.open('Updated Sucessfully', 'Close', {
			duration: 3000
		});
	}
	deleteSnackBar(message: string, action: string) {
		this.matSnackBar.open('Deleted Sucessfully', 'Close', {
			duration: 3000
		});
	}
	checkDates(control: FormControl) {
		if (control.value === '' || control.value === null) {
			return { dobNull: true };
		}
		return null;
	}
}
