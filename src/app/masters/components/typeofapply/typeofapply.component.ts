import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {ApiHandlerService} from '../../../services/api-handler.service';
import {DataSourceMasterService} from '../../../services/data-source-master.service';
import {CommonFunctionsService} from '../../../services/common-functions.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {environment} from '../../../../environments/environment';
import {MatPaginator, MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';
import {MyErrorStateMatcher} from '../../../services/customer-details-form.service';
import {catchError} from 'rxjs/operators';
import {of} from 'rxjs';

@Component({
  selector: 'app-typeofapply',
  templateUrl: './typeofapply.component.html',
  styleUrls: ['./typeofapply.component.scss']
})
export class TypeofapplyComponent implements OnInit {

  constructor(
    private apiHandler: ApiHandlerService,
    private dataService: DataSourceMasterService,
    public commonFunction: CommonFunctionsService,
    private matSnackBar: MatSnackBar,
    private formBuilder: FormBuilder
  ) {}
  displayedColumns = ['typeOfApplyID', 'typeCode', 'feeforInovice', 'remark', 'actions'];
  applyTypeMaster = environment.tradeAppRoot + environment.applyTypeMaster;
  yearmaster = environment.tradeAppRoot + environment.yearmaster;
  companyTypeMaster = environment.tradeAppRoot + environment.companyTypeMaster;
  receiptTypeApi = environment.tradeAppRoot + environment.receiptTypeMaster;
  companyAreaMaster = environment.tradeAppRoot + environment.companyAreaTypeMaster;

  loggedUserName = this.commonFunction.parseApplicantInfoFromlocalStorage()[5];
  loggedUserId = this.commonFunction.parseApplicantInfoFromlocalStorage()[6];

  @Input() group: FormGroup;
  myjson: any = JSON;
  dataSource = new MatTableDataSource([]);
  editMode = false;
  masterId = 0;
  companyType: string[] = [];
  industryType: string[] = [];
  receiptType: string[] = [];
  companyAreaType: string[] = [];
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  matchFinder = new MyErrorStateMatcher();
  form: FormGroup;
  yearMaster: string[] = [];

  ngOnInit() {
    this.dataService.loadApplicants(this.applyTypeMaster);
    this.dataService.dataSubject.subscribe((val) => {
      this.dataSource = new MatTableDataSource(val);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      return val;
    });

    this.form = this.formBuilder.group({
      ApplyTypeMasters: this.formBuilder.group({
        applyType : new FormControl('', [Validators.required]),
        year : new FormControl('', [Validators.required]),
        companyType : new FormControl('', [Validators.required]),
        receiptType: new FormControl('', [Validators.required]),
        companyAreaType : new FormControl('', [Validators.required]),
        feeforInovice : new FormControl('', [Validators.required]),
        remark : new FormControl('', [Validators.required]),
        status: new FormControl('', [Validators.required])

      })
    });
    this.getYear();
    this.getReceipt();
    this.getCompany();
    this.getCompanyArea();
  }
  getYear() {
    this.apiHandler.getApiRequest(this.yearmaster).subscribe((yearApi) => {
      this.yearMaster = yearApi;

    });
  }
  getCompany() {
    this.apiHandler.getApiRequest(this.companyTypeMaster).subscribe((companyTypeMaster) => {
      this.companyType = companyTypeMaster;
    });
  }
  getReceipt() {
    this.apiHandler.getApiRequest(this.receiptTypeApi).subscribe((receiptTypeApi) => {
      this.receiptType = receiptTypeApi;
    });
  }
  getCompanyArea() {
    this.apiHandler.getApiRequest(this.companyAreaMaster).subscribe((companyAreaMaster) => {
      this.companyAreaType = companyAreaMaster;
    });
  }
  add(body: string) {
    body['created_By'] = this.loggedUserName + this.loggedUserId;
    this.dataService.postApplicants(this.applyTypeMaster, body);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.reset(); this.editMode = false;
  }
  edit(anyId: number) {
    this.masterId = anyId;
    const getMasterInfo = this.applyTypeMaster + '/' + anyId;
    this.apiHandler.getApiRequest(getMasterInfo).pipe(catchError(() => of([]))).subscribe((applyType: any) => {
      let applyTypeObject: any;
      applyTypeObject = { ApplyTypeMasters: {} };
      applyTypeObject.ApplyTypeMasters['applyType'] = applyType.applyType;
      applyTypeObject.ApplyTypeMasters['year'] = applyType.year;
      applyTypeObject.ApplyTypeMasters['companyType'] = applyType.companyType;
      applyTypeObject.ApplyTypeMasters['receiptType'] = applyType.receiptType;
      applyTypeObject.ApplyTypeMasters['companyAreaType'] = applyType.companyAreaType;
      applyTypeObject.ApplyTypeMasters['feeforInovice'] = applyType.feeforInovice;
      applyTypeObject.ApplyTypeMasters['remark'] = applyType.remark;
      applyTypeObject.ApplyTypeMasters['status'] = applyType.status;



      this.form.setValue(applyTypeObject);
      this.editMode = true;
    });
  }

  update(body: string) {
    body['typeOfApplyID'] = this.masterId;
    body['edited_By'] = this.loggedUserName + this.loggedUserId;
    this.dataService.putApplicants(this.applyTypeMaster, this.masterId, body);
    this.reset(); this.editMode = false;
  }
  delete(applicantID: number) {
    this.dataService.deleteApplicants(this.applyTypeMaster, applicantID);
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  openSnackBar(message: string, action: string) {
    this.matSnackBar.open('Saved Sucessfully', 'Close', {
      duration: 3000
    });
  }
  updateSnackBar(message: string, action: string) {
    this.matSnackBar.open('Updated Sucessfully', 'Close', {
      duration: 3000
    });
  }
  deleteSnackBar(message: string, action: string) {
    this.matSnackBar.open('Deleted Sucessfully', 'Close', {
      duration: 3000
    });
  }
  reset() {
    this.form.reset();
  }
}

