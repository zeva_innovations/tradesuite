import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TypeofapplyComponent } from './typeofapply.component';

describe('TypeofapplyComponent', () => {
  let component: TypeofapplyComponent;
  let fixture: ComponentFixture<TypeofapplyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TypeofapplyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TypeofapplyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
