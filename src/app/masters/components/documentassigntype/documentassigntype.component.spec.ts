import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentassigntypeComponent } from './documentassigntype.component';

describe('DocumentassigntypeComponent', () => {
  let component: DocumentassigntypeComponent;
  let fixture: ComponentFixture<DocumentassigntypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocumentassigntypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentassigntypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
