import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ApiHandlerService } from '../../../services/api-handler.service';
import { DataSourceMasterService } from '../../../services/data-source-master.service';
import { CommonFunctionsService } from '../../../services/common-functions.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatPaginator, MatSnackBar, MatSort, MatTableDataSource } from '@angular/material';
import { environment } from '../../../../environments/environment';
import { MyErrorStateMatcher } from '../../../services/customer-details-form.service';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Component({
	selector: 'app-documentassigntype',
	templateUrl: './documentassigntype.component.html',
	styleUrls: ['./documentassigntype.component.scss']
})
export class DocumentassigntypeComponent implements OnInit {
	/* constructor(
    private apiHandler: ApiHandlerService,
    private dataService: DataSourceMasterService,
    public commonFunction: CommonFunctionsService,
    private formBuilder: FormBuilder,
    private matSnackBar: MatSnackBar,
  ) {}
  displayedColumns = ['docForStageID', 'receiptNameCode', 'docNameCode', 'actions'];
  documentTypeMaster = environment.tradeAppRoot + environment.documentTypeMaster;
  applyTypeMaster = environment.tradeAppRoot + environment.applyTypeMaster;
  documentAssignMaster= environment.tradeAppRoot + environment.documentAssignMaster;
  loggedUserName = this.commonFunction.parseApplicantInfoFromlocalStorage()[5];
  loggedUserId = this.commonFunction.parseApplicantInfoFromlocalStorage()[6];

  @Input() group: FormGroup;
  myjson: any = JSON;
  dataSource = new MatTableDataSource([]);
  editMode = false;
  masterId = 0;
  applyType: string[] = [];
  documentType: string[] = [];
  companyAreaType: string[] = [];
  industryType: string[] = [];
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  matchFinder = new MyErrorStateMatcher();
  form: FormGroup;

  ngOnInit() {
    this.getDocumentType();
    this.getApplyType();

    this.dataService.loadApplicants(this.documentAssignMaster);
    this.dataService.dataSubject.subscribe((val) => {
      this.dataSource = new MatTableDataSource(val);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      return val;
    });

    this.form = this.formBuilder.group({
      documentAssignTypeMasters: this.formBuilder.group({
        receiptNameCode: new FormControl('', [Validators.required]),
        docNameCode: new FormControl('', [Validators.required]),
      })
    });

  }

  add(body: string) {
    body['created_By'] = this.loggedUserName + this.loggedUserId;
    this.dataService.postApplicants(this.documentAssignMaster, body);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.reset(); this.editMode=false;;
  }
  edit(anyId: number) {
    this.masterId = anyId;
    const getMasterInfo = this.documentAssignMaster + '/' + anyId;
    this.apiHandler.getApiRequest(getMasterInfo).pipe(catchError(() => of([]))).subscribe((documentassignType: any) => {
      let documentAssignObject: any;
      documentAssignObject = { documentAssignTypeMasters: {} };
      documentAssignObject.documentAssignTypeMasters['receiptNameCode'] = documentassignType.receiptNameCode;
      documentAssignObject.documentAssignTypeMasters['docNameCode'] = documentassignType.docNameCode;
      this.form.setValue(documentAssignObject);
      this.editMode = true;
    });
  }

  update(body: string) {
    body['docForStageID'] = this.masterId;
    body['edited_By'] = this.loggedUserName + this.loggedUserId;
    this.dataService.putApplicants(this.documentAssignMaster, this.masterId, body);
    this.reset(); this.editMode=false;;
  }

  getApplyType() {
    this.apiHandler.getApiRequest(this.applyTypeMaster).subscribe((applyTypeApi) => {
      this.applyType = applyTypeApi;
    });
  }
  getDocumentType() {
    this.apiHandler.getApiRequest(this.documentTypeMaster).subscribe((documentApi) => {
      this.documentType = documentApi;
    });
  }
  delete(applicantID: number) {
    this.dataService.deleteApplicants(this.documentAssignMaster, applicantID);
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  reset() {
    this.form.reset();
  }
  openSnackBar(message: string, action: string) {
    this.matSnackBar.open('Saved Sucessfully', 'Close', {
      duration: 3000
    });
  }
  updateSnackBar(message: string, action: string) {
    this.matSnackBar.open('Updated Sucessfully', 'Close', {
      duration: 3000
    });
  }
  deleteSnackBar(message: string, action: string){
    this.matSnackBar.open('Deleted Sucessfully', 'Close', {
      duration: 3000
    });
  }
}
*/
	constructor(
		private apiHandler: ApiHandlerService,
		private dataService: DataSourceMasterService,
		public commonFunction: CommonFunctionsService,
		private matSnackBar: MatSnackBar,
		private formBuilder: FormBuilder
	) {}
	displayedColumns = ['docForStageID', 'receiptNameCode', 'docNameCode', 'actions'];
	documentTypeMaster = environment.tradeAppRoot + environment.documentTypeMaster;
	applyTypeMaster = environment.tradeAppRoot + environment.applyTypeMaster;
	documentAssignMaster = environment.tradeAppRoot + environment.documentAssignMaster;
	loggedUserName = this.commonFunction.parseApplicantInfoFromlocalStorage()[5];
	loggedUserId = this.commonFunction.parseApplicantInfoFromlocalStorage()[6];

	@Input() group: FormGroup;
	myjson: any = JSON;
	dataSource = new MatTableDataSource([]);
	editMode = false;
	masterId = 0;
	applyType: string[] = [];
	documentType: string[] = [];
	companyAreaType: string[] = [];
	industryType: string[] = [];
	@ViewChild(MatSort) sort: MatSort;
	@ViewChild(MatPaginator) paginator: MatPaginator;
	matchFinder = new MyErrorStateMatcher();
	form: FormGroup;

	ngOnInit() {
		this.dataService.loadApplicants(this.documentAssignMaster);
		this.dataService.dataSubject.subscribe((val) => {
			this.dataSource = new MatTableDataSource(val);
			this.dataSource.sort = this.sort;
			this.dataSource.paginator = this.paginator;
			return val;
		});

		this.form = this.formBuilder.group({
			documentAssignTypeMasters: this.formBuilder.group({
				receiptNameCode: new FormControl('', [Validators.required]),
				docNameCode: new FormControl('', [Validators.required]),
				status: new FormControl('', [Validators.required])
			})
		});
		this.getApplyType();

		this.getDocument();
	}
	getApplyType() {
		this.apiHandler.getApiRequest(this.applyTypeMaster).subscribe((yearApi) => {
			this.applyType = yearApi;
		});
	}
	getDocument() {
		this.apiHandler.getApiRequest(this.documentTypeMaster).subscribe((documentTypeMaster) => {
			this.documentType = documentTypeMaster;
		});
	}

	add(body: string) {
		body['created_By'] = this.loggedUserName + this.loggedUserId;
		this.dataService.postApplicants(this.documentAssignMaster, body);
		this.dataSource.sort = this.sort;
		this.dataSource.paginator = this.paginator;
		this.reset();
		this.editMode = false;
	}
	edit(anyId: number) {
		this.masterId = anyId;
		const getMasterInfo = this.documentAssignMaster + '/' + anyId;
		this.apiHandler.getApiRequest(getMasterInfo).pipe(catchError(() => of([]))).subscribe((documentApi: any) => {
			let documentObject: any;
			documentObject = { documentAssignTypeMasters: {} };
			documentObject.documentAssignTypeMasters['receiptNameCode'] = documentApi.receiptNameCode;
			documentObject.documentAssignTypeMasters['docNameCode'] = documentApi.docNameCode;
			documentObject.documentAssignTypeMasters['status'] = documentApi.status;

			this.form.setValue(documentObject);
			this.editMode = true;
		});
	}

	update(body: string) {
		body['docForStageID'] = this.masterId;
		body['edited_By'] = this.loggedUserName + this.loggedUserId;
		this.dataService.putApplicants(this.documentAssignMaster, this.masterId, body);
		this.reset();
		this.editMode = false;
	}
	delete(applicantID: number) {
		this.dataService.deleteApplicants(this.documentAssignMaster, applicantID);
	}

	applyFilter(filterValue: string) {
		this.dataSource.filter = filterValue.trim().toLowerCase();
	}
	openSnackBar(message: string, action: string) {
		this.matSnackBar.open('Saved Sucessfully', 'Close', {
			duration: 3000
		});
	}
	updateSnackBar(message: string, action: string) {
		this.matSnackBar.open('Updated Sucessfully', 'Close', {
			duration: 3000
		});
	}
	deleteSnackBar(message: string, action: string) {
		this.matSnackBar.open('Deleted Sucessfully', 'Close', {
			duration: 3000
		});
	}
	reset() {
		this.form.reset();
	}
}
