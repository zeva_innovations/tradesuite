import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainmastersComponent } from './mainmasters.component';

describe('MainmastersComponent', () => {
  let component: MainmastersComponent;
  let fixture: ComponentFixture<MainmastersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainmastersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainmastersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
