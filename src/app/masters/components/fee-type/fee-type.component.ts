import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ApiHandlerService } from '../../../services/api-handler.service';
import { DataSourceMasterService } from '../../../services/data-source-master.service';
import { CommonFunctionsService } from '../../../services/common-functions.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { environment } from '../../../../environments/environment';
import { MatPaginator, MatSnackBar, MatSort, MatTableDataSource } from '@angular/material';
import { MyErrorStateMatcher } from '../../../services/customer-details-form.service';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Component({
	selector: 'app-fee-type',
	templateUrl: './fee-type.component.html',
	styleUrls: ['./fee-type.component.scss']
})
export class FeeTypeComponent implements OnInit {
	constructor(
		private apiHandler: ApiHandlerService,
		private dataService: DataSourceMasterService,
		public commonFunction: CommonFunctionsService,
		private matSnackBar: MatSnackBar,
		private formBuilder: FormBuilder
	) {}
	displayedColumns = [
		'feeTypeID',
		'feeTypeCode',
		'feeTypeHeader',
		'percentage',
		'cost',
		'arrears',
		'new',
		'renewal',
		'penalty',
		'delayTime',
		'remark',
		'actions'
	];
	feeTypeMaster = environment.tradeAppRoot + environment.feeTypeMaster;

	loggedUserName = this.commonFunction.parseApplicantInfoFromlocalStorage()[5];
	loggedUserId = this.commonFunction.parseApplicantInfoFromlocalStorage()[6];

	@Input() group: FormGroup;
	myjson: any = JSON;
	dataSource = new MatTableDataSource([]);
	editMode = false;
	masterId = 0;
	companyType: string[] = [];
	companyAreaType: string[] = [];
	industryType: string[] = [];
	@ViewChild(MatSort) sort: MatSort;
	@ViewChild(MatPaginator) paginator: MatPaginator;
	matchFinder = new MyErrorStateMatcher();
	form: FormGroup;

	ngOnInit() {
		this.dataService.loadApplicants(this.feeTypeMaster);
		this.dataService.dataSubject.subscribe((val) => {
			this.dataSource = new MatTableDataSource(val);
			this.dataSource.sort = this.sort;
			this.dataSource.paginator = this.paginator;
			return val;
		});

		this.form = this.formBuilder.group({
			feeTypeMasters: this.formBuilder.group({
				feeTypeHeader: new FormControl(null, [Validators.required]),
				percentage: new FormControl(null, [Validators.required]),
				cost: new FormControl(null, [Validators.required]),
				arrears: new FormControl(null, [Validators.required]),
				new: new FormControl(null, [Validators.required]),
				renewal: new FormControl(null, [Validators.required]),
				penalty: new FormControl(null, [Validators.required]),
				delayTime: new FormControl(null, [Validators.required]),
				remark: new FormControl(null, [Validators.required]),
				status: new FormControl(null, [Validators.required])
			})
		});
	}

	add(body: string) {
		body['created_By'] = this.loggedUserName + this.loggedUserId;
		this.dataService.postApplicants(this.feeTypeMaster, body);
		this.dataSource.sort = this.sort;
		this.dataSource.paginator = this.paginator;
		this.reset();
		this.editMode = false;
	}
	edit(anyId: number) {
		this.masterId = anyId;
		const getMasterInfo = this.feeTypeMaster + '/' + anyId;
		this.apiHandler.getApiRequest(getMasterInfo).pipe(catchError(() => of([]))).subscribe((feeType: any) => {
			let feeTypeObject: any;
			feeTypeObject = { feeTypeMasters: {} };
			feeTypeObject.feeTypeMasters['feeTypeHeader'] = feeType.feeTypeHeader;

			feeTypeObject.feeTypeMasters['percentage'] = feeType.percentage;
			feeTypeObject.feeTypeMasters['cost'] = feeType.cost;
			feeTypeObject.feeTypeMasters['arrears'] = feeType.arrears;
			feeTypeObject.feeTypeMasters['new'] = feeType.new;
			feeTypeObject.feeTypeMasters['renewal'] = feeType.renewal;
			feeTypeObject.feeTypeMasters['penalty'] = feeType.penalty;
			feeTypeObject.feeTypeMasters['delayTime'] = feeType.delayTime;
			feeTypeObject.feeTypeMasters['remark'] = feeType.remark;
			feeTypeObject.feeTypeMasters['status'] = feeType.status;
			this.form.setValue(feeTypeObject);
			this.editMode = true;

		});
	}

	update(body: string) {
		body['feeTypeID'] = this.masterId;
		body['edited_By'] = this.loggedUserName + this.loggedUserId;
		this.dataService.putApplicants(this.feeTypeMaster, this.masterId, body);
		this.reset();
		this.editMode = false;
	}
	delete(applicantID: number) {
		this.dataService.deleteApplicants(this.feeTypeMaster, applicantID);
	}

	applyFilter(filterValue: string) {
		this.dataSource.filter = filterValue.trim().toLowerCase();
	}
	reset() {
		this.form.reset();
		this.editMode = false;
	}
	openSnackBar(message: string, action: string) {
		this.matSnackBar.open('Saved Sucessfully', 'Close', {
			duration: 3000
		});
	}
	updateSnackBar(message: string, action: string) {
		this.matSnackBar.open('Updated Sucessfully', 'Close', {
			duration: 3000
		});
	}
	deleteSnackBar(message: string, action: string) {
		this.matSnackBar.open('Deleted Sucessfully', 'Close', {
			duration: 3000
		});
	}
}
