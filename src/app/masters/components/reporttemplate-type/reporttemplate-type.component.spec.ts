import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReporttemplateTypeComponent } from './reporttemplate-type.component';

describe('ReporttemplateTypeComponent', () => {
  let component: ReporttemplateTypeComponent;
  let fixture: ComponentFixture<ReporttemplateTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReporttemplateTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReporttemplateTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
