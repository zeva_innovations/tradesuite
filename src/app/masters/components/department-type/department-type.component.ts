import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ApiHandlerService } from '../../../services/api-handler.service';
import { DataSourceMasterService } from '../../../services/data-source-master.service';
import { CommonFunctionsService } from '../../../services/common-functions.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { environment } from '../../../../environments/environment';
import { MatPaginator, MatSnackBar, MatSort, MatTableDataSource } from '@angular/material';
import { MyErrorStateMatcher } from '../../../services/customer-details-form.service';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Component({
	selector: 'app-department-type',
	templateUrl: './department-type.component.html',
	styleUrls: ['./department-type.component.scss']
})
export class DepartmentTypeComponent implements OnInit {
	constructor(
		private apiHandler: ApiHandlerService,
		private dataService: DataSourceMasterService,
		public commonFunction: CommonFunctionsService,
		private formBuilder: FormBuilder,
		private matSnackBar: MatSnackBar
	) {}
	displayedColumns = ['department_ID', 'department_Code', 'department_Description', 'actions'];
	departmentTypeMaster = environment.tradeAppRoot + environment.departmentType;

	loggedUserName = this.commonFunction.parseApplicantInfoFromlocalStorage()[5];
	loggedUserId = this.commonFunction.parseApplicantInfoFromlocalStorage()[6];

	@Input() group: FormGroup;
	myjson: any = JSON;
	dataSource = new MatTableDataSource([]);
	editMode = false;
	masterId = 0;
	companyType: string[] = [];
	companyAreaType: string[] = [];
	industryType: string[] = [];
	@ViewChild(MatSort) sort: MatSort;
	@ViewChild(MatPaginator) paginator: MatPaginator;
	matchFinder = new MyErrorStateMatcher();
	form: FormGroup;

	ngOnInit() {
		this.dataService.loadApplicants(this.departmentTypeMaster);
		this.dataService.dataSubject.subscribe((val) => {
			this.dataSource = new MatTableDataSource(val);
			this.dataSource.sort = this.sort;
			this.dataSource.paginator = this.paginator;
			return val;
		});

		this.form = this.formBuilder.group({
			departmentTypeMasters: this.formBuilder.group({
				department_Description: new FormControl('', [Validators.required]),
				status: new FormControl('', [Validators.required])
			})
		});
	}

	add(body: string) {
		body['created_By'] = this.loggedUserName + this.loggedUserId;
		this.dataService.postApplicants(this.departmentTypeMaster, body);
		this.dataSource.sort = this.sort;
		this.dataSource.paginator = this.paginator;
		this.reset();
		this.editMode = false;
	}
	edit(anyId: number) {
		this.masterId = anyId;
		const getMasterInfo = this.departmentTypeMaster + '/' + anyId;
		this.apiHandler.getApiRequest(getMasterInfo).pipe(catchError(() => of([]))).subscribe((deparmentType: any) => {
			let departmentTypeObject: any;
			departmentTypeObject = { departmentTypeMasters: {} };
			departmentTypeObject.departmentTypeMasters['department_Description'] = deparmentType.department_Description;
			departmentTypeObject.departmentTypeMasters['status'] = deparmentType.status;

			this.form.setValue(departmentTypeObject);
			this.editMode = true;
		});
	}

	update(body: string) {
		body['department_ID'] = this.masterId;
		body['edited_By'] = this.loggedUserName + this.loggedUserId;
		this.dataService.putApplicants(this.departmentTypeMaster, this.masterId, body);
		this.reset();
		this.editMode = false;
	}
	delete(applicantID: number) {
		this.dataService.deleteApplicants(this.departmentTypeMaster, applicantID);
	}

	applyFilter(filterValue: string) {
		this.dataSource.filter = filterValue.trim().toLowerCase();
	}
	reset() {
		this.form.reset();
	}
	openSnackBar(message: string, action: string) {
		this.matSnackBar.open('Saved Sucessfully', 'Close', {
			duration: 3000
		});
	}
	updateSnackBar(message: string, action: string) {
		this.matSnackBar.open('Updated Sucessfully', 'Close', {
			duration: 3000
		});
	}
	deleteSnackBar(message: string, action: string) {
		this.matSnackBar.open('Deleted Sucessfully', 'Close', {
			duration: 3000
		});
	}
}
