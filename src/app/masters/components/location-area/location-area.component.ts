import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {ApiHandlerService} from '../../../services/api-handler.service';
import {DataSourceMasterService} from '../../../services/data-source-master.service';
import {CommonFunctionsService} from '../../../services/common-functions.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {environment} from '../../../../environments/environment';
import {MatPaginator, MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';
import {MyErrorStateMatcher} from '../../../services/customer-details-form.service';
import {catchError} from 'rxjs/operators';
import {of} from 'rxjs';

@Component({
  selector: 'app-location-area',
  templateUrl: './location-area.component.html',
  styleUrls: ['./location-area.component.scss']
})
export class LocationAreaComponent implements OnInit {

  constructor(
    private apiHandler: ApiHandlerService,
    private dataService: DataSourceMasterService,
    public commonFunction: CommonFunctionsService,
    private matSnackBar: MatSnackBar,
    private formBuilder: FormBuilder
  ) {}
  displayedColumns = ['location_Area_Master_ID', 'location_Area_Code', 'location_Area_Name', 'actions'];
  locationMaster = environment.tradeAppRoot + environment.locationMaster;

  loggedUserName = this.commonFunction.parseApplicantInfoFromlocalStorage()[5];
  loggedUserId = this.commonFunction.parseApplicantInfoFromlocalStorage()[6];

  @Input() group: FormGroup;
  myjson: any = JSON;
  dataSource = new MatTableDataSource([]);
  editMode = false;
  masterId = 0;
  companyType: string[] = [];
  companyAreaType: string[] = [];
  industryType: string[] = [];
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  matchFinder = new MyErrorStateMatcher();
  form: FormGroup;

  ngOnInit() {
    this.dataService.loadApplicants(this.locationMaster);
    this.dataService.dataSubject.subscribe((val) => {
      this.dataSource = new MatTableDataSource(val);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      return val;
    });

    this.form = this.formBuilder.group({
      loacationAreaMasters: this.formBuilder.group({
        location_Area_Name: new FormControl('', [Validators.required]),
        org_ID: new FormControl('', [Validators.required]),
        unit_Org_ID: new FormControl('', [Validators.required]),
        institution_ID: new FormControl('', [Validators.required]),
        status: new FormControl('', [Validators.required])
      })
    });
  }

  add(body: string) {
    body['created_By'] = this.loggedUserName + this.loggedUserId;
    this.dataService.postApplicants(this.locationMaster, body);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.reset(); this.editMode = false;
  }
  edit(anyId: number) {
    this.masterId = anyId;
    const getMasterInfo = this.locationMaster + '/' + anyId;
    this.apiHandler.getApiRequest(getMasterInfo).pipe(catchError(() => of([]))).subscribe((locationType: any) => {
      let locationTypeObject: any;
      locationTypeObject = { loacationAreaMasters: {} };
      locationTypeObject.loacationAreaMasters['location_Area_Name'] = locationType.location_Area_Name;
      locationTypeObject.loacationAreaMasters['org_ID'] = locationType.org_ID;
      locationTypeObject.loacationAreaMasters['unit_Org_ID'] = locationType.unit_Org_ID;
      locationTypeObject.loacationAreaMasters['institution_ID'] = locationType.institution_ID;
      locationTypeObject.loacationAreaMasters['status'] = locationType.status;
      this.form.setValue(locationTypeObject);
      this.editMode = true;
    });
  }

  update(body: string) {
    body['location_Area_Master_ID'] = this.masterId;
    body['edited_By'] = this.loggedUserName + this.loggedUserId;
    this.dataService.putApplicants(this.locationMaster, this.masterId, body);
    this.reset(); this.editMode = false;
  }
  delete(applicantID: number) {
    this.dataService.deleteApplicants(this.locationMaster, applicantID);
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  openSnackBar(message: string, action: string) {
    this.matSnackBar.open('Saved Sucessfully', 'Close', {
      duration: 3000
    });
  }
  updateSnackBar(message: string, action: string) {
    this.matSnackBar.open('Updated Sucessfully', 'Close', {
      duration: 3000
    });
  }
  deleteSnackBar(message: string, action: string) {
    this.matSnackBar.open('Deleted Sucessfully', 'Close', {
      duration: 3000
    });
  }
  reset() {
    this.form.reset();
  }
}
