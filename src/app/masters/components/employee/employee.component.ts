import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ApiHandlerService } from '../../../services/api-handler.service';
import { DataSourceMasterService } from '../../../services/data-source-master.service';
import { CommonFunctionsService } from '../../../services/common-functions.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatPaginator, MatSnackBar, MatSort, MatTableDataSource } from '@angular/material';
import { environment } from '../../../../environments/environment';
import { MyErrorStateMatcher } from '../../../services/customer-details-form.service';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Component({
	selector: 'app-employee',
	templateUrl: './employee.component.html',
	styleUrls: ['./employee.component.scss']
})
export class EmployeeComponent implements OnInit {
	constructor(
		private apiHandler: ApiHandlerService,
		private dataService: DataSourceMasterService,
		public commonFunction: CommonFunctionsService,
		private formBuilder: FormBuilder,
		private matSnackBar: MatSnackBar
	) {}
	displayedColumns = ['emp_ID', 'emp_Code', 'emp_First_Name', 'emp_Type', 'emp_Joining_Date', 'date_Of_Birth', 'actions'];
	EmployeeMasters = environment.tradeAppRoot + environment.EmployeeMasters;
	organizationMaster = environment.tradeAppRoot + environment.organizationMaster;

	unitMaster = environment.tradeAppRoot + environment.unitMaster;
	unitBranchMaster = environment.tradeAppRoot + environment.unitBranchMaster;
	loggedUserName = this.commonFunction.parseApplicantInfoFromlocalStorage()[5];
	loggedUserId = this.commonFunction.parseApplicantInfoFromlocalStorage()[6];
	organisationMaster: string[] = [];
	UnitorganisationMaster: string[] = [];
	BranchorganisationMaster: string[] = [];
	@Input() group: FormGroup;
	myjson: any = JSON;
	dataSource = new MatTableDataSource([]);
	editMode = false;
	masterId = 0;

	@ViewChild(MatSort) sort: MatSort;
	@ViewChild(MatPaginator) paginator: MatPaginator;
	matchFinder = new MyErrorStateMatcher();
	form: FormGroup;

	ngOnInit() {
		this.dataService.loadApplicants(this.EmployeeMasters);
		this.dataService.dataSubject.subscribe((val) => {
			this.dataSource = new MatTableDataSource(val);
			this.dataSource.sort = this.sort;
			this.dataSource.paginator = this.paginator;
			return val;
		});

		this.form = this.formBuilder.group({
			employeeMasters: this.formBuilder.group({
				org_ID: new FormControl('', [Validators.required]),
				unit_Org_ID: new FormControl('', [Validators.required]),
				branch_Org_ID: new FormControl('', [Validators.required]),
				emp_First_Name: new FormControl('', [Validators.required]),
				emp_Last_Name: new FormControl('', [Validators.required]),
				emp_Type: new FormControl('', [Validators.required]),
				emp_Joining_Date: new FormControl({ value: '', disabled: false }),
				gender: new FormControl('', [Validators.required]),
				date_Of_Birth: new FormControl({ value: '', disabled: false }),
				department: new FormControl('', [Validators.required]),
				marital_Status: new FormControl('', [Validators.required]),
				qualification: new FormControl('', [Validators.required]),
				experience_Year: new FormControl('', [Validators.required]),
				designation: new FormControl('', [Validators.required]),
				grade: new FormControl('', [Validators.required]),
				location: new FormControl('', [Validators.required]),
				reporting_To: new FormControl('', [Validators.required]),
				mail_ID: new FormControl('', [Validators.required]),
				mobile: new FormControl('', [Validators.required]),
				pan_No: new FormControl('', [Validators.required]),
				pF_No: new FormControl('', [Validators.required]),
				emp_Service_Complition: new FormControl('', [Validators.required]),
				emp_Registration_Termination_Date: new FormControl({ value: '', disabled: false }),
				emp_Settlement_Date: new FormControl({ value: '', disabled: false }),
				initials: new FormControl('', [Validators.required]),
				status: new FormControl('', [Validators.required])
			})
		});
		this.getOrganisation();
		this.getUnitOrg();
		this.getUniBranchOrg();
	}

	add(body: string) {
		body['created_By'] = this.loggedUserName + this.loggedUserId;
		this.dataService.postApplicants(this.EmployeeMasters, body);
		this.dataSource.sort = this.sort;
		this.dataSource.paginator = this.paginator;
		this.reset();
	}
	edit(anyId: number) {
		this.masterId = anyId;
		const getMasterInfo = this.EmployeeMasters + '/' + anyId;
		this.apiHandler.getApiRequest(getMasterInfo).pipe(catchError(() => of([]))).subscribe((employee: any) => {
			let employeeObject: any;
			employeeObject = { employeeMasters: {} };
			employeeObject.employeeMasters['emp_First_Name'] = employee.emp_First_Name;
			employeeObject.employeeMasters['emp_Last_Name'] = employee.emp_Last_Name;
			employeeObject.employeeMasters['emp_Type'] = employee.emp_Type;
			employeeObject.employeeMasters['emp_Joining_Date'] = employee.emp_Joining_Date;
			employeeObject.employeeMasters['gender'] = employee.gender;
			employeeObject.employeeMasters['date_Of_Birth'] = employee.date_Of_Birth;
			employeeObject.employeeMasters['department'] = employee.department;
			employeeObject.employeeMasters['marital_Status'] = employee.marital_Status;
			employeeObject.employeeMasters['qualification'] = employee.qualification;
			employeeObject.employeeMasters['experience_Year'] = employee.experience_Year;
			employeeObject.employeeMasters['designation'] = employee.designation;
			employeeObject.employeeMasters['grade'] = employee.grade;
			employeeObject.employeeMasters['location'] = employee.location;
			employeeObject.employeeMasters['reporting_To'] = employee.reporting_To;
			employeeObject.employeeMasters['mail_ID'] = employee.mail_ID;
			employeeObject.employeeMasters['mobile'] = employee.mobile;
			employeeObject.employeeMasters['pan_No'] = employee.pan_No;
			employeeObject.employeeMasters['pF_No'] = employee.pF_No;
			employeeObject.employeeMasters['emp_Service_Complition'] = employee.emp_Service_Complition;
			employeeObject.employeeMasters['emp_Registration_Termination_Date'] = employee.emp_Registration_Termination_Date;
			employeeObject.employeeMasters['emp_Settlement_Date'] = employee.emp_Settlement_Date;
			employeeObject.employeeMasters['initials'] = employee.initials;
			employeeObject.employeeMasters['status'] = employee.status;
			employeeObject.employeeMasters['org_ID'] = employee.orG_ID;
			employeeObject.employeeMasters['unit_Org_ID'] = employee.uniT_ORG_ID;
			employeeObject.employeeMasters['branch_Org_ID'] = employee.brancH_ORG_ID;
			this.form.setValue(employeeObject);
			this.editMode = true;
		});
	}

	update(body: string) {
		body['employee_Master_ID'] = this.masterId;
		body['edited_By'] = this.loggedUserName + this.loggedUserId;
		this.dataService.putApplicants(this.EmployeeMasters, this.masterId, body);
		this.reset();
	}
	delete(applicantID: number) {
		this.dataService.deleteApplicants(this.EmployeeMasters, applicantID);
	}

	applyFilter(filterValue: string) {
		this.dataSource.filter = filterValue.trim().toLowerCase();
	}
	reset() {
		this.form.reset();
	}
	openSnackBar(message: string, action: string) {
		this.matSnackBar.open('Saved Sucessfully', 'Close', {
			duration: 3000
		});
	}
	updateSnackBar(message: string, action: string) {
		this.matSnackBar.open('Updated Sucessfully', 'Close', {
			duration: 3000
		});
	}
	deleteSnackBar(message: string, action: string) {
		this.matSnackBar.open('Deleted Sucessfully', 'Close', {
			duration: 3000
		});
	}

	getOrganisation() {
		this.apiHandler.getApiRequest(this.organizationMaster).subscribe((yearApi) => {
			this.organisationMaster = yearApi;
		});
	}
	getUnitOrg() {
		this.apiHandler.getApiRequest(this.unitMaster).subscribe((yearApi) => {
			this.UnitorganisationMaster = yearApi;
		});
	}
	getUniBranchOrg() {
		this.apiHandler.getApiRequest(this.unitBranchMaster).subscribe((yearApi) => {
			this.BranchorganisationMaster = yearApi;
		});
	}
}
