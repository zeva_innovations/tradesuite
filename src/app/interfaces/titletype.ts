export interface Titletype {
  titleId: number;
  titleCode: string;
  titleName: string;
  createdBy: string;
  createdDate: string;
  editedBy: string;
  editedDate: string;
  status: string;
  revisionId: number;
}
