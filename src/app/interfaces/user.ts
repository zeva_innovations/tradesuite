export interface ICurrentUser {
	token: string;
	expiresIn: number;
	orG_ID: string;
	uniT_ORG_ID: string;
	brancH_ORG_ID: string;
	userID: number;
	refreshToken: string;
	userName: string;
	error: string;
}
