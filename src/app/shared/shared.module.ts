/*
  Importing all the angular modules here
*/
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from '../components/navbar/navbar.component';
import { NavigationBarComponent } from '../components/navigation-bar/navigation-bar.component';
import { NgIdleKeepaliveModule } from '@ng-idle/keepalive';
import { RouterModule } from '@angular/router';
import {
	MatButtonModule,
	MatCheckboxModule,
	MatFormFieldModule,
	MatIconModule,
	MatInputModule,
	MatListModule,
	MatSelectModule,
	MatToolbarModule,
	MatDatepickerModule,
	MatSnackBarModule,
	MatGridListModule,
	MatTableModule,
	MatProgressSpinnerModule,
	MatPaginatorModule,
	MatSortModule,
	MatDialogModule,
	MatCardModule,
	MatSidenavModule,
	MatMenuModule,
	MatTabsModule,
	MatChipsModule,
	MatTooltipModule,
	MatExpansionModule,
	MatProgressBarModule,

} from '@angular/material';

@NgModule({
	declarations: [NavbarComponent, NavigationBarComponent],
	imports: [
		RouterModule,
		CommonModule,
		MatButtonModule,
		MatCheckboxModule,
		MatFormFieldModule,
		MatIconModule,
		MatInputModule,
		MatListModule,
		MatSelectModule,
		MatToolbarModule,
		MatDatepickerModule,
		MatSnackBarModule,
		MatGridListModule,
		MatTableModule,
		MatProgressSpinnerModule,
		MatPaginatorModule,
		MatSortModule,
		MatDialogModule,
		MatCardModule,
		MatSidenavModule,
		MatMenuModule,
		MatTabsModule,
		MatChipsModule,
		MatTooltipModule,
		MatExpansionModule,
		MatProgressBarModule,
		NgIdleKeepaliveModule.forRoot()
	],
	exports: [
		RouterModule,
		CommonModule,
		MatButtonModule,
		MatCheckboxModule,
		MatFormFieldModule,
		MatIconModule,
		MatInputModule,
		MatListModule,
		MatSelectModule,
		MatToolbarModule,
		MatDatepickerModule,
		MatSnackBarModule,
		MatGridListModule,
		MatTableModule,
		MatProgressSpinnerModule,
		MatPaginatorModule,
		MatSortModule,
		MatDialogModule,
		MatCardModule,
		MatSidenavModule,
		MatMenuModule,
		MatTabsModule,
		MatChipsModule,
		MatTooltipModule,
		MatExpansionModule,
		MatProgressBarModule,
		NavbarComponent,
		NavigationBarComponent
	]
})
export class SharedModule { }
