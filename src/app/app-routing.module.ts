import { CardIssueListComponent } from './components/card-issue-list/card-issue-list.component';
import { CardWriteListComponent } from './components/card-write-list/card-write-list.component';
import { CardPrintListComponent } from './components/card-print-list/card-print-list.component';
import { FormCollectionCompanyGridComponent } from './components/form-collection-company-grid/form-collection-company-grid.component';
import { CompanyListAccountOneComponent } from './components/company-list-account-one/company-list-account-one.component';
import { CompanyListAccountTwoComponent } from './components/company-list-account-two/company-list-account-two.component';
import { DataentryComponent } from './components/dataentry/dataentry.component';
import { FormCollectionComponent } from './components/form-collection/form-collection.component';
import { ApplicantReceiptComponent } from './components/applicant-receipt/applicant-receipt.component';
import { CompanyDetailsComponent } from './components/company-details/company-details.component';
import { FileUploadComponent } from './components/file-upload/file-upload.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { ScanDepartmentComponent } from './components/scan-department/scan-department.component';
import { ApplicationIssueCompanyGridComponent } from './components/application-issue-company-grid/application-issue-company-grid.component';
import { CompanyRegistrationComponent } from './components/company-registration/company-registration.component';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { VerifyassignComponent } from './components/verifyassign/verifyassign.component';

import { CompanyApprovalDGListComponent } from './components/company-approval-dglist/company-approval-dglist.component';

import { CompanyRenewalGridComponent } from './components/company-renewal-grid/company-renewal-grid.component';
import { CompanyListsComponent } from './components/company-lists/company-lists.component';

import { AuthGuard } from './services/guards.service';
import {CompanyApprovalListComponent} from './components/company-approval-list/company-approval-list.component';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  {
    path: 'applicantregistration',
    component: RegisterComponent,
    canActivate: [AuthGuard],
    data: {roles: ['applicantregistration']}
  },
	{ path: 'file', component: FileUploadComponent },
	{ path: 'company', component: CompanyDetailsComponent },
	{
		path: 'applicationissue',
		component: ApplicationIssueCompanyGridComponent,
		canActivate: [AuthGuard],
		data: { roles: ['applicationissue'] }
	},
	{ path: 'formcollection', component: FormCollectionComponent },
	{
		path: 'formcollect',
		component: FormCollectionCompanyGridComponent,
		canActivate: [AuthGuard],
		data: { roles: ['formcollect'] }
	},
	{
		path: 'scandepartment',
		component: ScanDepartmentComponent,
		canActivate: [AuthGuard],
		data: { roles: ['scandepartment'] }
	},
	{
		path: 'print',
		component: ApplicantReceiptComponent
	},
	{
		path: 'companyregistration',
		component: CompanyRegistrationComponent,
		canActivate: [AuthGuard],
		data: { roles: ['companyregistration'] }
	},
	{ path: 'welcome', component: WelcomeComponent },
	{
		path: 'dataentry',
		component: DataentryComponent,
		canActivate: [AuthGuard],
		data: { roles: ['dataentry'] }
	},
	{
		path: 'verifyassign',
		component: VerifyassignComponent,
		canActivate: [AuthGuard],
		data: { roles: ['companyregistration'] }
	},
	{
		path: 'companyapproval',
		component: CompanyApprovalListComponent,
		canActivate: [AuthGuard],
		data: { roles: ['companyapproval'] }
	},
	{
		path: 'accountone',
		component: CompanyListAccountOneComponent,
		canActivate: [AuthGuard],
		data: { roles: ['accountone'] }
	},
	{
		path: 'accounttwo',
		component: CompanyListAccountTwoComponent,
		canActivate: [AuthGuard],
		data: { roles: ['accounttwo'] }
	},
	{
		path: 'companydgapproval',
		component: CompanyApprovalDGListComponent,
		canActivate: [AuthGuard],
		data: { roles: ['companydgapproval'] }
	},
	{
		path: 'cardprint',
		component: CardPrintListComponent,
		canActivate: [AuthGuard],
		data: { roles: ['cardprint'] }
	},
	{
		path: 'cardwrite',
		component: CardWriteListComponent,
		canActivate: [AuthGuard],
		data: { roles: ['cardwrite'] }
	},
	{
		path: 'cardissue',
		component: CardIssueListComponent,
		canActivate: [AuthGuard],
		data: { roles: ['cardissue'] }
	},
	{
		path: 'companyrenewalgrid',
		component: CompanyRenewalGridComponent
	},
	{
		path: 'companylists',
		component: CompanyListsComponent,
		canActivate: [AuthGuard],
		data: { roles: ['companylists'] }
	},
	{
		path: 'MainMaster',
		loadChildren: './masters/masters.module#MastersModule'
	}
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule {}
